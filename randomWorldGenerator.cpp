//
// Created by jacomus on 8/25/17.
//

#include "randomWorldGenerator.h"
#include "gameData.h"

RandomWorldGenerator::RandomWorldGenerator(int rangeBegin, int rangeEnd) :
    m_randomDevice(), m_randomNumberEngine(m_randomDevice()), m_distribution(rangeBegin, rangeEnd)
{}

bool RandomWorldGenerator::canPlacePlatformHere(const Vector2D& v, const Vector2D& lastPosition, const int iteration)
{
    bool result = false;
    Vector2D difference = v - lastPosition;
    if (v.getX() == lastPosition.getX() and v.getY() == lastPosition.getY())
    {
        result = true;
    }
    else if (difference.getX() >= 25 and difference.getX() <= 200)
    {
        int randNum = m_distribution(m_randomNumberEngine);
        switch(iteration)
        {
            case 0:
                if (randNum < 5) result = true;
                break;
            case 1:
                if (randNum < 25) result = true;
                break;
            case 2:
                if (randNum <= 65) result = true;
                break;
            case 3:
                if (randNum <= 100) result = true;
                break;
            default:
                result = false;
                break;
        }
    }
    return result;
}

bool RandomWorldGenerator::willPlaceEnemyHere()
{
    bool result = false;
    int randNum = m_distribution(m_randomNumberEngine);
    if (randNum < 50)
    {
        result = true;
    }
    return result;
}

GameObject* RandomWorldGenerator::placePlayerHere(const Vector2D& pos, GameObjectFactory& gOF,
                                                  const std::string& playerString)
{
    GameObject* player = gOF.create(playerString);
    Vector2D temp = pos;
    temp.setY(temp.getY() - player->getHeight());
    player->setPosition(temp);
    return player;
}

GameObject* RandomWorldGenerator::placeWeaponHere(const Vector2D& pos, GameObjectFactory& gOF,
                                                  const std::string& weaponName, const bool inBox)
{
    auto weapon = dynamic_cast<Weapon*>(gOF.create(weaponName));
    weapon->setPosition(pos);
    weapon->setInBox(inBox);
    return weapon;
}

GameObject* RandomWorldGenerator::placeEnemyHere(const Vector2D& pos, GameObjectFactory& gOF,
                                                 const std::string& enemyType)
{
    auto enemy = dynamic_cast<Enemy*>(gOF.create(enemyType));
    Vector2D temp = pos;
    temp.setY(temp.getY() - (enemy->getCollisionBox().getYOffset() + enemy->getCollisionBox().getHeight()));
    enemy->setPosition(temp);
    enemy->setPatrolArea(int(pos.getX()), int(pos.getX()) + 125, int(pos.getY()));
    return enemy;
}

GameObject* RandomWorldGenerator::placeCrateHere(Vector2D& pos, GameObjectFactory& gOF,
                                                 const std::string& crateType)
{
    GameObject* c = gOF.create(crateType);
    Vector2D temp(pos);
    temp.setX(temp.getX() + 62.0f); // 62 is roughly half the width of a platform
    temp.setY(temp.getY() - c->getHeight());
    c->setPosition(temp);
    return c;
}

GameObject* RandomWorldGenerator::placePlatformHere(Vector2D& pos, const Vector2D& lastPosition, GameObjectFactory& gOF,
                                                    const std::string& platType)
{
    std::random_device rD;
    std::mt19937 rNE(rD());
    std::uniform_int_distribution<> dist(1, 25);
    GameObject* platform = nullptr;
    if (platType == "botPlat")
    {
        if (pos.getX() != 0.0f)
        {
            pos.setX(pos.getX() + 25.0f);
            platform = gOF.create(platType);
            platform->setPosition(pos);
            dynamic_cast<CollisionObject*>(platform)->updateCollisionBoxes();
        }
        else
        {
            platform = gOF.create(platType);
            platform->setPosition(pos);
            dynamic_cast<CollisionObject*>(platform)->updateCollisionBoxes();
        }
    }
    else
    {
        if (canPlacePlatformHere(pos, lastPosition, 0))
        {
            platform = gOF.create(platType);
            platform->setPosition(pos);
            dynamic_cast<CollisionObject*>(platform)->updateCollisionBoxes();
        }
        else
        {
            pos.setX(pos.getX() + dist(rNE));
            if (canPlacePlatformHere(pos, lastPosition, 1))
            {
                platform = gOF.create(platType);
                platform->setPosition(pos);
                dynamic_cast<CollisionObject*>(platform)->updateCollisionBoxes();
            }
            else
            {
                pos.setX(pos.getX() + dist(rNE));
                if (canPlacePlatformHere(pos, lastPosition, 2))
                {
                    platform = gOF.create(platType);
                    platform->setPosition(pos);
                    dynamic_cast<CollisionObject*>(platform)->updateCollisionBoxes();
                }
                else
                {
                    pos.setX(pos.getX() + dist(rNE));
                    if (canPlacePlatformHere(pos, lastPosition, 3))
                    {
                        platform = gOF.create(platType);
                        platform->setPosition(pos);
                        dynamic_cast<CollisionObject*>(platform)->updateCollisionBoxes();
                    }
                    else
                    {
                        std::cout << "ERROR! - " << platType << " was not placed!" << std::endl;
                    }
                }
            }
        }
    }
    return platform;
}

bool RandomWorldGenerator::generateWorld(std::vector<Platform*>& platforms, Player** player,
                                         std::vector<Enemy*>& enemies, std::vector<Crate*>& crates,
                                         std::vector<Weapon*>& weapons, GameObjectFactory& gOF)
{
    GameData* inst = GameData::getInstance();
    int worldWidth = inst->getJsonInt("GameData.playState.world.width");
    int bottomPlatWidth = inst->getJsonInt("GameData.playState.textures.BottomPlatform.width");
    int floatPlatHeight = inst->getJsonInt("GameData.playState.textures.FloatingPlatform.height");
    Vector2D position(0.0f, 423.0f);
    Vector2D lastPosition(0.0f, 423.0f);
    GameObject* plat = nullptr;
    Player* p = nullptr;
    Enemy* e = nullptr;
    Weapon* w = nullptr;
    Crate* c = nullptr;
    // booleans to determine whether or not a weapon has been deposited into a crate yet
    bool spearPlaced = false;
    bool battleAxePlaced = false;
    bool glaivePlaced = false;
    bool scimitarPlaced = false;
    bool spindarPlaced = false;
    bool result = true;
    while (position.getY() > 27)
    {
        while (position.getX() < worldWidth)
        {
            if (position.getY() == 423)
            {
                plat = placePlatformHere(position, lastPosition, gOF, "botPlat");
            }
            else
            {
                plat = placePlatformHere(position, lastPosition, gOF, "floatPlat");
            }
            if (plat != nullptr)
            {
                if (plat->getPosition().getY() == 423)
                {
                    if (plat->getPosition().getX() == 0)
                    {
                        p = dynamic_cast<Player*>(placePlayerHere(position, gOF, "player"));
                        w = dynamic_cast<Weapon*>(placeWeaponHere(position, gOF, "sword", false));
                        w->setPositionX(w->getPosition().getX() + 70);
                        w->setPositionY(w->getPosition().getY() - 50);
                        if (p == nullptr or w == nullptr)
                        {
                            result = false;
                            break;
                        }
                        else
                        {
                            *player = p;
                            weapons.emplace_back(w);
                        }
                    }
                    else
                    {
                        if (position.getX() < (worldWidth - 125))
                        {
                            if (lastPosition.getX() + bottomPlatWidth != position.getX())
                            {
                                if (willPlaceEnemyHere())
                                {
                                    e = dynamic_cast<Enemy*>(placeEnemyHere(position, gOF, "enemy"));
                                    if (e != nullptr)
                                    {
                                        enemies.emplace_back(e);
                                    }
                                    else
                                    {
                                        result = false;
                                        break;
                                    }
                                }
                                else
                                {
                                    c = dynamic_cast<Crate*>(placeCrateHere(position, gOF, "woodenCrate"));
                                    if (c != nullptr)
                                    {
                                        int randNum = m_distribution(m_randomNumberEngine);
                                        if (randNum < 50)
                                        {
                                            std::string wTP = determineWhichWeaponToPlaceInCrate(&spearPlaced,
                                                                                                 &battleAxePlaced,
                                                                                                 &glaivePlaced,
                                                                                                 &scimitarPlaced,
                                                                                                 &spindarPlaced);
                                            w = dynamic_cast<Weapon*>(placeWeaponHere(position, gOF, wTP, true));
                                            weapons.emplace_back(w);
                                            c->depositLoot(w);
                                        }
                                        crates.emplace_back(c);
                                    }
                                }
                            }
                            else
                            {
                                enemies.back()->setPatrolArea(int(lastPosition.getX()), int(position.getX()) + bottomPlatWidth);
                                dynamic_cast<Platform*>(plat)->enableOrDisableLeftCollisionBox(false);
                                platforms.back()->enableOrDisableRightCollisionBox(false);
                            }
                        }
                    }
                }
                else
                {
                    if (position.getX() < (worldWidth - 125))
                    {
                        if (lastPosition.getX() + bottomPlatWidth != position.getX())
                        {
                            if (willPlaceEnemyHere())
                            {
                                e = dynamic_cast<Enemy*>(placeEnemyHere(position, gOF, "enemy"));
                                if (e != nullptr)
                                {
                                    enemies.emplace_back(e);
                                }
                                else
                                {
                                    result = false;
                                    break;
                                }
                            }
                            else
                            {
                                c = dynamic_cast<Crate*>(placeCrateHere(position, gOF, "woodenCrate"));
                                if (c != nullptr)
                                {
                                    int randNum = m_distribution(m_randomNumberEngine);
                                    if (randNum < 50)
                                    {
                                        std::string wTP = determineWhichWeaponToPlaceInCrate(&spearPlaced,
                                                                                             &battleAxePlaced,
                                                                                             &glaivePlaced,
                                                                                             &scimitarPlaced,
                                                                                             &spindarPlaced);
                                        if (wTP != "")
                                        {
                                            w = dynamic_cast<Weapon*>(placeWeaponHere(position, gOF, wTP, true));
                                            weapons.emplace_back(w);
                                            c->depositLoot(w);
                                        }
                                    }
                                    crates.emplace_back(c);
                                }
                            }
                        }
                        else
                        {
                            enemies.back()->setPatrolArea(int(lastPosition.getX()), int(position.getX()) + 125);
                            dynamic_cast<Platform*>(plat)->enableOrDisableLeftCollisionBox(false);
                            platforms.back()->enableOrDisableRightCollisionBox(false);
                        }
                    }
                }
                lastPosition = position;
                position.setX(position.getX() + bottomPlatWidth);
                platforms.emplace_back(dynamic_cast<Platform*>(plat));
            }
            else
            {
                result = false;
                break;
            }
        }
        lastPosition.setY(position.getY() - (3 * floatPlatHeight + 10));
        lastPosition.setX(0.0f);
        position.setY(position.getY() - (3 * floatPlatHeight + 10));
        position.setX(1.0f);
    }
    return result;
}

std::string RandomWorldGenerator::determineWhichWeaponToPlaceInCrate(bool* spearP, bool* battleAxeP,
                                                                     bool* glaiveP, bool* scimitarP,
                                                                     bool* spindarP)
{
    std::string weaponToPlace;
    bool sP, bP, gP, scP, spP;
    if (spearP != nullptr) sP = *spearP;
    if (battleAxeP != nullptr) bP = *battleAxeP;
    if (glaiveP != nullptr) gP = *glaiveP;
    if (scimitarP != nullptr) scP = *scimitarP;
    if (spindarP != nullptr) spP = *spindarP;
    if (!sP)
    {
        weaponToPlace = "spear";
        *spearP = true;
    }
    else if (!bP)
    {
        weaponToPlace = "battleAxe";
        *battleAxeP = true;
    }
    else if (!gP)
    {
        weaponToPlace = "glaive";
        *glaiveP = true;
    }
    else if (!scP)
    {
        weaponToPlace = "scimitar";
        *scimitarP = true;
    }
    else if (!spP)
    {
        weaponToPlace = "spindar";
        *spindarP = true;
    }
    return weaponToPlace;
}
