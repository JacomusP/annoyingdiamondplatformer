//
// Created by jacomus on 7/11/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_PLAYERCREATOR_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_PLAYERCREATOR_H

#include "baseCreator.h"

class PlayerCreator : public BaseCreator
{
public:
    PlayerCreator(const int gObjID, const std::string& jsonPath,
                  const std::string& baseJsonPath, const std::string& texturePath);
    GameObject* createGameObject();
    virtual ~PlayerCreator() {}
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_PLAYERCREATOR_H
