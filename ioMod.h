//
// Created by jacomus on 3/6/17.
//

#ifndef POLYMORPHISMINACTION_IOMOD_H
#define POLYMORPHISMINACTION_IOMOD_H

#include <iostream>
#include <string>
#include <SDL.h>
#include <SDL_ttf.h>

class IO_Mod
{
public:
    static IO_Mod* getInstance();
    static void destroy();
    SDL_Texture* readTexture(const std::string& filename);
    SDL_Surface* readSurface(const std::string& filename);
    void writeText(const std::string& msg, int x, int y) const;
    void writeText(const std::string& msg, int x, int y, SDL_Color textColor) const;
    void setTextColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a);

private:
    int m_init;
    TTF_Font* m_font;
    SDL_Color m_textColor;
    SDL_Renderer* m_renderer;
    static IO_Mod* myInstance;
    IO_Mod();
    ~IO_Mod();

    // Explicitly disallow those functions we do not want the compiler generating for us
    IO_Mod(const IO_Mod& io) = delete;
    IO_Mod& operator=(const IO_Mod& io) = delete;
};


#endif //POLYMORPHISMINACTION_IOMOD_H
