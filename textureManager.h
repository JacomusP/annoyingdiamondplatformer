//
// Created by jacomus on 2/7/17.
//

#ifndef MYCODEALONG_TEXTUREMANAGER_H
#define MYCODEALONG_TEXTUREMANAGER_H

#include <string>
#include <map>
#include <SDL.h>

class TextureManager
{
public:
    // Explicitly disallow the compiler to generate these functions
    TextureManager(const std::map<std::string, SDL_Texture*> m) = delete;
    TextureManager(const TextureManager& tM) = delete;
    TextureManager& operator=(const TextureManager& tM) = delete;

    static TextureManager* getInstance();
    static void destroy();
    void load(std::string fileName, std::string id, int fWidth,
              int fHeight, SDL_Renderer *pRenderer);
    // draw
    void draw(std::string id, int x, int y,
              SDL_Renderer* pRenderer, SDL_RendererFlip flip = SDL_FLIP_NONE);
    void draw(std::string id, SDL_Rect source, int x, int y, int width, int height,
              SDL_Renderer* pRenderer, SDL_RendererFlip flip = SDL_FLIP_NONE);
    // draw frame
    void drawFrame(std::string id, int x, int y, int currentRow, int currentFrame,
                   SDL_Renderer* pRenderer, SDL_RendererFlip flip = SDL_FLIP_NONE);
    void clearFromTextureMap(const std::string& textureID);
    SDL_Surface* getSurface(const std::string& surfaceID);
    int getWidth(const std::string& id) const;
    int getHeight(const std::string& id) const;
private:
    struct Textures
    {
        SDL_Texture* m_texture;
        int m_frameWidth;
        int m_frameHeight;
        Textures(SDL_Texture* texture, const int fWidth, const int fHeight) :
            m_texture(texture), m_frameWidth(fWidth), m_frameHeight(fHeight) {}
    };
    std::map<std::string, Textures*> m_textureMap;
    std::map<std::string, SDL_Surface*> m_surfaceMap;
    static TextureManager* myInstance;

    // Methods
    TextureManager();
    ~TextureManager();
};

#endif //MYCODEALONG_TEXTUREMANAGER_H
