//
// Created by jacomus on 3/23/17.
//

#include "menuButton.h"
#include "vector2D.h"
#include "inputHandler.h"
#include "textureManager.h"

MenuButton::MenuButton(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
                       const unsigned int numRows, const unsigned int numCols, const std::string& name, const int frameInterval,
                       const unsigned int health, void (* callback)()) :
    GameObject(pos, vel, accel, textureID, numRows, numCols, name, frameInterval, health),
    m_callback(callback), m_bReleased(false)
{}

MenuButton::MenuButton(const MenuButton& mB) :
    GameObject(mB), m_callback(mB.m_callback), m_bReleased(mB.m_bReleased)
{}

MenuButton& MenuButton::operator=(const MenuButton& mB)
{
    if (this == &mB)
    {
        return *this;
    }
    GameObject::operator=(mB);
    m_callback = mB.m_callback;
    m_bReleased = mB.m_bReleased;
    return *this;
}

MenuButton::~MenuButton()
{
    std::cout << "MenuButton destructor" << std::endl;
}

void MenuButton::draw(const SDL_Renderer *pRenderer, SDL_RendererFlip flip) const
{
    GameObject::draw(pRenderer, flip);
}

void MenuButton::update(Uint32 ticks, const GameState& gameState)
{
    ticks += 1;
    Vector2D* pMousePos = InputHandler::getInstance()->getMousePosition();

    if (gameState.getStateID() == "PLAY")
    {}
    SpriteSheet sS = getSpriteSheet();

    if (pMousePos->getX() < (getPosition().getX() + getWidth())
        and pMousePos->getX() > getPosition().getX()
        and pMousePos->getY() < (getPosition().getY() + getHeight())
        and pMousePos->getY() > getPosition().getY())
    {
        sS.setCurrentFrame(MOUSE_OVER);

        if (InputHandler::getInstance()->getMouseButtonState(mouse_buttons::LEFT) and m_bReleased)
        {
            sS.setCurrentFrame(CLICKED);
            m_callback(); // call our callback function
            m_bReleased = false;
        }
        else if (!InputHandler::getInstance()->getMouseButtonState(mouse_buttons::LEFT))
        {
            m_bReleased = true;
            sS.setCurrentFrame(MOUSE_OVER);
        }
    }
    else
    {
        sS.setCurrentFrame(MOUSE_OUT);
    }
}

std::string MenuButton::type() const
{
    return "MenuButton";
}

bool MenuButton::checkForCollision(const GameObject& gObj)
{
    gObj.type();
    return false;
}
