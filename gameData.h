//
// Created by jacomus on 2/28/17.
//

#ifndef POLYMORPHISMINACTION_GAMEDATA_H
#define POLYMORPHISMINACTION_GAMEDATA_H

#include <SDL.h>
#include <string>
#include "parseJSON.h"

class GameData
{
public:
    static GameData* getInstance();
    static void clean();

    bool getJsonBool(const std::string& s) const;
    const std::string getJsonString(const std::string& s) const;
    float getJsonFloat(const std::string& s) const;
    int getJsonInt(const std::string& s) const;
    Uint32 getJsonUInt(const std::string& s) const;

private:
    ParseJSON m_parser;
    boost::property_tree::ptree m_gameData;
    static GameData* myInstance;

    GameData(const std::string& fileName = "JsonSpec/game.json");
    ~GameData();

    GameData(const GameData& gD) = delete;
    GameData& operator=(const GameData& gD) = delete;
};


#endif //POLYMORPHISMINACTION_GAMEDATA_H
