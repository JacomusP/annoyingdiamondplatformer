//
// Created by jacomus on 2/14/17.
//

#include <iostream>
#include "gameStateMachine.h"

GameStateMachine::GameStateMachine() :
    m_currentState(), m_previousState(), m_statesToDelete(), m_gameStates()
{}

GameStateMachine::~GameStateMachine()
{
    for (std::pair<std::string, GameState*> gS : m_gameStates)
    {
        delete gS.second;
    }
    m_gameStates.clear();
}

void GameStateMachine::pushState(const std::string& stateName, GameState* pState)
{
    m_previousState = m_currentState;
    m_currentState = stateName;
    if (m_gameStates.find(m_currentState) == m_gameStates.end())
    {
        // add the state to the state map
        m_gameStates[m_currentState] = pState;
    }
    else
    {
        delete pState;
    }
    m_gameStates[m_currentState]->onEnter();
}

void GameStateMachine::changeState(const std::string& stateName, GameState* pState)
{
    if (!m_gameStates.empty())
    {
        if (m_gameStates[m_currentState]->getStateID() == pState->getStateID())
        {
            return; // do nothing because we are already in this state
        }
        m_gameStates[m_currentState]->onExit();
        m_statesToDelete.emplace_back(m_currentState);
    }
    m_currentState = stateName;
    // add our new state
    if (m_gameStates.find(m_currentState) == m_gameStates.end())
    {
        m_gameStates[m_currentState] = pState;
    }
    else
    {
        delete pState;
    }
    // initialize it
    m_gameStates[m_currentState]->onEnter();
}

void GameStateMachine::exitState(const std::string& stateName)
{
    if (!m_gameStates.empty())
    {
        if (m_gameStates.find(stateName) != m_gameStates.end())
        {
            m_gameStates[stateName]->onExit();
            m_statesToDelete.emplace_back(stateName);
        }
    }
}

void GameStateMachine::popState()
{
    if (!m_gameStates.empty())
    {
        m_gameStates[m_currentState]->onExit();
        m_statesToDelete.emplace_back(m_currentState);
        m_currentState = m_previousState;
        m_previousState.clear();
    }
}

void GameStateMachine::update(Uint32 ticks)
{
    if (!m_gameStates.empty())
    {
        m_gameStates[m_currentState]->update(ticks);
        if (m_statesToDelete.size() > 0)
        {
            for (std::string& stateToDelete : m_statesToDelete)
            {
                delete m_gameStates[stateToDelete];
                m_gameStates.erase(stateToDelete);
            }
            m_statesToDelete.clear();
        }
    }
}

void GameStateMachine::render()
{
    if (!m_gameStates.empty())
    {
        m_gameStates[m_currentState]->render();
    }
}
