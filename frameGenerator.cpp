#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include "frameGenerator.h"
#include "gameData.h"

void FrameGenerator::makeFrame() 
{
    unsigned int maxFrames = static_cast<unsigned int>(GameData::getInstance()->getJsonInt("GameData.frameMax"));
    if (frameCount > maxFrames) return;

    SDL_Surface* screenCap = SDL_CreateRGBSurface(0, GameData::getInstance()->getJsonInt("GameData.viewPort.width"),
                                                  GameData::getInstance()->getJsonInt("GameData.viewPort.height"), 32,
                                                  0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000);
    if (screenCap) 
    {
        SDL_RenderReadPixels(rend, NULL, SDL_GetWindowPixelFormat(window), 
        screenCap->pixels, screenCap->pitch); 
    }
    std::stringstream strm;
    strm << "frames/" << GameData::getInstance()->getJsonString("GameData.username").c_str() << '.' << std::setfill('0') << std::setw(4)
       << frameCount++ << ".bmp";
    std::string filename(strm.str());
    std::cout << "Making frame: " << filename << std::endl;
    SDL_SaveBMP(screenCap, filename.c_str());
    SDL_FreeSurface(screenCap);
}

