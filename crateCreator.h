//
// Created by jacomus on 9/4/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_CRATECREATOR_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_CRATECREATOR_H

#include "baseCreator.h"

class CrateCreator : public BaseCreator
{
public:
    CrateCreator(const int gObjID, const std::string& jsonPath,
                 const std::string& baseJsonPath, const std::string& texturePath, const std::string& crateName);
    GameObject* createGameObject();
    virtual ~CrateCreator() = default;

private:
    std::string m_baseCrateName;
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_CRATECREATOR_H
