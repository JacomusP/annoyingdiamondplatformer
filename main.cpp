#include <iostream>
#include "game.h"

int main(int argc, char* argv[])
{
    std::cout << argc << ": " << argv[0] << std::endl;
    std::cout << "game init attempt..." << std::endl;
    Game* game = Game::getInstance();
    try
    {
        game->init();
        std::cout << "game init success!" <<std::endl;
        game->playGame();
    }
    catch (const std::string& msg)
    {
        std::cerr << msg << std::endl;
    }

    std::cout << "game closing..." << std::endl;
    game->clean();
    return 0;
}