//
// Created by jacomus on 4/19/17.
//

#include <vector>
#include <algorithm>
#include "collision.h"
#include "viewPort.h"
#include "textureManager.h"
#include "player.h"
#include "enemy.h"

Collision* Collision::myInstance = nullptr;

Collision* Collision::getInstance()
{
    if (myInstance == nullptr)
    {
        myInstance = new Collision();
    }
    return myInstance;
}

void Collision::destroy()
{
    delete myInstance;
}

bool Collision::gameObjectsAndCollisionObject(const GameObject* gObj, const CollisionObject* cObj) const
{
    //return perPixelCollisionMethod(gObj, cObj);
    return rectangularCollisionMethod(gObj, cObj);
}

bool Collision::weaponCarrierAndGameObject(const WeaponCarrier* wC, const GameObject* gObj) const
{
    return perPixelCollisionMethod(wC, gObj);
}

bool Collision::crateAndWeapon(const Crate* c, const GameObject* gObj) const
{
    Weapon* w = dynamic_cast<Weapon*>(const_cast<GameObject*>(gObj));
    return rectangularCollisionMethod(c, w);
}

bool Collision::shooterAndGameObject(const Shooter* s, const GameObject* gObj) const
{
    return rectangularCollisionMethod(s, gObj);
}

bool Collision::bulletAndGameObject(const Bullet* b, const GameObject* gObj) const
{
    return perPixelCollisionMethod(b, gObj);
}

bool Collision::bulletAndWeapon(const Bullet* b, const Weapon* w) const
{
    return rectangularCollisionMethod(b, w);
}

bool Collision::isVisible(Uint32 pixel, SDL_Surface* surface) const
{
    Uint32 temp;
    Uint8 alpha;
    SDL_PixelFormat* fmt = surface->format;
    if (fmt->BitsPerPixel == 32)
    {
        temp = pixel & fmt->Amask; /* Isolate alpha component */
        temp = temp >> fmt->Ashift;/* Shift it down to 8-bit */
        temp = temp << fmt->Aloss; /* Expand to a full 8-bit number */
        alpha = static_cast<Uint8>(temp);
        if (alpha == 0) return false;
    }
    return true;
}

bool Collision::rectangularCollisionMethod(const GameObject* gObj, const CollisionObject* cObj) const
{
    bool result = false;
    if (gameObjectCollidedWithPlatform(const_cast<GameObject*>(gObj), cObj->getRightCollisionBox()) and
        !gObj->isOnGround() and cObj->getRightCollisionBox().active)
    {
        result = true;
        const_cast<CollisionObject*>(cObj)->setCollisionSideRight();
    }
    else if (gameObjectCollidedWithPlatform(const_cast<GameObject*>(gObj), cObj->getLeftCollisionBox()) and
             !gObj->isOnGround() and cObj->getLeftCollisionBox().active)
    {
        result = true;
        const_cast<CollisionObject*>(cObj)->setCollisionSideLeft();
    }
    else if (gameObjectCollidedWithPlatform(const_cast<GameObject*>(gObj), cObj->getTopCollisionBox()) and
             cObj->getTopCollisionBox().active)
    {
        result = true;
        const_cast<CollisionObject*>(cObj)->setCollisionSideTop();
    }
    else if (gameObjectCollidedWithPlatform(const_cast<GameObject*>(gObj), cObj->getBottomCollisionBox()) and
             cObj->getBottomCollisionBox().active)
    {
        result = true;
        const_cast<CollisionObject*>(cObj)->setCollisionSideBottom();
    }
    else const_cast<CollisionObject*>(cObj)->setCollisionSideNone();
    return result;
}

bool Collision::rectangularCollisionMethod(const Crate* c, const Weapon* w) const
{
    return weaponCollidedWithCrate(c, w);
}

bool Collision::gameObjectCollidedWithPlatform(GameObject* gObj, const PlatformCollisionBox& cBox) const
{
    CollisionBox gObjCBox;
    if (gObj->type().find("Enemy") != std::string::npos)
    {
        gObjCBox = dynamic_cast<Enemy*>(gObj)->getCollisionBox();
    }
    else if (gObj->type().find("Player") != std::string::npos)
    {
        gObjCBox = dynamic_cast<Player*>(gObj)->getCollisionBox();
    }
    float left1 = gObj->getPosition().getX() + gObjCBox.getXOffset();
    float left2 = cBox.uLeft.getX();
    float right1 = left1 + gObjCBox.getWidth();
    float right2 = cBox.uRight.getX();
    if (right1 < left2) return false;
    if (left1 > right2) return false;

    float top1 = gObj->getPosition().getY() + gObjCBox.getYOffset();
    float top2 = cBox.uLeft.getY();
    float bottom1 = top1 + gObjCBox.getHeight();
    float bottom2 = cBox.lLeft.getY();
    if (bottom1 < top2) return false;
    if (bottom2 < top1) return false;
    return true;
}

bool Collision::weaponCollidedWithCrate(const Crate* c, const Weapon* w) const
{
    CollisionBox cCBox = c->getCollisionBox();
    float left1 = c->getPosition().getX() + cCBox.getXOffset();
    float left2 = w->getPosition().getX();
    float right1 = left1 + cCBox.getWidth();
    float right2 = left2 + w->getWidth();
    if (right1 < left2) return false;
    if (left1 > right2) return false;

    float top1 = c->getPosition().getY() + cCBox.getYOffset();
    float top2 = w->getPosition().getY();
    float bottom1 = top1 + cCBox.getHeight();
    float bottom2 = top2 + w->getHeight();
    if (bottom1 < top2) return false;
    if (bottom2 < top1) return false;
    return true;
}

bool Collision::rectangularCollisionMethod(const GameObject* gObj1, const GameObject* gObj2) const
{
    TextureManager* tM_inst = TextureManager::getInstance();
    float left1 = gObj1->getPosition().getX();
    float left2 = gObj2->getPosition().getX();
    float right1 = left1 + tM_inst->getWidth(gObj1->getSpriteSheet().getTextureID());
    float right2 = left2 + tM_inst->getWidth(gObj2->getSpriteSheet().getTextureID());
    if ( right1 < left2 ) return false;
    if ( left1 > right2 ) return false;

    float top1 = gObj1->getPosition().getY();
    float top2 = gObj2->getPosition().getY();
    float bottom1 = top1 + tM_inst->getHeight(gObj1->getSpriteSheet().getTextureID());
    float bottom2 = top2 + tM_inst->getHeight(gObj2->getSpriteSheet().getTextureID());
    if ( bottom1 < top2 ) return false;
    if ( bottom2 < top1 ) return false;
    return true;
}

bool Collision::perPixelCollisionMethod(const GameObject* gObj, const CollisionObject* cObj) const
{
    if (!rectangularCollisionMethod(gObj, cObj)) return false;
    TextureManager* tM_inst = TextureManager::getInstance();
    // If we got this far, we know that the sprite rectangles intersect!
    Vector2D pos1 = gObj->getPosition();
    Vector2D pos2 = cObj->getPosition();
    // Need to change the position of X to adjust for collision width
    pos2.setX(pos2.getX() + cObj->getCollisionWidth());
    // Need to change the position of Y to adjust for collision height
    pos2.setY(pos2.getY() + cObj->getCollisionHeight());
    Vector2D p1 = pos1 - ViewPort::getInstance()->getPosition();
    Vector2D p2 = pos2 - ViewPort::getInstance()->getPosition();

    int width1 = tM_inst->getWidth(gObj->getSpriteSheet().getTextureID());
    int height1 = tM_inst->getHeight(gObj->getSpriteSheet().getTextureID());

    int width2 = tM_inst->getWidth(cObj->getSpriteSheet().getTextureID()) - cObj->getCollisionWidth();
    int height2 = tM_inst->getHeight(cObj->getSpriteSheet().getTextureID()) - cObj->getCollisionHeight();

    int o1Left = p1.getX();
    int o1Right = o1Left + width1;

    int o2Left = p2.getX() + cObj->getCollisionWidth();
    int o2Right = o2Left + width2;
    std::vector<int> sides;
    sides.reserve(4);
    sides.push_back(o1Left);
    sides.push_back(o1Right);
    sides.push_back(o2Left);
    sides.push_back(o2Right);
    std::sort(sides.begin(), sides.end());


    int o1Up = p1.getY();
    int o1Down = o1Up + height1;
    int o2Up = p2.getY() + cObj->getCollisionHeight();
    int o2Down = o2Up + height2;
    std::vector<int> lids;
    lids.reserve(4);
    lids.push_back(o1Up);
    lids.push_back(o1Down);
    lids.push_back(o2Up);
    lids.push_back(o2Down);
    std::sort(lids.begin(), lids.end());

    SDL_Surface* temp1 = TextureManager::getInstance()->getSurface(gObj->getSpriteSheet().getTextureID());
    SDL_Surface* surface1 = blitSurface(temp1, width1, height1);
    SDL_Surface* temp2 = TextureManager::getInstance()->getSurface(cObj->getSpriteSheet().getTextureID());
    SDL_Surface* surface2 = blitSurface(temp2, width2, height2);

    SDL_LockSurface(surface1);
    SDL_LockSurface(surface2);
    Uint32* pixels1 = static_cast<Uint32*>(surface1->pixels);
    Uint32* pixels2 = static_cast<Uint32*>(surface2->pixels);
    unsigned pixel1;
    unsigned pixel2;
    for (int x = sides[1]; x < sides[2]; ++x)
    {
        for (int y = lids[1]; y < lids[2]; ++y)
        {
            // check pixels in surface for obj1 and surface for obj2!
            int i = x - p1.getX();
            int j = y - p1.getY();
            pixel1 = pixels1[(j * width1) + i];
            i = x - p2.getX();
            j = y - p2.getY();
            pixel2 = pixels2[(j * width2) + i];

            if (isVisible(pixel1, surface1) && isVisible(pixel2, surface2))
            {
                SDL_UnlockSurface(surface1);
                SDL_UnlockSurface(surface2);
                SDL_FreeSurface(surface1);
                SDL_FreeSurface(surface2);
                return true;
            }
        }
    }
    SDL_UnlockSurface(surface1);
    SDL_UnlockSurface(surface2);
    SDL_FreeSurface(surface1);
    SDL_FreeSurface(surface2);

    return false;
}

bool Collision::perPixelCollisionMethod(const GameObject* gObj1, const GameObject* gObj2) const
{
    if (!rectangularCollisionMethod(gObj1, gObj2)) return false;
    TextureManager* tM_inst = TextureManager::getInstance();
    // If we got this far, we know that the sprite rectangles intersect!
    Vector2D pos1 = gObj1->getPosition();
    Vector2D pos2 = gObj2->getPosition();
    Vector2D p1 = pos1 - ViewPort::getInstance()->getPosition();
    Vector2D p2 = pos2 - ViewPort::getInstance()->getPosition();

    int width1 = tM_inst->getWidth(gObj1->getSpriteSheet().getTextureID());
    int height1 = tM_inst->getHeight(gObj1->getSpriteSheet().getTextureID());

    int width2 = tM_inst->getWidth(gObj2->getSpriteSheet().getTextureID());
    int height2 = tM_inst->getHeight(gObj2->getSpriteSheet().getTextureID());

    int o1Left = p1.getX();
    int o1Right = o1Left + width1;

    int o2Left = p2.getX();
    int o2Right = o2Left + width2;
    std::vector<int> sides;
    sides.reserve(4);
    sides.push_back(o1Left);
    sides.push_back(o1Right);
    sides.push_back(o2Left);
    sides.push_back(o2Right);
    std::sort(sides.begin(), sides.end());


    int o1Up = p1.getY();
    int o1Down = o1Up + height1;
    int o2Up = p2.getY();
    int o2Down = o2Up + height2;
    std::vector<int> lids;
    lids.reserve(4);
    lids.push_back(o1Up);
    lids.push_back(o1Down);
    lids.push_back(o2Up);
    lids.push_back(o2Down);
    std::sort(lids.begin(), lids.end());

    SDL_Surface* temp1 = TextureManager::getInstance()->getSurface(gObj1->getSpriteSheet().getTextureID());
    SDL_Surface* surface1 = blitSurface(temp1, width1, height1);
    SDL_Surface* temp2 = TextureManager::getInstance()->getSurface(gObj2->getSpriteSheet().getTextureID());
    SDL_Surface* surface2 = blitSurface(temp2, width2, height2);

    SDL_LockSurface(surface1);
    SDL_LockSurface(surface2);
    Uint32* pixels1 = static_cast<Uint32 *>(surface1->pixels);
    Uint32* pixels2 = static_cast<Uint32 *>(surface2->pixels);
    unsigned pixel1;
    unsigned pixel2;
    for (int x = sides[1]; x < sides[2]; ++x)
    {
        for (int y = lids[1]; y < lids[2]; ++y)
        {
            // check pixels in surface for obj1 and surface for obj2!
            int i = x - p1.getX();
            int j = y - p1.getY();
            pixel1 = pixels1[(j * width1) + i];
            i = x - p2.getX();
            j = y - p2.getY();
            pixel2 = pixels2[(j * width2) + i];

            if (isVisible(pixel1, surface1) && isVisible(pixel2, surface2))
            {
                SDL_UnlockSurface(surface1);
                SDL_UnlockSurface(surface2);
                SDL_FreeSurface(surface1);
                SDL_FreeSurface(surface2);
                return true;
            }
        }
    }
    SDL_UnlockSurface(surface1);
    SDL_UnlockSurface(surface2);
    SDL_FreeSurface(surface1);
    SDL_FreeSurface(surface2);

    return false;
}

SDL_Surface* Collision::blitSurface(SDL_Surface* surface, int width, int height, unsigned int row, unsigned int col) const
{
    const auto* fmt = surface->format;
    SDL_Surface* sub = SDL_CreateRGBSurface(0, width, height, fmt->BitsPerPixel, fmt->Rmask,
                                            fmt->Gmask, fmt->Bmask, fmt->Amask);
    // Get the old mode
    SDL_BlendMode oldBlendMode;
    SDL_GetSurfaceBlendMode(surface, &oldBlendMode);

    // Set the new mode so copying the source won't change the source
    SDL_SetSurfaceBlendMode(surface, SDL_BLENDMODE_NONE);

    SDL_Rect srcRect = {(static_cast<int>(col) * width), (static_cast<int>(row - 1) * height), width, height};
    SDL_Rect dstRect = {0, 0, width, height};
    int flag = SDL_BlitSurface(surface, &srcRect, sub, &dstRect);
    if (flag < 0)
    {
        std::cerr << "SDL_BlitSurface failed!" << std::endl;
        return nullptr;
    }
    SDL_SetSurfaceBlendMode(surface, oldBlendMode);
    return sub;
}
