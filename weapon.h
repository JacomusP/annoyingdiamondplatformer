//
// Created by jacomus on 5/21/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_WEAPON_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_WEAPON_H


#include "gameObject.h"

class WeaponCarrier;

class Weapon : public GameObject
{
public:
    Weapon(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
               const unsigned int numRows, const unsigned int numCols, const std::string& name, const int frameInterval,
               const unsigned int power);
    Weapon(const Weapon& w);
    Weapon& operator=(const Weapon& w);
    virtual ~Weapon();
    virtual void update(Uint32 ticks, const GameState& gState);
    virtual void draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const;
    virtual std::string type() const;
    virtual bool checkForCollision(const GameObject& gObj);
    bool isPickedUp() const { return m_bPickedUp; }
    bool isEquipped() const { return m_bEquipped; }
    unsigned int getPower() const { return m_power; }
    const std::string& getPlayerName() const;
    const WeaponCarrier* getOwner() const { return m_owner; }
    bool inBox() const { return m_bInBox; }
    void setOwner(const WeaponCarrier* p);
    void setPower(const unsigned int power);
    void setInBox(const bool inBox);
    void pickUp();
    void equip();
    void unequip();
private:
    bool m_bPickedUp;
    bool m_bEquipped;
    bool m_bInBox;
    unsigned int m_power;
    WeaponCarrier* m_owner;
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_WEAPON_H
