//
// Created by jacomus on 5/21/17.
//

#include "weapon.h"
#include "weaponCarrier.h"
#include "viewPort.h"
#include "textureManager.h"
#include "gameData.h"

Weapon::Weapon(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
               const unsigned int numRows, const unsigned int numCols, const std::string& name, const int frameInterval,
               const unsigned int power) :
    GameObject(pos, vel, accel, textureID, numRows, numCols, name, frameInterval, 0),
    m_bPickedUp(false), m_bEquipped(false), m_bInBox(false), m_power(power), m_owner(nullptr)
{}

Weapon::Weapon(const Weapon& w) :
    GameObject(w), m_bPickedUp(w.m_bPickedUp), m_bEquipped(w.m_bEquipped), m_bInBox(w.m_bInBox), m_power(w.m_power),
    m_owner(w.m_owner)
{}

Weapon& Weapon::operator=(const Weapon& w)
{
    if (this == &w)
    {
        return *this;
    }
    GameObject::operator=(w);
    m_bPickedUp = w.m_bPickedUp;
    m_bEquipped = w.m_bEquipped;
    m_bInBox = w.m_bInBox;
    m_power = w.m_power;
    m_owner = w.m_owner;
    return *this;
}

Weapon::~Weapon()
{
    m_owner = nullptr;
    std::cout << "Weapon destructor" << std::endl;
}

const std::string& Weapon::getPlayerName() const
{
    return m_owner->getName();
}

void Weapon::update(Uint32 ticks, const GameState& gState)
{
    // Only advance to the next frame of the animation if the weapon is picked up
    if (m_bPickedUp && m_bEquipped)
    {
        //std::cout << "Advancing to next frame of animation!" << std::endl;
        advanceToNextFrameOfAnimation(ticks);
    }
    if (m_owner != nullptr)
    {
        setVelocity(m_owner->getVelocity());
    }
    if (gState.getStateID() == "PLAY")
    {
        Vector2D pos = getPosition();
        // Need to bounce the player off the edges of the world
        int difference = GameData::getInstance()->getJsonInt("GameData.playState.world.width")
                         - TextureManager::getInstance()->getWidth(getSpriteSheet().getTextureID());
        if (pos.getX() > difference)
        {
            pos.setX(difference);
        }
        else if (pos.getX() < 0)
        {
            pos.setX(0);
        }
        if (pos.getY() > 416)
        {
            pos.setY(416);
        }
        else if (pos.getY() < 0)
        {
            pos.setY(0);
        }
    }
    if (m_owner != nullptr)
    {
        SpriteSheet pSprSht = m_owner->getSpriteSheet();
        if (getSpriteSheet().getCurrentAnimation() != pSprSht.getCurrentAnimation())
        {
            setCurrentAnimation(pSprSht.getCurrentAnimation());
        }
    }
    GameObject::update(ticks, gState);
}

void Weapon::draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const
{
    float x, y;
    if (m_bPickedUp) // Get the player's position who picked up the weapon
    {
        x = m_owner->getPosition().getX();
        y = m_owner->getPosition().getY();
    }
    else // Get the weapons positions since it isn't picked up yet
    {
        x = getPosition().getX();
        y = getPosition().getY();
    }
    x -= ViewPort::getInstance()->getX();
    y -= ViewPort::getInstance()->getY();

    if (!m_bInBox)
    {
        TextureManager::getInstance()->drawFrame(getSpriteSheet().getTextureID(),
                                                 static_cast<int>(x), static_cast<int>(y),
                                                 getSpriteSheet().getCurrentRow(),
                                                 getSpriteSheet().getCurrentFrame() % getSpriteSheet().getColumns(),
                                                 const_cast<SDL_Renderer*>(pRenderer), flip);
    }
}

std::string Weapon::type() const
{
    return "Weapon";
}

bool Weapon::checkForCollision(const GameObject& gObj)
{
    gObj.type();
    return false;
}

void Weapon::setOwner(const WeaponCarrier* p)
{
    m_owner = const_cast<WeaponCarrier*>(p);
}

void Weapon::setPower(const unsigned int power)
{
    m_power = power;
}

void Weapon::setInBox(const bool inBox)
{
    m_bInBox = inBox;
}

void Weapon::pickUp()
{
    m_bPickedUp = true;
}

void Weapon::equip()
{
    m_bEquipped = true;
}

void Weapon::unequip()
{
    m_bEquipped = false;
}
