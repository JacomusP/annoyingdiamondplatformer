//
// Created by jacomus on 6/20/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_VIEWPORT_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_VIEWPORT_H

#include "gameObject.h"
#include "vector2D.h"

class ViewPort
{
public:
    static ViewPort* getInstance();
    static void destroy();
    void draw() const;
    void update();

    const Vector2D getPosition() const;
    float getX() const;
    void setX(float x);
    float getY() const;
    void setY(float y);

    void setObjectToTrack(const GameObject* gObj);
    const GameObject* getObjectToTrack() const;

private:
    Vector2D m_position;
    int m_worldWidth;
    int m_worldHeight;
    int m_width;
    int m_height;
    int m_objWidth;
    int m_objHeight;
    const GameObject* m_objectToTrack;
    static ViewPort* myInstance;

    ViewPort();
    ~ViewPort();

    // Explicitly disallow those functions we do not want the compiler to generate for us
    ViewPort(const ViewPort& vP) = delete;
    ViewPort& operator=(const ViewPort& vP) = delete;
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_VIEWPORT_H
