//
// Created by jacomus on 5/2/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_GAMEOVERSTATE_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_GAMEOVERSTATE_H

#include <vector>
#include <string>
#include "gameState.h"
#include "gameObject.h"

class GameOverState : public GameState
{
public:
    GameOverState(const bool won);
    GameOverState(const GameOverState& gOS) = delete;
    GameOverState& operator=(const GameOverState& gOS) = delete;
    virtual ~GameOverState();
    virtual void update(Uint32 ticks);
    virtual void render();
    virtual bool onEnter();
    virtual bool onExit();
    virtual std::string getStateID() const;
private:
    std::vector<GameObject*> m_gameObjects;
    bool m_won;
    static const std::string s_gameOverID;

    // Methods
    bool loadTextures();

    // call back functions
    static void s_returnToMain();
    static void s_restartPlay();
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_GAMEOVERSTATE_H
