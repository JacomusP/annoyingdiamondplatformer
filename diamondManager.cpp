//
// Created by jacomus on 4/4/17.
//

#include <cstdlib>
#include <ctime>
#include "diamondManager.h"
#include "gameData.h"

DiamondManager::DiamondManager() : m_smallDiamonds(),
                                   m_mediumDiamonds(), m_largeDiamonds()
{
    srand(time(nullptr));
}

DiamondManager::~DiamondManager()
{
    for (Diamond* dS : m_smallDiamonds)
    {
        delete dS;
    }
    for (Diamond* dM : m_mediumDiamonds)
    {
        delete dM;
    }
    for (Diamond* dL : m_largeDiamonds)
    {
        delete dL;
    }
}

void DiamondManager::drawSmall(const SDL_Renderer* pRenderer, SDL_RendererFlip flip)
{
    for (unsigned int i = 0; i < m_smallDiamonds.size(); i++)
    {
        m_smallDiamonds[i]->draw(pRenderer, flip);
    }
}

void DiamondManager::drawMedium(const SDL_Renderer* pRenderer, SDL_RendererFlip flip)
{
    for (unsigned int i = 0; i < m_mediumDiamonds.size(); i++)
    {
        m_mediumDiamonds[i]->draw(pRenderer, flip);
    }
}

void DiamondManager::drawLarge(const SDL_Renderer* pRenderer, SDL_RendererFlip flip)
{
    for (unsigned int i = 0; i < m_largeDiamonds.size(); i++)
    {
        m_largeDiamonds[i]->draw(pRenderer, flip);
    }
}

void DiamondManager::update(Uint32 ticks, const GameState& gS)
{
    updateSmall(ticks, gS);
    updateMedium(ticks, gS);
    updateLarge(ticks, gS);
}

void DiamondManager::updateSmall(Uint32 ticks, const GameState& gS)
{
    for (unsigned int i = 0; i < m_smallDiamonds.size(); i++)
    {
        m_smallDiamonds[i]->update(ticks, gS);
    }
}

void DiamondManager::updateMedium(Uint32 ticks, const GameState& gS)
{
    for (unsigned int i = 0; i < m_mediumDiamonds.size(); i++)
    {
        m_mediumDiamonds[i]->update(ticks, gS);
    }
}

void DiamondManager::updateLarge(Uint32 ticks, const GameState& gS)
{
    for (unsigned int i = 0; i < m_largeDiamonds.size(); i++)
    {
        m_largeDiamonds[i]->update(ticks, gS);
    }
}

void DiamondManager::initializeDiamonds()
{
    GameData* inst = GameData::getInstance();
    int numS = inst->getJsonInt("GameData.playState.textures.Diamond.numSmall");
    int numM = inst->getJsonInt("GameData.playState.textures.Diamond.numMedium");
    int numL = inst->getJsonInt("GameData.playState.textures.Diamond.numLarge");
    int fI = inst->getJsonUInt("GameData.playState.textures.Diamond.frameInterval");
    unsigned int rows = inst->getJsonUInt("GameData.playState.textures.Diamond.rows");
    unsigned int cols = inst->getJsonUInt("GameData.playState.textures.Diamond.cols");
    std::string small_tID = inst->getJsonString("GameData.playState.textures.Diamond.small.id");
    std::string medium_tID = inst->getJsonString("GameData.playState.textures.Diamond.medium.id");
    std::string large_tID = inst->getJsonString("GameData.playState.textures.Diamond.large.id");
    std::string small_type = "smallDiamond";
    std::string medium_type = "mediumDiamond";
    std::string large_type = "largeDiamond";
    int wWidth = inst->getJsonInt("GameData.playState.world.width");
    int wHeight = inst->getJsonInt("GameData.playState.world.height");

    for (int i = 0; i < numS; i++)
    {
        Diamond* sD = new Diamond(Vector2D(rand() % wWidth, rand() % wHeight), Vector2D(), Vector2D(), small_tID, rows,
                                  cols, inst->getJsonString("GameData.playState.gameObjects.SmallDiamond.name"), fI, 0,
                                  small_type);
        sD->addAnimation(inst->getJsonString("GameData.playState.animations.diamond.diamond.name"),
                         inst->getJsonUInt("GameData.playState.animations.diamond.diamond.row"),
                         inst->getJsonUInt("GameData.playState.animations.diamond.diamond.rowSpan"),
                         inst->getJsonUInt("GameData.playState.animations.diamond.diamond.startPos"),
                         inst->getJsonUInt("GameData.playState.animations.diamond.diamond.endPos"));
        sD->setCurrentAnimation(inst->getJsonString("GameData.playState.animations.diamond.diamond.name"));
        m_smallDiamonds.push_back(sD);

    }
    for (int i = 0; i < numM; i++)
    {
        Diamond* mD = new Diamond(Vector2D(rand() % wWidth, rand() % wHeight), Vector2D(), Vector2D(), medium_tID, rows,
                                  cols, inst->getJsonString("GameData.playState.animations.diamond.diamond.name"), fI,
                                  0,
                                  medium_type);
        mD->addAnimation(inst->getJsonString("GameData.playState.animations.diamond.diamond.name"),
                         inst->getJsonUInt("GameData.playState.animations.diamond.diamond.row"),
                         inst->getJsonUInt("GameData.playState.animations.diamond.diamond.rowSpan"),
                         inst->getJsonUInt("GameData.playState.animations.diamond.diamond.startPos"),
                         inst->getJsonUInt("GameData.playState.animations.diamond.diamond.endPos"));
        mD->setCurrentAnimation(inst->getJsonString("GameData.playState.animations.diamond.diamond.name"));
        m_mediumDiamonds.push_back(mD);
    }
    for (int i = 0; i < numL; i++)
    {
        Diamond* lD = new Diamond(Vector2D(rand() % wWidth, rand() % wHeight), Vector2D(), Vector2D(), large_tID, rows,
                                  cols, inst->getJsonString("GameData.playState.animations.diamond.diamond.name"), fI,
                                  0,
                                  large_type);
        lD->addAnimation(inst->getJsonString("GameData.playState.animations.diamond.diamond.name"),
                         inst->getJsonUInt("GameData.playState.animations.diamond.diamond.row"),
                         inst->getJsonUInt("GameData.playState.animations.diamond.diamond.rowSpan"),
                         inst->getJsonUInt("GameData.playState.animations.diamond.diamond.startPos"),
                         inst->getJsonUInt("GameData.playState.animations.diamond.diamond.endPos"));
        lD->setCurrentAnimation(inst->getJsonString("GameData.playState.animations.diamond.diamond.name"));
        m_largeDiamonds.push_back(lD);
    }
}
