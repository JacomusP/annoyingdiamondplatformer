//
// Created by jacomus on 6/9/17.
//

#include "playState.h"
#include "game.h"
#include "textureManager.h"
#include "gameData.h"
#include "viewPort.h"
#include "gameOverState.h"
#include "pauseState.h"
#include "inputHandler.h"
#include "textureManagerException.h"
#include "platformCreator.h"
#include "crateCreator.h"
#include "enemyCreator.h"
#include "playerCreator.h"
#include "weaponCreator.h"

const std::string PlayState::s_playID = "PLAY";

PlayState::PlayState() :
    GameState(), m_skyMountains(nullptr), m_cliffWaterfalls(nullptr),
    m_weapons(), m_player(nullptr), m_enemies(), m_platforms(), m_animations(), m_hud(),
    m_diamondManager(new DiamondManager()), m_numEnemiesDead(0), m_gameObjectFactory(), m_randomWorldGenerator(1, 100),
    m_playerCollidedWithPlatform(false), m_enemiesCollidedWithPlatform(), m_crates(), m_numberOfEnemies(0),
    m_numberOfWeapons(0)
{
    m_gameObjectFactory.registerType("botPlat", new PlatformCreator(1, "", "",
                                                                    "GameData.playState.textures.BottomPlatform",
                                                                    "botPlat"));
    m_gameObjectFactory.registerType("floatPlat", new PlatformCreator(1, "", "",
                                                                      "GameData.playState.textures.FloatingPlatform",
                                                                      "floatPlat"));
    m_gameObjectFactory.registerType("woodenCrate", new CrateCreator(1, "", "",
                                                                     "GameData.playState.textures.woodenCrate",
                                                                     "woodenCrate"));
    m_gameObjectFactory.registerType("enemy", new EnemyCreator(1, "",
                                                               "GameData.playState.gameObjects.Enemy",
                                                               "GameData.playState.textures.torchGuy", "enemy"));
    m_gameObjectFactory.registerType("player", new PlayerCreator(-1, "", "GameData.playState.gameObjects.Nemesis",
                                                                 "GameData.playState.textures.nemesis"));
    m_gameObjectFactory.registerType("sword", new WeaponCreator(-1, "", "GameData.playState.gameObjects.Sword",
                                                                "GameData.playState.textures.sword"));
    m_gameObjectFactory.registerType("spear", new WeaponCreator(-1, "", "GameData.playState.gameObjects.Spear",
                                                                "GameData.playState.textures.spear"));
    m_gameObjectFactory.registerType("battleAxe", new WeaponCreator(-1, "", "GameData.playState.gameObjects.BattleAxe",
                                                                "GameData.playState.textures.battleAxe"));
    m_gameObjectFactory.registerType("glaive", new WeaponCreator(-1, "", "GameData.playState.gameObjects.Glaive",
                                                                "GameData.playState.textures.glaive"));
    m_gameObjectFactory.registerType("scimitar", new WeaponCreator(-1, "", "GameData.playState.gameObjects.Scimitar",
                                                                "GameData.playState.textures.scimitar"));
    m_gameObjectFactory.registerType("spindar", new WeaponCreator(-1, "", "GameData.playState.gameObjects.Spindar",
                                                                "GameData.playState.textures.spindar"));
}

PlayState::~PlayState()
{
    onExit();
}

void PlayState::update(Uint32 ticks)
{
    CollisionObject platPlayerCollidedWith;
    resetCollidedWithPlatformVariables();
    if (GameState::getExited()) return;
    applyGravity();
    if (GameState::getExited()) return;
    m_player->update(ticks, *this);
    if (GameState::getExited()) return;
    auto it = m_enemies.begin();
    while (it != m_enemies.end())
    {
        (*it)->update(ticks, *this);
        it++;
    }
    if (GameState::getExited()) return;
    auto cIt = m_crates.begin();
    while (cIt != m_crates.end())
    {
        (*cIt)->update(ticks, *this);
        cIt++;
    }
    if (GameState::getExited()) return;
    auto wIt = m_weapons.begin();
    std::vector<Weapon*>::iterator pWIt = m_weapons.end();
    while (wIt != m_weapons.end())
    {
        if (!(*wIt)->isPickedUp() and !(*wIt)->inBox())
        {
            if (m_player->checkForCollision(*(*wIt)))
            {
                pWIt = wIt;
                m_player->pickupWeapon((*wIt)->getName(), (*wIt));
                // The player is invincible by default because they do not start with a weapon picked up.
                // In order to make the game more challenging invincibility will be set to false every time they
                // pick up a weapon
                if (m_player->isInvincible())
                {
                    m_player->setInvincibility(false);
                }
            }
        }
        wIt++;
    }
    // Remove weapon from m_weapons vector so we don't have 2 copies of the pointer
    if (pWIt != m_weapons.end())
    {
        m_weapons.erase(pWIt);
    }
    if (GameState::getExited()) return;
    auto pIt = m_platforms.begin();
    while (pIt != m_platforms.end())
    {
        if ((*pIt)->checkForCollision(*m_player))
        {
            m_playerCollidedWithPlatform = true;
            platPlayerCollidedWith = *(*pIt);
            checkWhichSideGameObjectCollidedWith(m_player, (*pIt));
        }
        auto eIt = m_enemies.begin();
        auto epIt = m_enemiesCollidedWithPlatform.begin();
        while (eIt != m_enemies.end())
        {
            if ((*pIt)->checkForCollision(*(*eIt)))
            {
                checkWhichSideGameObjectCollidedWith((*eIt), (*pIt));
                (*epIt) = true;
            }
            eIt++;
            epIt++;
        }
        pIt++;
    }
    if (m_playerCollidedWithPlatform)
    {
        if (platPlayerCollidedWith.whichCollisionSide("TOP"))
        {
            m_player->setOnGround(m_playerCollidedWithPlatform);
        }
        else
        {
            m_player->setOnGround(false);
        }
    }
    else m_player->setOnGround(false);

    auto enIt = m_enemies.begin();
    auto epIt = m_enemiesCollidedWithPlatform.begin();
    while (epIt != m_enemiesCollidedWithPlatform.end())
    {
        (*enIt)->setOnGround((*epIt));
        enIt++;
        epIt++;
    }

    if (GameState::getExited()) return;
    auto eIt = m_enemies.begin();
    while (eIt != m_enemies.end())
    {
        if ((*eIt)->getBulletPool().collidedWith(*dynamic_cast<GameObject*>(m_player)))
        {
            std::cout << "Bullet collided with player!" << std::endl;
            if (!m_player->getEquippedWeaponName().empty() && !m_player->isInvincible())
            {
                m_player->takeDamage((*eIt)->getBulletPool().getBulletPower());
            }
        }
        if (m_player->getCurrentAnimation() == "attack" or m_player->getCurrentAnimation() == "attackLeft")
        {
            if (!m_player->getEquippedWeaponName().empty())
            {
                if ((*eIt)->checkForCollision(*dynamic_cast<GameObject*>(m_player->getEquippedWeapon())))
                {
                    std::cout << (*eIt)->getName() << " was hit by " << m_player->getName() << "'s "
                              << m_player->getEquippedWeaponName() << std::endl;
                    (*eIt)->takeDamage(m_player->getWeaponPower());
                }
                (*eIt)->getBulletPool().collidedWithWeapon(*(m_player->getEquippedWeapon()));
            }
        }
        eIt++;
    }
    if (GameState::getExited()) return;
    cIt = m_crates.begin();
    while (cIt != m_crates.end())
    {
        if (m_player->getCurrentAnimation() == "attack" or m_player->getCurrentAnimation() == "attackLeft")
        {
            if (!m_player->getEquippedWeaponName().empty())
            {
                if ((*cIt)->checkForCollision(*dynamic_cast<GameObject*>(m_player->getEquippedWeapon())))
                {
                    std::cout << (*cIt)->getName() << " was hit by " << m_player->getName() << "'s "
                              << m_player->getEquippedWeaponName() << std::endl;
                    (*cIt)->takeDamage(m_player->getWeaponPower());
                }
            }
        }
        if((*cIt)->isExploded())
        {
            delete (*cIt);
            cIt = m_crates.erase(cIt);
        }
        cIt++;
    }
    if (GameState::getExited()) return;
    m_diamondManager->update(ticks, *this);
    if (GameState::getExited()) return;
    m_cliffWaterfalls->update(ticks);
    if (GameState::getExited()) return;
    ViewPort::getInstance()->update();
    if (GameState::getExited()) return;
    checkInput();
    checkIfAllEnemiesDead();
    checkIfPlayerIsDead();
}

void PlayState::render()
{
    if (GameState::getExited()) return;
    m_skyMountains->draw();
    if (GameState::getExited()) return;
    m_diamondManager->drawSmall(Game::getInstance()->getRenderer(), SDL_FLIP_NONE);
    if (GameState::getExited()) return;
    m_cliffWaterfalls->draw();
    if (GameState::getExited()) return;
    m_diamondManager->drawMedium(Game::getInstance()->getRenderer(), SDL_FLIP_NONE);
    if (GameState::getExited()) return;
    auto pIt = m_platforms.begin();
    while (pIt != m_platforms.end())
    {
        (*pIt)->draw(Game::getInstance()->getRenderer(), SDL_FLIP_NONE);
        pIt++;
    }
    if (GameState::getExited()) return;
    m_player->draw(Game::getInstance()->getRenderer(), SDL_FLIP_NONE);
    if (GameState::getExited()) return;
    auto eIt = m_enemies.begin();
    while (eIt != m_enemies.end())
    {
        (*eIt)->draw(Game::getInstance()->getRenderer(), SDL_FLIP_NONE);
        eIt++;
    }
    if (GameState::getExited()) return;
    auto wIt = m_weapons.begin();
    while (wIt != m_weapons.end())
    {
        if (!(*wIt)->isPickedUp())
        {
            (*wIt)->draw(Game::getInstance()->getRenderer(), SDL_FLIP_NONE);
        }
        wIt++;
    }
    if (GameState::getExited()) return;
    auto cIt = m_crates.begin();
    while (cIt != m_crates.end())
    {
        (*cIt)->draw(Game::getInstance()->getRenderer(), SDL_FLIP_NONE);
        cIt++;
    }
    if (GameState::getExited()) return;
    m_diamondManager->drawLarge(Game::getInstance()->getRenderer(), SDL_FLIP_NONE);
    if (GameState::getExited()) return;
    TextureManager::getInstance()->draw(GameData::getInstance()->getJsonString("GameData.playState.textures.nameLabel.id"),
                                        10, 436, const_cast<SDL_Renderer*>(Game::getInstance()->getRenderer()));
    if (GameState::getExited()) return;
    m_hud.draw();
    if (GameState::getExited()) return;
    ViewPort::getInstance()->draw();
}

bool PlayState::onEnter()
{
    GameData* inst = GameData::getInstance();
    initializeEnemyAnimations();
    initializePlayerAnimations();
    std::vector<AnimationInfo> enemy = m_animations["enemy"];
    std::vector<AnimationInfo> player = m_animations["player"];
    // Load the textures into the texture manager
    if (!loadTextures())
    {
        std::cerr << "playState - Problem loading textures!" << std::endl;
        return false;
    }
    try
    {
        GameState::setExited(false);
        // add the sky background object
        m_skyMountains = new World(inst->getJsonString("GameData.playState.textures.skyMountains.id"),
                                   inst->getJsonInt("GameData.playState.worlds.SkyMountains.factor"),
                                   inst->getJsonInt("GameData.playState.worlds.SkyMountains.width"),
                                   inst->getJsonInt("GameData.playState.worlds.SkyMountains.height"),
                                   inst->getJsonInt("GameData.playState.world.width"),
                                   inst->getJsonInt("GameData.playState.world.height"));
        std::cout << "Added skyMountains world object!" << std::endl;
        // add the mountains background object
        m_cliffWaterfalls = new AnimatedWorld(inst->getJsonString("GameData.playState.textures.waterfallCliff.id"),
                                              inst->getJsonInt("GameData.playState.animatedWorlds.WaterfallCliff.factor"),
                                              inst->getJsonInt("GameData.playState.animatedWorlds.WaterfallCliff.width"),
                                              inst->getJsonInt("GameData.playState.animatedWorlds.WaterfallCliff.height"),
                                              inst->getJsonInt("GameData.playState.world.width"),
                                              inst->getJsonInt("GameData.playState.world.height"),
                                              inst->getJsonInt("GameData.playState.animatedWorlds.WaterfallCliff.frameInterval"),
                                              inst->getJsonUInt("GameData.playState.textures.waterfallCliff.rows"),
                                              inst->getJsonUInt("GameData.playState.textures.waterfallCliff.cols"),
                                              inst->getJsonString("GameData.playState.animations.waterfallCliff.waterfall_torch.name"),
                                              inst->getJsonUInt("GameData.playState.animations.waterfallCliff.waterfall_torch.row"),
                                              inst->getJsonUInt("GameData.playState.animations.waterfallCliff.waterfall_torch.rowSpan"),
                                              inst->getJsonUInt("GameData.playState.animations.waterfallCliff.waterfall_torch.startPos"),
                                              inst->getJsonUInt("GameData.playState.animations.waterfallCliff.waterfall_torch.endPos"));
        std::cout << "Added cliff waterfalls animated world object!" << std::endl;
        // add the bottom platforms
        if (!m_randomWorldGenerator.generateWorld(m_platforms, &m_player, m_enemies, m_crates, m_weapons, m_gameObjectFactory))
        {
            std::cout << "PlayState ERROR - Couldn't generate the platforms and the player!" << std::endl;
        }
        m_diamondManager->initializeDiamonds();
        // add the enemy object animations
        for (Enemy* e : m_enemies)
        {
            for (AnimationInfo aInfo : enemy)
            {
                e->addAnimation(aInfo.name, aInfo.row, aInfo.rowSpan, aInfo.startPos, aInfo.endPos);
            }
            e->setCurrentAnimation(enemy[0].name);
            m_enemiesCollidedWithPlatform.push_back(false);
        }
        std::cout << "PlayState - Added Enemies" << std::endl;

        // add the animations to the player object
        for (AnimationInfo aInfo : player)
        {
            m_player->addAnimation(aInfo.name, aInfo.row, aInfo.rowSpan, aInfo.startPos, aInfo.endPos);
        }
        m_player->setCurrentAnimation(player[0].name);

        // Add the weapons
        for (AnimationInfo aInfo : player)
        {
            for (Weapon* w : m_weapons)
            {
                w->addAnimation(aInfo.name, aInfo.row, aInfo.rowSpan, aInfo.startPos, aInfo.endPos);
            }
        }

        // Add the Enemy as an Observer to Player
        for (Enemy* e : m_enemies)
        {
            m_player->Attach(e);
        }
        ViewPort::getInstance()->setObjectToTrack(dynamic_cast<GameObject*>(m_player));
        m_numberOfEnemies = m_enemies.size();
        m_numberOfWeapons = m_weapons.size();
    }
    catch (const boost::property_tree::ptree_error& e)
    {
        std::cerr << "playState - Error Reading JSON data! " << e.what() << std::endl;
        return false;
    }
    std::cout << "entering PlayState" << std::endl;
    return true;
}

bool PlayState::onExit()
{
    GameData* inst = GameData::getInstance();
    if (!GameState::getExited())
    {
        delete m_player;
        for (Platform* plat : m_platforms)
        {
            delete plat;
        }
        m_platforms.clear();
        for (Weapon* wep : m_weapons)
        {
            delete wep;
        }
        m_weapons.clear();
        for (Enemy* e : m_enemies)
        {
            delete e;
        }
        m_enemies.clear();
        for (Crate* c : m_crates)
        {
            delete c;
        }
        m_crates.clear();
        delete m_skyMountains;
        delete m_cliffWaterfalls;
        delete m_diamondManager;
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.playState.textures.skyMountains.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.playState.textures.waterfallCliff.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.playState.textures.BottomPlatform.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.playState.textures.FloatingPlatform.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.playState.textures.torchGuy.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.playState.textures.nemesis.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.playState.textures.Diamond.small.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.playState.textures.Diamond.medium.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.playState.textures.Diamond.large.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.playState.textures.sword.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.playState.textures.spear.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.playState.textures.battleAxe.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.playState.textures.glaive.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.playState.textures.scimitar.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.playState.textures.spindar.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.playState.textures.bullet.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.playState.textures.woodenCrate.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.playState.textures.nameLabel.id"));
    }
    std::cout << "exiting PlayState" << std::endl;
    GameState::setExited(true);
    return true;
}

std::string PlayState::getStateID() const
{
    return s_playID;
}

void PlayState::checkInput()
{
    InputHandler* iH = InputHandler::getInstance();
    if (iH->isKeyDown(SDL_SCANCODE_F1))
    {
        m_hud.toggleDraw();
    }
    if (iH->isKeyDown(SDL_SCANCODE_ESCAPE))
    {
        Game::getInstance()->getStateMachine()->pushState("PAUSE", new PauseState());
    }
}

bool PlayState::loadTextures()
{
    try
    {
        GameData* inst = GameData::getInstance();
        TextureManager* tm_inst = TextureManager::getInstance();
        auto renderer = const_cast<SDL_Renderer*>(Game::getInstance()->getRenderer());
        tm_inst->load(inst->getJsonString("GameData.playState.textures.skyMountains.path"),
                      inst->getJsonString("GameData.playState.textures.skyMountains.id"),
                      inst->getJsonInt("GameData.playState.worlds.SkyMountains.width"),
                      inst->getJsonInt("GameData.playState.worlds.SkyMountains.height"), renderer);
        std::cout << "Loaded sky background texture!" << std::endl;
        tm_inst->load(inst->getJsonString("GameData.playState.textures.waterfallCliff.path"),
                      inst->getJsonString("GameData.playState.textures.waterfallCliff.id"),
                      inst->getJsonInt("GameData.playState.animatedWorlds.WaterfallCliff.width"),
                      inst->getJsonInt("GameData.playState.animatedWorlds.WaterfallCliff.height"), renderer);
        std::cout << "Loaded mountains background texture!" << std::endl;
        tm_inst->load(inst->getJsonString("GameData.playState.textures.BottomPlatform.path"),
                      inst->getJsonString("GameData.playState.textures.BottomPlatform.id"),
                      inst->getJsonInt("GameData.playState.textures.BottomPlatform.width"),
                      inst->getJsonInt("GameData.playState.textures.BottomPlatform.height"), renderer);
        tm_inst->load(inst->getJsonString("GameData.playState.textures.FloatingPlatform.path"),
                      inst->getJsonString("GameData.playState.textures.FloatingPlatform.id"),
                      inst->getJsonInt("GameData.playState.textures.FloatingPlatform.width"),
                      inst->getJsonInt("GameData.playState.textures.FloatingPlatform.height"), renderer);
        tm_inst->load(inst->getJsonString("GameData.playState.textures.torchGuy.path"),
                      inst->getJsonString("GameData.playState.textures.torchGuy.id"),
                      inst->getJsonInt("GameData.playState.gameObjects.Enemy.width"),
                      inst->getJsonInt("GameData.playState.gameObjects.Enemy.height"), renderer);
        tm_inst->load(inst->getJsonString("GameData.playState.textures.nemesis.path"),
                      inst->getJsonString("GameData.playState.textures.nemesis.id"),
                      inst->getJsonInt("GameData.playState.gameObjects.Nemesis.width"),
                      inst->getJsonInt("GameData.playState.gameObjects.Nemesis.height"), renderer);
        tm_inst->load(inst->getJsonString("GameData.playState.textures.Diamond.small.path"),
                      inst->getJsonString("GameData.playState.textures.Diamond.small.id"),
                      inst->getJsonInt("GameData.playState.gameObjects.SmallDiamond.width"),
                      inst->getJsonInt("GameData.playState.gameObjects.SmallDiamond.height"), renderer);
        tm_inst->load(inst->getJsonString("GameData.playState.textures.Diamond.medium.path"),
                      inst->getJsonString("GameData.playState.textures.Diamond.medium.id"),
                      inst->getJsonInt("GameData.playState.gameObjects.MediumDiamond.width"),
                      inst->getJsonInt("GameData.playState.gameObjects.MediumDiamond.height"), renderer);
        tm_inst->load(inst->getJsonString("GameData.playState.textures.Diamond.large.path"),
                      inst->getJsonString("GameData.playState.textures.Diamond.large.id"),
                      inst->getJsonInt("GameData.playState.gameObjects.LargeDiamond.width"),
                      inst->getJsonInt("GameData.playState.gameObjects.LargeDiamond.height"), renderer);
        tm_inst->load(inst->getJsonString("GameData.playState.textures.sword.path"),
                      inst->getJsonString("GameData.playState.textures.sword.id"),
                      inst->getJsonInt("GameData.playState.gameObjects.Sword.width"),
                      inst->getJsonInt("GameData.playState.gameObjects.Sword.height"), renderer);
        tm_inst->load(inst->getJsonString("GameData.playState.textures.spear.path"),
                      inst->getJsonString("GameData.playState.textures.spear.id"),
                      inst->getJsonInt("GameData.playState.gameObjects.Spear.width"),
                      inst->getJsonInt("GameData.playState.gameObjects.Spear.height"), renderer);
        tm_inst->load(inst->getJsonString("GameData.playState.textures.battleAxe.path"),
                      inst->getJsonString("GameData.playState.textures.battleAxe.id"),
                      inst->getJsonInt("GameData.playState.gameObjects.BattleAxe.width"),
                      inst->getJsonInt("GameData.playState.gameObjects.BattleAxe.height"), renderer);
        tm_inst->load(inst->getJsonString("GameData.playState.textures.glaive.path"),
                      inst->getJsonString("GameData.playState.textures.glaive.id"),
                      inst->getJsonInt("GameData.playState.gameObjects.Glaive.width"),
                      inst->getJsonInt("GameData.playState.gameObjects.Glaive.height"), renderer);
        tm_inst->load(inst->getJsonString("GameData.playState.textures.scimitar.path"),
                      inst->getJsonString("GameData.playState.textures.scimitar.id"),
                      inst->getJsonInt("GameData.playState.gameObjects.Scimitar.width"),
                      inst->getJsonInt("GameData.playState.gameObjects.Scimitar.height"), renderer);
        tm_inst->load(inst->getJsonString("GameData.playState.textures.spindar.path"),
                      inst->getJsonString("GameData.playState.textures.spindar.id"),
                      inst->getJsonInt("GameData.playState.gameObjects.Spindar.width"),
                      inst->getJsonInt("GameData.playState.gameObjects.Spindar.height"), renderer);
        tm_inst->load(inst->getJsonString("GameData.playState.textures.bullet.path"),
                      inst->getJsonString("GameData.playState.textures.bullet.id"),
                      inst->getJsonInt("GameData.playState.bullet.width"),
                      inst->getJsonInt("GameData.playState.bullet.height"), renderer);
        tm_inst->load(inst->getJsonString("GameData.playState.textures.woodenCrate.path"),
                      inst->getJsonString("GameData.playState.textures.woodenCrate.id"),
                      inst->getJsonInt("GameData.playState.textures.woodenCrate.width"),
                      inst->getJsonInt("GameData.playState.textures.woodenCrate.height"), renderer);
        tm_inst->load(inst->getJsonString("GameData.playState.textures.nameLabel.path"),
                      inst->getJsonString("GameData.playState.textures.nameLabel.id"),
                      inst->getJsonInt("GameData.playState.textures.nameLabel.width"),
                      inst->getJsonInt("GameData.playState.textures.nameLabel.height"), renderer);
    }
    catch (const boost::property_tree::ptree_error& e)
    {
        std::cerr << "playState - Error loading JSON data! " << e.what() << std::endl;
    }
    catch (const TextureManagerException& tME)
    {
        std::cerr << "playState - Error: " << tME.what();
        return false;
    }
    return true;
}

void PlayState::initializeEnemyAnimations()
{
    GameData* inst = GameData::getInstance();
    m_animations["enemy"].emplace_back(
        AnimationInfo(inst->getJsonString("GameData.playState.animations.torchGuy.idle.name"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.idle.row"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.idle.rowSpan"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.idle.startPos"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.idle.endPos")));
    m_animations["enemy"].emplace_back(
        AnimationInfo(inst->getJsonString("GameData.playState.animations.torchGuy.idleLeft.name"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.idleLeft.row"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.idleLeft.rowSpan"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.idleLeft.startPos"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.idleLeft.endPos")));
    m_animations["enemy"].emplace_back(
        AnimationInfo(inst->getJsonString("GameData.playState.animations.torchGuy.walk.name"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.walk.row"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.walk.rowSpan"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.walk.startPos"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.walk.endPos")));
    m_animations["enemy"].emplace_back(
        AnimationInfo(inst->getJsonString("GameData.playState.animations.torchGuy.walkLeft.name"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.walkLeft.row"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.walkLeft.rowSpan"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.walkLeft.startPos"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.walkLeft.endPos")));
    m_animations["enemy"].emplace_back(
        AnimationInfo(inst->getJsonString("GameData.playState.animations.torchGuy.jump.name"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.jump.row"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.jump.rowSpan"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.jump.startPos"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.jump.endPos")));
    m_animations["enemy"].emplace_back(
        AnimationInfo(inst->getJsonString("GameData.playState.animations.torchGuy.jumpLeft.name"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.jumpLeft.row"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.jumpLeft.rowSpan"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.jumpLeft.startPos"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.jumpLeft.endPos")));
    m_animations["enemy"].emplace_back(
        AnimationInfo(inst->getJsonString("GameData.playState.animations.torchGuy.run.name"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.run.row"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.run.rowSpan"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.run.startPos"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.run.endPos")));
    m_animations["enemy"].emplace_back(
        AnimationInfo(inst->getJsonString("GameData.playState.animations.torchGuy.runLeft.name"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.runLeft.row"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.runLeft.rowSpan"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.runLeft.startPos"),
                      inst->getJsonUInt("GameData.playState.animations.torchGuy.runLeft.endPos")));
}

void PlayState::initializePlayerAnimations()
{
    GameData* inst = GameData::getInstance();
    m_animations["player"].emplace_back(
        AnimationInfo(inst->getJsonString("GameData.playState.animations.nemesis.idle.name"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.idle.row"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.idle.rowSpan"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.idle.startPos"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.idle.endPos")));
    m_animations["player"].emplace_back(
        AnimationInfo(inst->getJsonString("GameData.playState.animations.nemesis.idleLeft.name"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.idleLeft.row"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.idleLeft.rowSpan"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.idleLeft.startPos"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.idleLeft.endPos")));
    m_animations["player"].emplace_back(
        AnimationInfo(inst->getJsonString("GameData.playState.animations.nemesis.walk.name"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.walk.row"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.walk.rowSpan"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.walk.startPos"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.walk.endPos")));
    m_animations["player"].emplace_back(
        AnimationInfo(inst->getJsonString("GameData.playState.animations.nemesis.walkLeft.name"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.walkLeft.row"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.walkLeft.rowSpan"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.walkLeft.startPos"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.walkLeft.endPos")));
    m_animations["player"].emplace_back(
        AnimationInfo(inst->getJsonString("GameData.playState.animations.nemesis.jump.name"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.jump.row"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.jump.rowSpan"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.jump.startPos"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.jump.endPos")));
    m_animations["player"].emplace_back(
        AnimationInfo(inst->getJsonString("GameData.playState.animations.nemesis.jumpLeft.name"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.jumpLeft.row"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.jumpLeft.rowSpan"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.jumpLeft.startPos"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.jumpLeft.endPos")));
    m_animations["player"].emplace_back(
        AnimationInfo(inst->getJsonString("GameData.playState.animations.nemesis.run.name"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.run.row"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.run.rowSpan"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.run.startPos"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.run.endPos")));
    m_animations["player"].emplace_back(
        AnimationInfo(inst->getJsonString("GameData.playState.animations.nemesis.runLeft.name"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.runLeft.row"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.runLeft.rowSpan"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.runLeft.startPos"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.runLeft.endPos")));
    m_animations["player"].emplace_back(
        AnimationInfo(inst->getJsonString("GameData.playState.animations.nemesis.attack.name"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.attack.row"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.attack.rowSpan"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.attack.startPos"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.attack.endPos")));
    m_animations["player"].emplace_back(
        AnimationInfo(inst->getJsonString("GameData.playState.animations.nemesis.attackLeft.name"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.attackLeft.row"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.attackLeft.rowSpan"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.attackLeft.startPos"),
                      inst->getJsonUInt("GameData.playState.animations.nemesis.attackLeft.endPos")));
}

void PlayState::applyGravity()
{
    if (!m_player->isOnGround())
    {
        m_player->setVelocityY(m_player->getVelocity().getY() + 2);
        if (m_player->getVelocity().getY() > 16) m_player->setVelocityY(16.0f);
    }
    else
    {
        m_player->setVelocityY(0.0f);
    }
    auto iter = m_enemies.begin();
    while (iter != m_enemies.end())
    {
        if (!(*iter)->isOnGround())
        {
            (*iter)->setVelocityY((*iter)->getVelocity().getY() + 2);
            if ((*iter)->getVelocity().getY() > 16) (*iter)->setVelocityY(16.0f);
        }
        else
        {
            (*iter)->setVelocityY(0.0f);
        }
        iter++;
    }
}

void PlayState::checkIfAllEnemiesDead()
{
    std::vector<std::vector<Enemy*>::iterator> itersToDelete;
    m_numEnemiesDead = 0;
    auto eIter = m_enemies.begin();
    while (eIter != m_enemies.end())
    {
        if ((*eIter)->isDead() and (*eIter)->isExploded())
        {
            m_numEnemiesDead++;
            m_player->Detach((*eIter));
            itersToDelete.emplace_back(eIter);
        }
        eIter++;
    }
    for (auto i : itersToDelete)
    {
        delete (*i);
        i = m_enemies.erase(i);
    }
    m_numberOfEnemies = m_enemies.size();
    if (m_enemies.empty() and m_player->getNumberOfWeapons() == m_numberOfWeapons)
    {
        Game::getInstance()->getStateMachine()->pushState("GAME_OVER", new GameOverState(true));
    }
}

void PlayState::checkIfPlayerIsDead()
{
    if (m_player->isDead() and m_player->isExploded())
    {
        Game::getInstance()->getStateMachine()->pushState("GAME_OVER", new GameOverState(false));
    }
}

void PlayState::resetCollidedWithPlatformVariables()
{
    m_playerCollidedWithPlatform = false;
    auto itr = m_enemiesCollidedWithPlatform.begin();
    while (itr != m_enemiesCollidedWithPlatform.end())
    {
        (*itr) = false;
        itr++;
    }
}

void PlayState::checkWhichSideGameObjectCollidedWith(GameObject* gObj, const CollisionObject* cObj)
{
    CollisionBox cB;
    if (gObj->type().find("Enemy") != std::string::npos)
    {
        cB = dynamic_cast<Enemy*>(gObj)->getCollisionBox();
    }
    else if (gObj->type().find("Player") != std::string::npos)
    {
        cB = dynamic_cast<Player*>(gObj)->getCollisionBox();
    }
    if (cObj->whichCollisionSide("TOP"))
    {
        gObj->setPositionY(cObj->getPosition().getY() + cObj->getCollisionHeight()
                           - (cB.getHeight() + cB.getYOffset()));
    }
    if (cObj->whichCollisionSide("RIGHT"))
    {
        gObj->setPositionX(cObj->getRightCollisionBox().uRight.getX() - cB.getXOffset());
    }
    if (cObj->whichCollisionSide("BOTTOM"))
    {
        gObj->setPositionY(cObj->getBottomCollisionBox().lLeft.getY() - cB.getYOffset());
    }
    if (cObj->whichCollisionSide("LEFT"))
    {
        gObj->setPositionX(cObj->getLeftCollisionBox().uLeft.getX() - (cB.getXOffset() + cB.getWidth()));
    }
}
