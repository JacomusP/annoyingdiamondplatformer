//
// Created by jacomus on 2/14/17.
//

#ifndef ANIMATIONWITHSDL2_0_VECTOR2D_H
#define ANIMATIONWITHSDL2_0_VECTOR2D_H

#include <iostream>

class Vector2D
{
public:
    Vector2D();
    Vector2D(const float x, const float y);
    Vector2D(const Vector2D& v2);
    ~Vector2D();
    float getX() const;
    float getY() const;
    void setX(const float x);
    void setY(const float y);
    float distance();
    Vector2D operator+(const Vector2D& v2);
    friend Vector2D& operator+=(Vector2D& v1, Vector2D& v2);
    Vector2D operator*(const float scalar);
    Vector2D& operator*=(const float scalar);
    Vector2D operator-(const Vector2D& v2);
    friend Vector2D& operator-=(Vector2D& v1, Vector2D& v2);
    Vector2D operator/(const float scalar);
    Vector2D& operator/=(const float scalar);
    Vector2D& operator=(const Vector2D& v);
    void normalize();

private:
    float m_x;
    float m_y;
};

std::ostream& operator<<(std::ostream& out, const Vector2D& v);
Vector2D operator+(const Vector2D& v1, const Vector2D& v2);
Vector2D operator-(const Vector2D& v1, const Vector2D& v2);
bool operator==(const Vector2D& v1, const Vector2D& v2);

#endif //ANIMATIONWITHSDL2_0_VECTOR2D_H
