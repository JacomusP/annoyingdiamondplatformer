//
// Created by jacomus on 5/23/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_BULLETPOOL_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_BULLETPOOL_H

#include <string>
#include "gameObject.h"
#include "bullet.h"
#include "weapon.h"
#include "gameState.h"

class BulletPool
{
public:
    BulletPool(const std::string& textureID);
    BulletPool(const BulletPool& bPool);
    BulletPool& operator=(const BulletPool& bPool);
    ~BulletPool();
    void draw(SDL_Renderer* pRenderer) const;
    void update(Uint32 ticks, const GameState& gState);
    void shoot(const Vector2D& pos, const Vector2D& vel);
    unsigned int bulletCount() const { return m_bulletList.size(); }
    unsigned int freeCount()  const { return m_freeList.size(); }
    bool shooting() const { return m_bulletList.empty(); }
    bool collidedWith(const GameObject& obj) const;
    bool collidedWithWeapon(const Weapon& w) const;
    unsigned int getBulletPower() const { return m_bulletPower; }
private:
    std::string m_textureID;
    float m_poolInterval;
    float m_timeSinceLastBulletFired;
    mutable std::list<Bullet> m_bulletList; // BulletPool is a list of Bullet
    mutable std::list<Bullet> m_freeList;   // when a Bullet is out of range
    unsigned int m_bulletPower;
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_BULLETPOOL_H
