//
// Created by jacomus on 4/4/17.
//

#include "hud.h"
#include "game.h"
#include "gameData.h"
#include "clock.h"
#include "ioMod.h"
#include "viewPort.h"

Hud::Hud() :
    m_position(GameData::getInstance()->getJsonInt("GameData.playState.hud.posX"), GameData::getInstance()->getJsonInt("GameData.playState.hud.posY")),
    m_width(GameData::getInstance()->getJsonInt("GameData.playState.hud.width")),
    m_height(GameData::getInstance()->getJsonInt("GameData.playState.hud.height")), m_draw(true)
{}

Hud::~Hud()
{}

void Hud::draw()
{
    if (m_draw)
    {
        SDL_Rect r;
        r.x = static_cast<int>(m_position.getX());
        r.y = static_cast<int>(m_position.getY());
        r.w = m_width;
        r.h = m_height;
        SDL_SetRenderDrawBlendMode(const_cast<SDL_Renderer*>(Game::getInstance()->getRenderer()), SDL_BLENDMODE_BLEND);
        SDL_SetRenderDrawColor(const_cast<SDL_Renderer*>(Game::getInstance()->getRenderer()), 255, 255, 255, 255/2);
        SDL_RenderFillRect(const_cast<SDL_Renderer*>(Game::getInstance()->getRenderer()), &r);
        SDL_SetRenderDrawColor(const_cast<SDL_Renderer*>(Game::getInstance()->getRenderer()), 0, 0, 255, 255);
        SDL_RenderDrawRect(const_cast<SDL_Renderer*>(Game::getInstance()->getRenderer()), &r);

        std::stringstream strm, strn, strp, strq, strr, strs, strb, strj;
        strm << "fps: " << Clock::getInstance()->getFps();
        strn << "average fps: " << Clock::getInstance()->getAvgFps();
        strp << "Move: AWD.";
        strs << "Press left shift to run.";
        strq << "Defeat all enemies to and";
        strj << "Collect all weapons to win";
        strr << "Switch weapons: 1-6";
        strb << "Press ESC for pause menu.";
        IO_Mod::getInstance()->writeText("Tracking " + ViewPort::getInstance()->getObjectToTrack()->getName(),
                                         GameData::getInstance()->getJsonInt("GameData.playState.hud.posX") + 20, 30);
        IO_Mod::getInstance()->writeText(strm.str(), GameData::getInstance()->getJsonInt("GameData.playState.hud.posX") + 20, 60);
        IO_Mod::getInstance()->writeText(strn.str(), GameData::getInstance()->getJsonInt("GameData.playState.hud.posX") + 20, 80);
        IO_Mod::getInstance()->writeText(strp.str(), GameData::getInstance()->getJsonInt("GameData.playState.hud.posX") + 20, 100);
        IO_Mod::getInstance()->writeText(strs.str(), GameData::getInstance()->getJsonInt("GameData.playState.hud.posX") + 20, 120);
        IO_Mod::getInstance()->writeText(strq.str(), GameData::getInstance()->getJsonInt("GameData.playState.hud.posX") + 20, 140);
        IO_Mod::getInstance()->writeText(strj.str(), GameData::getInstance()->getJsonInt("GameData.playState.hud.posX") + 20, 160);
        IO_Mod::getInstance()->writeText(strr.str(), GameData::getInstance()->getJsonInt("GameData.playState.hud.posX") + 20, 180);
        IO_Mod::getInstance()->writeText(strb.str(), GameData::getInstance()->getJsonInt("GameData.playState.hud.posX") + 20, 200);
    }
}
