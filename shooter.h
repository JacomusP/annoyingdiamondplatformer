//
// Created by jacomus on 5/21/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_SHOOTER_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_SHOOTER_H

#include "gameObject.h"
#include "bulletPool.h"

class Shooter : public GameObject
{
public:
    Shooter(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
                const unsigned int numRows, const unsigned int numCols, const std::string& name, const int frameInterval,
                const unsigned int health);
    Shooter(const Shooter& s);
    Shooter& operator=(const Shooter& s);
    virtual ~Shooter();
    virtual std::string type() const;
    virtual bool checkForCollision(const GameObject& gObj);
    virtual void update(Uint32 ticks, const GameState& gState);
    virtual void draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const;
    void shoot(const Vector2D& vel);
    void updateBulletPool(Uint32 ticks, const GameState& gState);
    const BulletPool& getBulletPool() const { return m_bulletPool; }
    const Vector2D& getBulletFiringPosition() const { return m_bulletFiringPosition; }
private:
    BulletPool m_bulletPool;
    Vector2D m_bulletFiringPosition;
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_SHOOTER_H
