//
// Created by jacomus on 2/14/17.
//

#ifndef ANIMATIONWITHSDL2_0_GAMESTATE_H
#define ANIMATIONWITHSDL2_0_GAMESTATE_H

#include <string>
#include <SDL.h>

class GameState
{
public:
    GameState() : m_exited(false) {}
    virtual ~GameState() {}
    virtual void update(Uint32 ticks) = 0;
    virtual void render() = 0;
    virtual bool onEnter() = 0;
    virtual bool onExit() = 0;
    virtual std::string getStateID() const = 0;
    void setExited(const bool e) { m_exited = e; }
    bool getExited() const { return m_exited; }

private:
    bool m_exited;
};

#endif //ANIMATIONWITHSDL2_0_GAMESTATE_H
