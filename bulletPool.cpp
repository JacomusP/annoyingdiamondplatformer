//
// Created by jacomus on 5/23/17.
//

#include "bulletPool.h"
#include "gameData.h"

BulletPool::BulletPool(const std::string& textureID) :
    m_textureID(textureID),
    m_poolInterval(GameData::getInstance()->getJsonInt("GameData.playState.bullet.poolInterval")),
    m_timeSinceLastBulletFired(0), m_bulletList(), m_freeList(),
    m_bulletPower(GameData::getInstance()->getJsonUInt("GameData.playState.bullet.power"))
{}

BulletPool::BulletPool(const BulletPool& bPool) :
    m_textureID(bPool.m_textureID), m_poolInterval(bPool.m_poolInterval),
    m_timeSinceLastBulletFired(bPool.m_timeSinceLastBulletFired), m_bulletList(bPool.m_bulletList),
    m_freeList(bPool.m_freeList), m_bulletPower(bPool.m_bulletPower)
{}

BulletPool& BulletPool::operator=(const BulletPool& bPool)
{
    if (this == &bPool)
    {
        return *this;
    }
    m_textureID = bPool.m_textureID;
    m_poolInterval = bPool.m_poolInterval;
    m_timeSinceLastBulletFired = bPool.m_timeSinceLastBulletFired;
    m_bulletList = bPool.m_bulletList;
    m_freeList = bPool.m_freeList;
    m_bulletPower = bPool.m_bulletPower;
    return *this;
}

BulletPool::~BulletPool()
{
    //std::cout << "BulletPool destructor" << std::endl;
    m_bulletList.clear();
    m_freeList.clear();
}

void BulletPool::draw(SDL_Renderer* pRenderer) const
{
    auto it = m_bulletList.begin();
    while (it != m_bulletList.end())
    {
        (*it).draw(pRenderer, SDL_FLIP_NONE);
        it++;
    }
}

void BulletPool::update(Uint32 ticks, const GameState& gState)
{
    m_timeSinceLastBulletFired += ticks;
    auto it = m_bulletList.begin();
    while (it != m_bulletList.end())
    {
        (*it).update(ticks, gState);
        if ((*it).goneTooFar())
        {
            m_freeList.push_back(*it);
            it = m_bulletList.erase(it);
        }
        else
        {
            it++;
        }
    }
}

void BulletPool::shoot(const Vector2D& pos, const Vector2D& vel)
{
    if (m_timeSinceLastBulletFired > m_poolInterval)
    {
        GameData* inst = GameData::getInstance();
        if (m_freeList.empty())
        {
            Bullet b(pos, vel, Vector2D(), inst->getJsonString("GameData.playState.textures.bullet.id"),
                     inst->getJsonUInt("GameData.playState.textures.bullet.rows"),
                     inst->getJsonUInt("GameData.playState.textures.bullet.cols"),
                     inst->getJsonString("GameData.playState.bullet.name"),
                     inst->getJsonInt("GameData.playState.bullet.frameInterval"), 0);
            b.addAnimation(inst->getJsonString("GameData.playState.animations.bullet.bullet_fire.name"),
                           inst->getJsonUInt("GameData.playState.animations.bullet.bullet_fire.row"),
                           inst->getJsonUInt("GameData.playState.animations.bullet.bullet_fire.rowSpan"),
                           inst->getJsonUInt("GameData.playState.animations.bullet.bullet_fire.startPos"),
                           inst->getJsonUInt("GameData.playState.animations.bullet.bullet_fire.endPos"));
            b.addAnimation(inst->getJsonString("GameData.playState.animations.bullet.bullet_fire_left.name"),
                           inst->getJsonUInt("GameData.playState.animations.bullet.bullet_fire_left.row"),
                           inst->getJsonUInt("GameData.playState.animations.bullet.bullet_fire_left.rowSpan"),
                           inst->getJsonUInt("GameData.playState.animations.bullet.bullet_fire_left.startPos"),
                           inst->getJsonUInt("GameData.playState.animations.bullet.bullet_fire_left.endPos"));
            b.setPosition(pos);
            b.setVelocity(vel);
            if (vel.getX() > 0.0f) b.setCurrentAnimation("bullet_fire_left");
            else b.setCurrentAnimation("bullet_fire");
            m_bulletList.push_back(b);
        }
        else
        {
            Bullet b = m_freeList.front();
            m_freeList.pop_front();
            b.reset();
            b.setPosition(pos);
            b.setVelocity(vel);
            if (vel.getX() > 0.0f) b.setCurrentAnimation("bullet_fire_left");
            else b.setCurrentAnimation("bullet_fire");
            m_bulletList.push_back(b);
        }
        m_timeSinceLastBulletFired = 0;
    }
}

bool BulletPool::collidedWith(const GameObject& obj) const
{
    auto it = m_bulletList.begin();
    while (it != m_bulletList.end())
    {
        if ((*it).checkForCollision(obj))
        {
            m_freeList.push_back(*it);
            it = m_bulletList.erase(it);
            return true;
        }
        it++;
    }
    return false;
}

bool BulletPool::collidedWithWeapon(const Weapon& w) const
{
    auto it = m_bulletList.begin();
    while (it != m_bulletList.end())
    {
        if ((*it).checkForCollision(w))
        {
            (*it).setVelocityX((*it).getVelocity().getX() * -1);
            return true;
        }
        it++;
    }
    return false;
}
