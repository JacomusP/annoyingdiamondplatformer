//
// Created by jacomus on 5/21/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_ENEMY_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_ENEMY_H

#include "shooter.h"
#include "observer.h"
#include "collisionBox.h"

class Enemy : public Shooter, public Observer
{
public:
    Enemy(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
              const unsigned int numRows, const unsigned int numCols, const std::string& name, const int frameInterval,
              const unsigned int health, const int leftX, const int rightX, const int yy);
    Enemy(const Enemy& e);
    Enemy& operator=(const Enemy& e);
    virtual ~Enemy();
    virtual void draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const;
    virtual std::string type() const;
    virtual bool checkForCollision(const GameObject& gObj);
    virtual void update(Uint32 ticks, const GameState& gameState);
    virtual void updateFromSubject(const Vector2D& pos);
    void setPatrolArea(const int lX, const int rX, const int yy = 0);
    const CollisionBox& getCollisionBox() const { return m_collisionBox; }
private:

    enum class STATE
    {
        PATROL,
        ATTACK,
        DEAD
    };

    struct PatrolArea
    {
        int leftX;
        int rightX;
        int y;
        PatrolArea(const int lX, const int rX, const int yy) : leftX(lX), rightX(rX), y(yy) {}
    };
    Vector2D m_playerPosition;
    STATE m_state;
    PatrolArea m_patrolArea;
    CollisionBox m_collisionBox;
    // Methods
    void moveTowardsPlayer();
    void shootTowardsPlayer();
    void patrolArea();
    void attack();
    void goLeft(bool run = false);
    void goRight(bool run = false);
    void stayStill();
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_ENEMY_H
