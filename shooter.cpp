//
// Created by jacomus on 5/21/17.
//

#include "shooter.h"
#include "gameData.h"
#include "collision.h"

Shooter::Shooter(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
                 const unsigned int numRows, const unsigned int numCols, const std::string& name, const int frameInterval,
                 const unsigned int health) :
    GameObject(pos, vel, accel, textureID, numRows, numCols, name, frameInterval, health),
    m_bulletPool(GameData::getInstance()->getJsonString("GameData.playState.textures.bullet.id")),
    m_bulletFiringPosition(GameData::getInstance()->getJsonFloat("GameData.playState.gameObjects.Enemy.bulletXOffset"),
                           GameData::getInstance()->getJsonFloat("GameData.playState.gameObjects.Enemy.bulletYOffset"))
{}

Shooter::Shooter(const Shooter& s) :
    GameObject(s), m_bulletPool(s.m_bulletPool), m_bulletFiringPosition(s.m_bulletFiringPosition)
{}

Shooter& Shooter::operator=(const Shooter& s)
{
    if (this == &s)
    {
        return *this;
    }
    GameObject::operator=(s);
    m_bulletPool = s.m_bulletPool;
    m_bulletFiringPosition = s.m_bulletFiringPosition;
    return *this;
}

Shooter::~Shooter()
{
    std::cout << "Shooter destructor" << std::endl;
}

std::string Shooter::type() const
{
    return "Shooter";
}

bool Shooter::checkForCollision(const GameObject& gObj)
{
    return Collision::getInstance()->shooterAndGameObject(this, &gObj);
}

void Shooter::update(Uint32 ticks, const GameState& gState)
{
    GameObject::update(ticks, gState);
}

void Shooter::updateBulletPool(Uint32 ticks, const GameState& gState)
{
    m_bulletPool.update(ticks, gState);
}

void Shooter::draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const
{
    GameObject::draw(pRenderer, flip);
}

void Shooter::shoot(const Vector2D& vel)
{
    Vector2D temp = getPosition();
    m_bulletPool.shoot(temp + m_bulletFiringPosition, vel);
}
