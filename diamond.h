//
// Created by jacomus on 4/4/17.
//

#ifndef BUILDINGDEPTH_DIAMOND_H
#define BUILDINGDEPTH_DIAMOND_H

#include <string>
#include "gameObject.h"
#include "vector2D.h"

class Diamond : public GameObject
{
public:
    Diamond(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
                const unsigned int numRows, const unsigned int numCols, const std::string& name, const int frameInterval,
                const unsigned int health, const std::string& type);
    Diamond(const Diamond& d) = delete;
    Diamond& operator=(const Diamond& d) = delete;
    virtual ~Diamond();
    virtual void draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const;
    virtual void update(Uint32 ticks, const GameState& gS);
    virtual std::string type() const;
    virtual bool checkForCollision(const GameObject& gObj);
private:
    std::string m_type;
};


#endif //BUILDINGDEPTH_DIAMOND_H
