//
// Created by jacomus on 3/23/17.
//

#ifndef BUILDINGDEPTH_MENUBUTTON_H
#define BUILDINGDEPTH_MENUBUTTON_H

#include <string>
#include "gameObject.h"
#include "gameState.h"

class MenuButton : public GameObject
{
public:
    MenuButton(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
               const unsigned int numRows, const unsigned int numCols, const std::string& name,
               const int frameInterval, const unsigned int health, void (* callback)());
    MenuButton(const MenuButton& mB);
    MenuButton& operator=(const MenuButton& mB);
    virtual ~MenuButton();
    virtual void draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const;
    virtual void update(Uint32 ticks, const GameState& gameState);
    virtual std::string type() const;
    virtual bool checkForCollision(const GameObject& gObj);
private:
    void (*m_callback)();
    bool m_bReleased;
    enum button_state
    {
        MOUSE_OUT = 0,
        MOUSE_OVER = 1,
        CLICKED = 2
    };
};


#endif //BUILDINGDEPTH_MENUBUTTON_H
