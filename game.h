//
// Created by jacomus on 2/6/17.
//

#ifndef MYCODEALONG_GAME_H
#define MYCODEALONG_GAME_H

#include <SDL.h>
#include "gameStateMachine.h"
#include "frameGenerator.h"

class Game
{
public:
    static Game* getInstance();
    static void clean();
    void init();
    bool init(int xpos, int ypos, int width, int height, bool fullscreen);
    void render();
    void update(Uint32 ticks);
    void handleEvents();
    void playGame();
    void quit() { m_bRunning = false; }
    bool running() const { return m_bRunning; } // getter method
    const SDL_Renderer* getRenderer() const { return m_pRenderer; }
    void toggleMakeVideo();
    GameStateMachine* getStateMachine() { return m_pGameStateMachine; }
private:
    SDL_Window* m_pWindow;
    SDL_Renderer* m_pRenderer;
    bool m_bRunning;
    bool m_bMakeVideo;
    int m_currentFrame;
    GameStateMachine* m_pGameStateMachine;
    FrameGenerator* m_pFrameGenerator;
    static Game* myInstance;

    // Methods
    Game();
    ~Game();

    // Explicitly disallow the compiler to generate these functions
    Game(const Game& g) = delete;
    Game(const SDL_Window& win, const SDL_Renderer& ren, const bool r) = delete;
    Game& operator=(const Game& g) = delete;
};

#endif //MYCODEALONG_GAME_H
