//
// Created by jacomus on 5/21/17.
//

#include "enemy.h"
#include "gameData.h"
#include "textureManager.h"
#include "collision.h"

Enemy::Enemy(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
             const unsigned int numRows, const unsigned int numCols, const std::string& name, const int frameInterval,
             const unsigned int health, const int leftX, const int rightX, const int yy) :
    Shooter(pos, vel, accel, textureID, numRows, numCols, name, frameInterval, health), Observer(),
    m_playerPosition(), m_state(STATE::PATROL), m_patrolArea(leftX, rightX, yy), m_collisionBox(20, 12, 25, 49)
{}

Enemy::Enemy(const Enemy& e) :
    Shooter(e), Observer(), m_playerPosition(e.m_playerPosition),
    m_state(e.m_state), m_patrolArea(e.m_patrolArea.leftX, e.m_patrolArea.rightX, e.m_patrolArea.y), m_collisionBox(e.m_collisionBox)
{}

Enemy& Enemy::operator=(const Enemy& e)
{
    if (this == &e)
    {
        return *this;
    }
    Shooter::operator=(e);
    m_playerPosition = e.m_playerPosition;
    m_state = e.m_state;
    m_patrolArea.leftX = e.m_patrolArea.leftX;
    m_patrolArea.rightX = e.m_patrolArea.rightX;
    m_collisionBox = e.m_collisionBox;
    return *this;
}

Enemy::~Enemy()
{
    std::cout << "Enemy destructor" << std::endl;
}

void Enemy::draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const
{
    Shooter::draw(pRenderer, flip);
    if (m_state == STATE::ATTACK)
    {
        getBulletPool().draw(const_cast<SDL_Renderer*>(pRenderer));
    }
}

std::string Enemy::type() const
{
    return Shooter::type() + ": Enemy";
}

bool Enemy::checkForCollision(const GameObject& gObj)
{
    return Shooter::checkForCollision(gObj);
}

void Enemy::update(Uint32 ticks, const GameState& gameState)
{
    GameObject::advanceToNextFrameOfAnimation(ticks);
    if (gameState.getStateID() == "PLAY")
    {
        if (m_state == STATE::PATROL)
        {
            if ((m_playerPosition.getX() > m_patrolArea.leftX && m_playerPosition.getX() < m_patrolArea.rightX)
                && (m_playerPosition.getY() > getPosition().getY() &&
                    m_playerPosition.getY() < (getPosition().getY() + 50)))
            {
                m_state = STATE::ATTACK;
            }
            patrolArea();
        }
        else if (m_state == STATE::ATTACK)
        {
            if (m_playerPosition.getX() < m_patrolArea.leftX || m_playerPosition.getX() > m_patrolArea.rightX ||
                m_playerPosition.getY() < getPosition().getY() || m_playerPosition.getY() > getPosition().getY() + 50)
            {
                m_state = STATE::PATROL;
            }
            attack();
            updateBulletPool(ticks, gameState);
        }
        else
        {}
    }
    Shooter::update(ticks, gameState);
}

void Enemy::updateFromSubject(const Vector2D& pos)
{
    m_playerPosition = pos;
}

void Enemy::moveTowardsPlayer()
{
    Vector2D temp = getPosition();
    Vector2D difference = temp - m_playerPosition;
    if (!isDead())
    {
        int width = TextureManager::getInstance()->getWidth(getSpriteSheet().getTextureID());
        if (difference.getX() > -(width - 30))
        {
            goLeft();
            if (getSpriteSheet().getCurrentAnimation() != "walkLeft")
            {
                setCurrentAnimation("walkLeft");
            }
        }
        else if (difference.getX() < -(width - 30))
        {
            goRight();
            if (getSpriteSheet().getCurrentAnimation() != "walk")
            {
                setCurrentAnimation("walk");
            }
        }
        else if (difference.getX() == -(width - 30))
        {
            stayStill();
            if (getSpriteSheet().getCurrentAnimation() != "idle")
            {
                setCurrentAnimation("idle");
            }
        }
    }
}

void Enemy::shootTowardsPlayer()
{
    if (wentLeftLast())
    {
        //std::cout << "shooting left" << std::endl;
        shoot(Vector2D(-3.0f, 0.0f));
    }
    else
    {
        //std::cout << "shooting right" << std::endl;
        shoot(Vector2D(3.0f, 0.0f));
    }
}

void Enemy::patrolArea()
{
    if (!isDead())
    {
        if (wentLeftLast())
        {
            goLeft();
            if (getSpriteSheet().getCurrentAnimation() != "walkLeft")
            {
                setCurrentAnimation("walkLeft");
            }
        }
        else
        {
            goRight();
            if (getSpriteSheet().getCurrentAnimation() != "walk")
            {
                setCurrentAnimation("walk");
            }
        }
        if ((getPosition().getX() + 40) > m_patrolArea.rightX)
        {
            setPositionX(m_patrolArea.rightX - 40);
            setLeftLast(true);
        }
        else if (getPosition().getX() < m_patrolArea.leftX)
        {
            setPositionX(m_patrolArea.leftX);
            setLeftLast(false);
        }
        if (getPosition().getY() > m_patrolArea.y)
        {
            setPositionY(m_patrolArea.y);
        }
    }
}

void Enemy::attack()
{
    if (!isDead())
    {
        moveTowardsPlayer();
        shootTowardsPlayer();
    }
}

void Enemy::goLeft(bool run)
{
    float incrX = GameData::getInstance()->getJsonFloat("GameData.playState.gameObjects.Enemy.velX");
    if (run)
    {
        setVelocityX(-(2 * incrX));
    }
    else
    {
        setVelocityX(-incrX);
    }
    setLeftLast(true);
}

void Enemy::goRight(bool run)
{
    float incrX = GameData::getInstance()->getJsonFloat("GameData.playState.gameObjects.Enemy.velX");
    if (run)
    {
        setVelocityX(2 * incrX);
    }
    else
    {
        setVelocityX(incrX);
    }
    setLeftLast(false);
}

void Enemy::stayStill()
{
    setVelocityX(0.0f);
}

void Enemy::setPatrolArea(const int lX, const int rX, const int yy)
{
    m_patrolArea.leftX = lX;
    m_patrolArea.rightX = rX;
    if (yy != 0)
    {
        m_patrolArea.y = yy;
    }
}
