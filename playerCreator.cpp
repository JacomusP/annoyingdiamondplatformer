//
// Created by jacomus on 7/11/17.
//

#include "playerCreator.h"
#include "player.h"
#include "gameData.h"

PlayerCreator::PlayerCreator(const int gObjID, const std::string& jsonPath,
              const std::string& baseJsonPath, const std::string& texturePath) :
    BaseCreator(gObjID, jsonPath, baseJsonPath, texturePath)
{}

GameObject* PlayerCreator::createGameObject()
{
    GameData* inst = GameData::getInstance();
    return new Player(Vector2D(inst->getJsonFloat(getBaseJsonPath() + ".posX"),
                               inst->getJsonFloat(getBaseJsonPath() + ".posY")), Vector2D(), Vector2D(),
                      inst->getJsonString(getTexturePath() + ".id"), inst->getJsonUInt(getTexturePath() + ".rows"),
                      inst->getJsonUInt(getTexturePath() + ".cols"), inst->getJsonString(getBaseJsonPath() + ".name"),
                      inst->getJsonInt(getBaseJsonPath() + ".frameInterval"),
                      inst->getJsonUInt(getBaseJsonPath() + ".health"));
}