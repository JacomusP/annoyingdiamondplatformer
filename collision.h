//
// Created by jacomus on 4/19/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_COLLISION_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_COLLISION_H

#include "gameObject.h"
#include "shooter.h"
#include "weaponCarrier.h"
#include "weapon.h"
#include "bullet.h"
#include "collisionObject.h"
#include "crate.h"

class Collision
{
public:
    static Collision* getInstance();
    static void destroy();
    bool gameObjectsAndCollisionObject(const GameObject* gObj, const CollisionObject* cObj) const;
    bool weaponCarrierAndGameObject(const WeaponCarrier* wC, const GameObject* gObj) const;
    bool crateAndWeapon(const Crate* c, const GameObject* gObj) const;
    bool shooterAndGameObject(const Shooter* s, const GameObject* gObj) const;
    bool bulletAndGameObject(const Bullet* b, const GameObject* gObj) const;
    bool bulletAndWeapon(const Bullet* b, const Weapon* w) const;
private:
    static Collision* myInstance;
    // Constructor
    Collision() = default;
    ~Collision() = default;
    bool isVisible(Uint32 pixel, SDL_Surface* surface) const;
    bool rectangularCollisionMethod(const GameObject* gObj, const CollisionObject* cObj) const;
    bool rectangularCollisionMethod(const Crate* c, const Weapon* w) const;
    bool gameObjectCollidedWithPlatform(GameObject* gObj, const PlatformCollisionBox& cBox) const;
    bool weaponCollidedWithCrate(const Crate* c, const Weapon* w) const;
    bool rectangularCollisionMethod(const GameObject* gObj1, const GameObject* gObj2) const;
    bool perPixelCollisionMethod(const GameObject* gObj, const CollisionObject* cObj) const;
    bool perPixelCollisionMethod(const GameObject* gObj1, const GameObject* gObj2) const;
    SDL_Surface* blitSurface(SDL_Surface* surface, int width, int height, unsigned int row = 1, unsigned int col = 0) const;
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_COLLISION_H
