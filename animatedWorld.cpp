//
// Created by jacomus on 3/31/17.
//

#include <iostream>
#include "animatedWorld.h"
#include "textureManager.h"
#include "game.h"

AnimatedWorld::AnimatedWorld(const std::string& id, const int factor, const int picWidth, const int picHeight,
                             const int worldWidth, const int worldHeight, const int frameInterval,
                             const unsigned int numRows, const unsigned int numCols, const std::string& animationName,
                             const unsigned int row, const unsigned int rowSpan, const unsigned int startPos,
                             const unsigned int endPos) :
    World(id, factor, picWidth, picHeight, worldWidth, worldHeight), m_frameInterval(frameInterval),
    m_timeSinceLastFrame(0), m_spriteSheet(id, numRows, numCols), m_animationNames()
{
    addAnimation(animationName, row, rowSpan, startPos, endPos);
    setCurrentAnimation(animationName);
}

AnimatedWorld::~AnimatedWorld()
{
    m_animationNames.clear();
}

void AnimatedWorld::draw()
{
    TextureManager* tM_inst = TextureManager::getInstance();
    int viewX = static_cast<int>(getViewX());
    int viewY = static_cast<int>(getViewY());
    tM_inst->drawFrame(m_spriteSheet.getTextureID(), -viewX, -viewY,
                       m_spriteSheet.getCurrentRow(), m_spriteSheet.getCurrentFrame() % m_spriteSheet.getColumns(),
                       const_cast<SDL_Renderer*>(Game::getInstance()->getRenderer()), SDL_FLIP_NONE);
    tM_inst->drawFrame(m_spriteSheet.getTextureID(), getPicWidth()-viewX, -viewY,
                       m_spriteSheet.getCurrentRow(), m_spriteSheet.getCurrentFrame() % m_spriteSheet.getColumns(),
                       const_cast<SDL_Renderer*>(Game::getInstance()->getRenderer()), SDL_FLIP_NONE);
}

void AnimatedWorld::update(Uint32 ticks)
{
    advanceToNextFrameOfAnimation(ticks);
    World::update();
}

void AnimatedWorld::addAnimation(const std::string &name, const unsigned int row, const unsigned int rowSpan,
                                 const unsigned int startPos, const unsigned int endPos)
{
    m_animationNames.insert(std::pair<std::string, std::string>(name, name));
    m_spriteSheet.addAnimation(name, row, rowSpan, startPos, endPos);
}

void AnimatedWorld::setCurrentAnimation(const std::string &animationName)
{
    if (m_animationNames.find(animationName) != m_animationNames.end())
    {
        m_spriteSheet.setCurrentAnimation(animationName);
    }
    else
    {
        std::cerr << "The texture '" << animationName << "' is not a valid animation name!" << std::endl;
    }
}

void AnimatedWorld::advanceToNextFrameOfAnimation(Uint32 ticks)
{
    m_timeSinceLastFrame += ticks;
    if (m_timeSinceLastFrame > m_frameInterval)
    {
        m_spriteSheet.advanceToNextFrame();
        m_timeSinceLastFrame = 0;
    }
}
