//
// Created by jacomus on 4/29/17.
//

#include "explosion.h"
#include "gameData.h"

Explosion::Explosion() :
    m_chunkList(), m_freeList()
{}

Explosion::Explosion(const Explosion& e) :
    m_chunkList(e.m_chunkList), m_freeList(e.m_freeList)
{}

Explosion& Explosion::operator=(const Explosion& e)
{
    if (this == &e)
    {
        return *this;
    }
    m_chunkList = e.m_chunkList;
    m_freeList = e.m_freeList;
    return *this;
}

Explosion::~Explosion()
{
    //std::cout << "Explosion destructor" << std::endl;
    //std::cout << "Deleting chunks in the chunk list" << std::endl;
    for (Chunk* c : m_chunkList)
    {
        delete c;
    }
    //std::cout << "Deleting chunks in the free list" << std::endl;
    for (Chunk* f : m_freeList)
    {
        delete f;
    }
    m_chunkList.clear();
    m_freeList.clear();
    //std::cout << "End Explosion destructor" << std::endl;
}

void Explosion::draw(SDL_Renderer* pRenderer) const
{
    for (Chunk* c : m_chunkList)
    {
        c->draw(pRenderer, SDL_FLIP_NONE);
    }
}

void Explosion::update(Uint32 ticks)
{
    auto it = m_chunkList.begin();
    while (it != m_chunkList.end())
    {
        (*it)->update(ticks);
        if ((*it)->goneTooFar()) // Check to see if we should free a chunk
        {
            m_freeList.push_back(*it);
            it = m_chunkList.erase(it);
        }
        else
        {
            it++;
        }
    }
}

void Explosion::makeChunks(unsigned int n, const std::string& textureID, const Vector2D& position,
                           const Vector2D& velocity, const int width, const int height)
{
    //std::cout << "Making chunks in Explosion!" << std::endl;
    // Break the SDL_Surface into n*n squares; where each square
    // has width and height of frameWidth/n and frameHeight/n
    int chunk_width = std::max(1u, (width / n));
    int chunk_height = std::max(1u, (height / n));
    int speedx = static_cast<int>(velocity.getX()); // Wanna test for zero...
    int speedy = static_cast<int>(velocity.getY()); // Make sure it's an int.
    if (speedx == 0) speedx = 1; // Make sure it's not 0;
    if (speedy == 0) speedy = 1; // Make sure it's not 0;

    // Read the SDL_Surface so we can chunk it
    int source_y = 0;
    while ((source_y + chunk_height) < height)
    {
        int source_x = 0;
        while ((source_x + chunk_width) < width)
        {
            // Give each chunk it's own speed/direction:
            float sx = (rand() % speedx + 40) * (rand() % 2? -1 : 1); // 'cause %0 is
            float sy = (rand() % speedy + 40) * (rand() % 2? -1 : 1); // float except

            Chunk* chunk = new Chunk(textureID, Vector2D(position.getX() + source_x, position.getY() + source_y),
                                     Vector2D(sx, sy), { source_x, source_y, chunk_width, chunk_height },
                                     chunk_width, chunk_height);
            m_chunkList.push_back(chunk);
            source_x += chunk_width;
        }
        source_y += chunk_height;
    }
    //std::cout << "Finished making chunks in Explosion!" << std::endl;
}
