//
// Created by jacomus on 3/23/17.
//

#include <iostream>
#include <boost/property_tree/exceptions.hpp>
#include "menuState.h"
#include "game.h"
#include "textureManager.h"
#include "textureManagerException.h"
#include "gameData.h"
#include "menuButton.h"
#include "playState.h"

const std::string MenuState::s_menuID = "MENU";

MenuState::MenuState() :
    GameState(), m_skyMountains(nullptr), m_waterfallCliffs(nullptr), m_platforms(nullptr), m_gameObjects()
{}

MenuState::~MenuState()
{
    onExit();
}

void MenuState::update(Uint32 ticks)
{
    if (GameState::getExited()) return; // if we have exited this state then return from this function
    m_skyMountains->update();
    if (GameState::getExited()) return; // if we have exited this state then return from this function
    m_waterfallCliffs->update(ticks);
    if (GameState::getExited()) return; // if we have exited this state then return from this function
    for (unsigned int i = 0; i < m_gameObjects.size(); i++)
    {
        m_gameObjects[i]->update(ticks, *this);
    }
    if (GameState::getExited()) return; // if we have exited this state then return from this function
    m_platforms->update();
}

void MenuState::render()
{
    if (GameState::getExited()) return; // if we have exited this state then return from this function
    m_skyMountains->draw();
    if (GameState::getExited()) return; // if we have exited this state then return from this function
    m_waterfallCliffs->draw();
    if (GameState::getExited()) return; // if we have exited this state then return from this function
    m_platforms->draw();
    if (GameState::getExited()) return; // if we have exited this state then return from this function
    for (unsigned int i = 0; i < m_gameObjects.size(); i++)
    {
        m_gameObjects[i]->draw(Game::getInstance()->getRenderer(), SDL_FLIP_NONE);
    }
}

bool MenuState::onEnter()
{
    GameData* inst = GameData::getInstance();
    if (!loadTextures())
    {
        std::cerr << "menuState - Problem loading textures!" << std::endl;
        return false;
    }
    try
    {
        GameState::setExited(false);
        m_gameObjects.push_back(
            new MenuButton(Vector2D(inst->getJsonFloat("GameData.menuState.gameObjects.PlayButton.posX"),
                                    inst->getJsonFloat("GameData.menuState.gameObjects.PlayButton.posY")), Vector2D(),
                           Vector2D(), inst->getJsonString("GameData.menuState.textures.playButton.id"), 1, 3,
                           "playButt", 0, 0, s_menuToPlay));
        m_gameObjects.push_back(
            new MenuButton(Vector2D(inst->getJsonFloat("GameData.menuState.gameObjects.ExitButton.posX"),
                                    inst->getJsonFloat("GameData.menuState.gameObjects.ExitButton.posY")), Vector2D(),
                           Vector2D(), inst->getJsonString("GameData.menuState.textures.exitButton.id"), 1, 3,
                           "exitButt", 0, 0, s_exitFromMenu));

        m_skyMountains = new World(inst->getJsonString("GameData.menuState.textures.skyMountains.id"),
                                   inst->getJsonInt("GameData.menuState.worlds.SkyMountains.factor"),
                                   inst->getJsonInt("GameData.menuState.worlds.SkyMountains.width"),
                                   inst->getJsonInt("GameData.menuState.worlds.SkyMountains.height"),
                                   inst->getJsonInt("GameData.menuState.world.width"),
                                   inst->getJsonInt("GameData.menuState.world.height"));
        m_waterfallCliffs = new AnimatedWorld(inst->getJsonString("GameData.menuState.textures.waterfallCliff.id"),
                                         inst->getJsonInt("GameData.menuState.animatedWorlds.WaterfallCliff.factor"),
                                         inst->getJsonInt("GameData.menuState.animatedWorlds.WaterfallCliff.width"),
                                         inst->getJsonInt("GameData.menuState.animatedWorlds.WaterfallCliff.height"),
                                         inst->getJsonInt("GameData.menuState.world.width"),
                                         inst->getJsonInt("GameData.menuState.world.height"),
                                         inst->getJsonInt("GameData.menuState.animatedWorlds.WaterfallCliff.frameInterval"),
                                         inst->getJsonUInt("GameData.menuState.textures.waterfallCliff.rows"),
                                         inst->getJsonUInt("GameData.menuState.textures.waterfallCliff.cols"),
                                         inst->getJsonString("GameData.menuState.animations.waterfallCliff.waterfall_torch.name"),
                                         inst->getJsonUInt("GameData.menuState.animations.waterfallCliff.waterfall_torch.row"),
                                         inst->getJsonUInt("GameData.menuState.animations.waterfallCliff.waterfall_torch.rowSpan"),
                                         inst->getJsonUInt("GameData.menuState.animations.waterfallCliff.waterfall_torch.startPos"),
                                         inst->getJsonUInt("GameData.menuState.animations.waterfallCliff.waterfall_torch.endPos"));
        m_platforms = new World(inst->getJsonString("GameData.menuState.textures.platforms.id"),
                                inst->getJsonInt("GameData.menuState.worlds.Platforms.factor"),
                                inst->getJsonInt("GameData.menuState.worlds.Platforms.width"),
                                inst->getJsonInt("GameData.menuState.worlds.Platforms.height"),
                                inst->getJsonInt("GameData.menuState.world.width"),
                                inst->getJsonInt("GameData.menuState.world.height"));
    }
    catch (const boost::property_tree::ptree_error& e)
    {
        std::cerr << "menuState - Error Reading JSON data! " << e.what() << std::endl;
    }
    std::cout << "entering menuState" << std::endl;
    return true;
}

bool MenuState::loadTextures()
{
    try
    {
        GameData* inst = GameData::getInstance();
        TextureManager* tm_inst = TextureManager::getInstance();
        Game* g_inst = Game::getInstance();
        tm_inst->load(inst->getJsonString("GameData.menuState.textures.playButton.path"),
                      inst->getJsonString("GameData.menuState.textures.playButton.id"),
                      inst->getJsonInt("GameData.menuState.gameObjects.PlayButton.width"),
                      inst->getJsonInt("GameData.menuState.gameObjects.PlayButton.height"),
                      const_cast<SDL_Renderer*>(g_inst->getRenderer()));
        std::cout << "menuState - playButton loaded" << std::endl;
        tm_inst->load(inst->getJsonString("GameData.menuState.textures.exitButton.path"),
                      inst->getJsonString("GameData.menuState.textures.exitButton.id"),
                      inst->getJsonInt("GameData.menuState.gameObjects.ExitButton.width"),
                      inst->getJsonInt("GameData.menuState.gameObjects.ExitButton.height"),
                      const_cast<SDL_Renderer*>(g_inst->getRenderer()));
        std::cout << "menuState - exitButton loaded" << std::endl;
        tm_inst->load(inst->getJsonString("GameData.menuState.textures.skyMountains.path"),
                      inst->getJsonString("GameData.menuState.textures.skyMountains.id"),
                      inst->getJsonInt("GameData.menuState.worlds.SkyMountains.width"),
                      inst->getJsonInt("GameData.menuState.worlds.SkyMountains.height"),
                      const_cast<SDL_Renderer*>(g_inst->getRenderer()));
        std::cout << "menuState - background loaded" << std::endl;
        tm_inst->load(inst->getJsonString("GameData.menuState.textures.waterfallCliff.path"),
                      inst->getJsonString("GameData.menuState.textures.waterfallCliff.id"),
                      inst->getJsonInt("GameData.menuState.animatedWorlds.WaterfallCliff.width"),
                      inst->getJsonInt("GameData.menuState.animatedWorlds.WaterfallCliff.height"),
                      const_cast<SDL_Renderer*>(g_inst->getRenderer()));
        std::cout << "menuState - midground loaded" << std::endl;
        tm_inst->load(inst->getJsonString("GameData.menuState.textures.platforms.path"),
                      inst->getJsonString("GameData.menuState.textures.platforms.id"),
                      inst->getJsonInt("GameData.menuState.worlds.Platforms.width"),
                      inst->getJsonInt("GameData.menuState.worlds.Platforms.height"),
                      const_cast<SDL_Renderer*>(g_inst->getRenderer()));
        std::cout << "menuState - foreground loaded" << std::endl;
    }
    catch (const boost::property_tree::ptree_error& e)
    {
        std::cerr << "menuState - Error loading JSON data! " << e.what() << std::endl;
        return false;
    }
    catch (const TextureManagerException& tME)
    {
        std::cerr << "menuState - Error: " << tME.what();
        return false;
    }
    return true;
}

bool MenuState::onExit()
{
    GameData* inst = GameData::getInstance();
    TextureManager* tM_inst = TextureManager::getInstance();
    if (!GameState::getExited())
    {
        for (GameObject* sdlGO : m_gameObjects)
        {
            delete sdlGO;
        }
        m_gameObjects.clear();
        delete m_skyMountains;
        delete m_waterfallCliffs;
        delete m_platforms;
        tM_inst->clearFromTextureMap(inst->getJsonString("GameData.menuState.textures.playButton.id"));
        tM_inst->clearFromTextureMap(inst->getJsonString("GameData.menuState.textures.exitButton.id"));
        tM_inst->clearFromTextureMap(inst->getJsonString("GameData.menuState.textures.skyMountains.id"));
        tM_inst->clearFromTextureMap(inst->getJsonString("GameData.menuState.textures.waterfallCliff.id"));
        tM_inst->clearFromTextureMap(inst->getJsonString("GameData.menuState.textures.platforms.id"));
    }
    std::cout << "exiting menuState" << std::endl;
    GameState::setExited(true);
    return true;
}

void MenuState::s_menuToPlay()
{
    Game::getInstance()->getStateMachine()->changeState("PLAY", new PlayState());
}

void MenuState::s_exitFromMenu()
{
    Game::getInstance()->quit();
}

std::string MenuState::getStateID() const
{
    return s_menuID;
}
