//
// Created by jacomus on 6/20/17.
//

#include "viewPort.h"
#include "gameData.h"
#include "textureManager.h"

ViewPort* ViewPort::myInstance = nullptr;

ViewPort* ViewPort::getInstance()
{
    if (myInstance == nullptr)
    {
        myInstance = new ViewPort();
    }
    return myInstance;
}

void ViewPort::destroy()
{
    delete myInstance;
}

void ViewPort::draw() const
{
}

void ViewPort::update()
{
    const float x = m_objectToTrack->getPosition().getX();
    const float y = m_objectToTrack->getPosition().getY();
    setX((x + (m_objWidth / 2)) - (m_width / 2));
    setY((y + (m_objHeight / 2)) - (m_height / 2));
    if (getX() < 0)
    {
        setX(0);
    }
    if (getY() < 0)
    {
        setY(0);
    }
    if (getX() > (m_worldWidth - m_width))
    {
        setX(m_worldWidth - m_width);
    }
    if (getY() > (m_worldHeight - m_height))
    {
        setY(m_worldHeight - m_height);
    }
}

const Vector2D ViewPort::getPosition() const
{
    return m_position;
}

float ViewPort::getX() const
{
    return m_position.getX();
}

void ViewPort::setX(float x)
{
    m_position.setX(x);
}

float ViewPort::getY() const
{
    return m_position.getY();
}

void ViewPort::setY(float y)
{
    m_position.setY(y);
}

void ViewPort::setObjectToTrack(const GameObject* gObj)
{
    m_objectToTrack = gObj;
    m_objWidth = TextureManager::getInstance()->getWidth(m_objectToTrack->getSpriteSheet().getTextureID());
    m_objHeight = TextureManager::getInstance()->getHeight(m_objectToTrack->getSpriteSheet().getTextureID());
}

const GameObject* ViewPort::getObjectToTrack() const
{
    return m_objectToTrack;
}

ViewPort::ViewPort() :
    m_position(GameData::getInstance()->getJsonFloat("GameData.viewPort.posX"),
               GameData::getInstance()->getJsonFloat("GameData.viewPort.posY")),
    m_worldWidth(GameData::getInstance()->getJsonInt("GameData.playState.world.width")),
    m_worldHeight(GameData::getInstance()->getJsonInt("GameData.playState.world.height")),
    m_width(GameData::getInstance()->getJsonInt("GameData.viewPort.width")),
    m_height(GameData::getInstance()->getJsonInt("GameData.viewPort.height")),
    m_objWidth(0), m_objHeight(0), m_objectToTrack(nullptr)
{}

ViewPort::~ViewPort()
{}
