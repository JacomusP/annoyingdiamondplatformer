//
// Created by jacomus on 2/28/17.
//

#include <sstream>
#include "textureManagerException.h"

TextureManagerException::TextureManagerException(const std::string& msg) :
    m_message(msg)
{}

TextureManagerException::TextureManagerException(const TextureManagerException &tME) :
    m_message(tME.m_message)
{}

const char* TextureManagerException::what() const throw()
{
    std::string convertStr("");
    convertStr += std::string(std::exception::what()) + std::string(": ") + m_message;
    return convertStr.c_str();
}
