//
// Created by jacomus on 3/6/17.
//

#ifndef POLYMORPHISMINACTION_CLOCK_H
#define POLYMORPHISMINACTION_CLOCK_H

#include <SDL.h>
#include <string>
#include <queue>

class Clock
{
public:
    static Clock* getInstance();
    static void destroy();
    unsigned int getTicks() const;
    unsigned int getElapsedTicks();
    void incrementFrame();
    void toggleSlowMotion();
    bool isStarted() const { return m_started; }
    bool isPaused() const { return m_paused; }
    unsigned int getFrames() const { return m_frames; }
    unsigned int getSeconds() const { return (getTicks() / 1000); }
    unsigned int capFrameRate() const;
    int getFps() const;
    int getAvgFps() const;
    void startClock();
    void pause();
    void unpause();

private:
    bool m_started;
    bool m_paused;
    const bool m_FRAME_CAP_ON;
    const Uint32 m_PERIOD;
    unsigned int m_frames;
    unsigned int m_timeAtStart;
    unsigned int m_timeAtPause;
    unsigned int m_currTicks;
    unsigned int m_prevTicks;
    unsigned int m_ticks;
    unsigned int m_maxFrames;
    std::queue<int> m_fpsQueue;
    int m_sum;
    static Clock* myInstance;
    // Methods
    Clock();
    Clock(const Clock& c);
    ~Clock();
    void addFrameToQueue(int fps);
    Clock& operator=(const Clock& c) = delete;
};


#endif //POLYMORPHISMINACTION_CLOCK_H
