//
// Created by jacomus on 2/28/17.
//

#include "parseJSON.h"

ParseJSON::ParseJSON(const std::string &fileName) :
    m_fileName(fileName), m_root()
{
    parseJson();
}

const boost::property_tree::ptree ParseJSON::getJsonData() const
{
    return m_root;
}

void ParseJSON::parseJson()
{
    boost::property_tree::read_json("JsonSpec/game.json", m_root);
}
