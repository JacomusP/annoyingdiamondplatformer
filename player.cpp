//
// Created by jacomus on 5/9/17.
//

#include "player.h"
#include "gameData.h"
#include "inputHandler.h"
#include "ioMod.h"

Player::Player(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
               const unsigned int numRows, const unsigned int numCols, const std::string& name, const int frameInterval,
               const unsigned int health) :
    WeaponCarrier(pos, vel, accel, textureID, numRows, numCols, name, frameInterval, health),
    Subject(), m_invincible(true), m_collisionBox(23, 27, 16, 30), m_state(STATE::IDLE_RIGHT)
{}

Player::Player(const Player& p) :
    WeaponCarrier(p), Subject(), m_invincible(p.m_invincible), m_collisionBox(p.m_collisionBox),
    m_state(p.m_state)
{}

Player& Player::operator=(const Player& p)
{
    if (this == &p)
    {
        return *this;
    }
    WeaponCarrier::operator=(p);
    m_invincible = p.m_invincible;
    m_collisionBox = p.m_collisionBox;
    m_state = p.m_state;
    return *this;
}

Player::~Player()
{
    std::cout << "Player destructor" << std::endl;
}

void Player::draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const
{
    SDL_Color color;
    color.r = 0;
    color.g = 0;
    color.b = 0;
    color.a = 255;
    IO_Mod::getInstance()->writeText("Current Player State: " + convertSTATEToString(), 20, 20, color);
    WeaponCarrier::draw(pRenderer, flip);
}

void Player::update(Uint32 ticks, const GameState& gameState)
{
    STATE lastState;
    GameObject::advanceToNextFrameOfAnimation(ticks);
    setVelocityX(getVelocity().getX() - 0.5f); // To bring player to a stop when the movement key is no longer pressed
    if (getVelocity().getX() < 0)
    {
        setVelocityX(0.0f);
    }
    if (gameState.getStateID() == "PLAY")
    {
        // Need to bounce the player off the edges of the world
        int difference = GameData::getInstance()->getJsonInt("GameData.playState.world.width")
                         - (m_collisionBox.getXOffset() + m_collisionBox.getWidth());
        if (getPosition().getX() > difference)
        {
            setPositionX(difference);
        }
        else if (getPosition().getX() < -m_collisionBox.getXOffset())
        {
            setPositionX(-m_collisionBox.getXOffset());
        }
        // let player fall off the bottom of the world and place them back on the starting platform
        if (getPosition().getY() > 480)
        {
            setPositionX(0.0f);
            setPositionY(423.0f - getHeight());
            setVelocityY(0.0f);
        }
        else if (getPosition().getY() < 0) // don't let player go off the top of the world
        {
            setPositionY(0.0f);
        }
        lastState = m_state;
        handleInput();
        reactToStateChange(lastState);
        // Sets the player back to idle mode if they are no longer moving or attacking
        Vector2D v(0.0f, 0.0f); // vector with no magnitude in either direction
        if (getVelocity() == v && (getSpriteSheet().getCurrentAnimation() != "attack" and
            getSpriteSheet().getCurrentAnimation() != "attackLeft"))
        {
            if (!wentLeftLast() and isOnGround())
            {
                m_state = STATE::IDLE_RIGHT;
                if (getSpriteSheet().getCurrentAnimation() != "idle")
                {
                    setCurrentAnimation("idle");
                }
            }
            else if (isOnGround())
            {
                m_state = STATE::IDLE_LEFT;
                if (getSpriteSheet().getCurrentAnimation() != "idleLeft")
                {
                    setCurrentAnimation("idleLeft");
                }
            }
        }
        WeaponCarrier::update(ticks, gameState);
        Notify(getPosition());
    }
}

std::string Player::type() const
{
    return WeaponCarrier::type() + ": Player";
}

bool Player::checkForCollision(const GameObject& gObj)
{
    return WeaponCarrier::checkForCollision(gObj);
}

void Player::toggleInvincibility()
{
    m_invincible = !m_invincible;
}

void Player::handleInput()
{
    InputHandler* iH = InputHandler::getInstance();
    STATE state = STATE::NONE;
    if (iH->isKeyDown(SDL_SCANCODE_SPACE))
    {
        if (!getEquippedWeaponName().empty())
        {
            if (wentLeftLast())
            {
                state = STATE::ATTACK_LEFT;
            }
            else
            {
                state = STATE::ATTACK_RIGHT;
            }
        }
    }
    else if (iH->isKeyDown(SDL_SCANCODE_G))
    {
        toggleInvincibility();
    }
    else
    {
        if (iH->isKeyDown(SDL_SCANCODE_A) and iH->isKeyDown(SDL_SCANCODE_LSHIFT))
        {
            state = STATE::RUN_LEFT;
        }
        else if (iH->isKeyDown(SDL_SCANCODE_A))
        {
            state = STATE::WALK_LEFT;
        }
        if (iH->isKeyDown(SDL_SCANCODE_D) and iH->isKeyDown(SDL_SCANCODE_LSHIFT))
        {
            state = STATE::RUN_RIGHT;
        }
        else if (iH->isKeyDown(SDL_SCANCODE_D))
        {
            state = STATE::WALK_RIGHT;
        }
        if (iH->isKeyDown(SDL_SCANCODE_W) and iH->isKeyDown(SDL_SCANCODE_D))
        {
            if (isOnGround())
            {
                state = STATE::JUMP_RIGHT;
            }
            else
            {
                if (m_state == STATE::FALLING)
                {
                    state = STATE::DOUBLE_JUMP_RIGHT;
                }
            }
        }
        else if (iH->isKeyDown(SDL_SCANCODE_W) and iH->isKeyDown(SDL_SCANCODE_A))
        {
            if (isOnGround())
            {
                state = STATE::JUMP_LEFT;
            }
            else
            {
                if (m_state == STATE::FALLING)
                {
                    state = STATE::DOUBLE_JUMP_LEFT;
                }
            }
        }
        else if (iH->isKeyDown(SDL_SCANCODE_W))
        {
            if (isOnGround())
            {
                state = STATE::JUMP;
            }
            else
            {
                if (m_state == STATE::FALLING)
                {
                    state = STATE::DOUBLE_JUMP;
                }
            }
        }
        if (iH->isKeyDown(SDL_SCANCODE_1))
        {
            if (hasWeapon("sword"))
            {
                if (getEquippedWeaponName() != "sword")
                {
                    unequipWeapon();
                    equipWeapon("sword");
                }
            }
        }
        if (iH->isKeyDown(SDL_SCANCODE_2))
        {
            if (hasWeapon("spear"))
            {
                if (getEquippedWeaponName() != "spear")
                {
                    unequipWeapon();
                    equipWeapon("spear");
                }
            }
        }
        if (iH->isKeyDown(SDL_SCANCODE_3))
        {
            if (hasWeapon("battleAxe"))
            {
                if (getEquippedWeaponName() != "battleAxe")
                {
                    unequipWeapon();
                    equipWeapon("battleAxe");
                }
            }
        }
        if (iH->isKeyDown(SDL_SCANCODE_4))
        {
            if (hasWeapon("glaive"))
            {
                if (getEquippedWeaponName() != "glaive")
                {
                    unequipWeapon();
                    equipWeapon("glaive");
                }
            }
        }
        if (iH->isKeyDown(SDL_SCANCODE_5))
        {
            if (hasWeapon("scimitar"))
            {
                if (getEquippedWeaponName() != "scimitar")
                {
                    unequipWeapon();
                    equipWeapon("scimitar");
                }
            }
        }
        if (iH->isKeyDown(SDL_SCANCODE_6))
        {
            if (hasWeapon("spindar"))
            {
                if (getEquippedWeaponName() != "spindar")
                {
                    unequipWeapon();
                    equipWeapon("spindar");
                }
            }
        }
    }
    m_state = state;
}

void Player::reactToStateChange(const STATE& lastState)
{
    switch (m_state)
    {
        case STATE::WALK_RIGHT:
            setVelocityX(1.0f);
            if (getSpriteSheet().getCurrentAnimation() != "walk")
            {
                setCurrentAnimation("walk");
            }
            setLeftLast(false);
            break;
        case STATE::WALK_LEFT:
            setVelocityX(-1.0f);
            if (getSpriteSheet().getCurrentAnimation() != "walkLeft")
            {
                setCurrentAnimation("walkLeft");
            }
            setLeftLast(true);
            break;
        case STATE::RUN_RIGHT:
            setVelocityX(1.0f * 4);
            if (getSpriteSheet().getCurrentAnimation() != "run")
            {
                setCurrentAnimation("run");
            }
            setLeftLast(false);
            break;
        case STATE::RUN_LEFT:
            setVelocityX(-1.0f * 4);
            if (getSpriteSheet().getCurrentAnimation() != "runLeft")
            {
                setCurrentAnimation("runLeft");
            }
            setLeftLast(true);
            break;
        case STATE::JUMP_LEFT:
        case STATE::DOUBLE_JUMP_LEFT:
            setVelocityX(-10.0f);
            setVelocityY(-20.0f);
            if (getSpriteSheet().getCurrentAnimation() != "jumpLeft")
            {
                setCurrentAnimation("jumpLeft");
            }
            GameObject::setOnGround(false);
            setLeftLast(true);
            break;
        case STATE::JUMP:
        case STATE::DOUBLE_JUMP:
            setVelocityY(-20.0f);
            if (wentLeftLast())
            {
                if (getSpriteSheet().getCurrentAnimation() != "jumpLeft")
                {
                    setCurrentAnimation("jumpLeft");
                }
            }
            else
            {
                if (getSpriteSheet().getCurrentAnimation() != "jump")
                {
                    setCurrentAnimation("jump");
                }
            }
            break;
        case STATE::JUMP_RIGHT:
        case STATE::DOUBLE_JUMP_RIGHT:
            setVelocityX(10.0f);
            setVelocityY(-20.0f);
            if (getSpriteSheet().getCurrentAnimation() != "jump")
            {
                setCurrentAnimation("jump");
            }
            GameObject::setOnGround(false);
            setLeftLast(false);
            break;
        case STATE::FALLING:
            break;
        case STATE::ATTACK_RIGHT:
            if (getSpriteSheet().getCurrentAnimation() != "attack")
            {
                setCurrentAnimation("attack");
            }
            break;
        case STATE::ATTACK_LEFT:
            if (getSpriteSheet().getCurrentAnimation() != "attackLeft")
            {
                setCurrentAnimation("attackLeft");
            }
            break;
        case STATE::DEAD:
            break;
        default:
            if (lastState == STATE::JUMP or lastState == STATE::JUMP_LEFT or lastState == STATE::JUMP_RIGHT or
                lastState == STATE::FALLING)
            {
                m_state = STATE::FALLING;
            }
            else if (lastState == STATE::ATTACK_LEFT)
            {
                m_state = STATE::ATTACK_LEFT;
            }
            else if (lastState == STATE::ATTACK_RIGHT)
            {
                m_state = STATE::ATTACK_RIGHT;
            }
    }
}

std::string Player::convertSTATEToString() const
{
    std::string stateString;
    switch(m_state)
    {
        case STATE::IDLE_RIGHT:
            stateString = "IDLE_RIGHT";
            break;
        case STATE::IDLE_LEFT:
            stateString = "IDLE_LEFT";
            break;
        case STATE::WALK_RIGHT:
            stateString = "WALK_RIGHT";
            break;
        case STATE::WALK_LEFT:
            stateString = "WALK_LEFT";
            break;
        case STATE::RUN_RIGHT:
            stateString = "RUN_RIGHT";
            break;
        case STATE::RUN_LEFT:
            stateString = "RUN_LEFT";
            break;
        case STATE::JUMP_LEFT:
            stateString = "JUMP_LEFT";
            break;
        case STATE::JUMP:
            stateString = "JUMP";
            break;
        case STATE::JUMP_RIGHT:
            stateString = "JUMP_RIGHT";
            break;
        case STATE::DOUBLE_JUMP_LEFT:
            stateString = "DOUBLE_JUMP_LEFT";
            break;
        case STATE::DOUBLE_JUMP:
            stateString = "DOUBLE_JUMP";
            break;
        case STATE::DOUBLE_JUMP_RIGHT:
            stateString = "DOUBLE_JUMP_RIGHT";
            break;
        case STATE::FALLING:
            stateString = "FALLING";
            break;
        case STATE::ATTACK_RIGHT:
            stateString = "ATTACK_RIGHT";
            break;
        case STATE::ATTACK_LEFT:
            stateString = "ATTACK_LEFT";
            break;
        case STATE::DEAD:
            stateString = "DEAD";
            break;
        default:
            stateString = "NONE";
            break;
    }
    return stateString;
}

void Player::setInvincibility(const bool i)
{
    m_invincible = i;
}
