//
// Created by jacomus on 2/6/17.
//

#include <iostream>
#include "game.h"
#include "menuState.h"
#include "inputHandler.h"
#include "textureManager.h"
#include "gameData.h"
#include "viewPort.h"
#include "clock.h"

Game* Game::myInstance = nullptr;

Game* Game::getInstance()
{
    if (myInstance == nullptr)
    {
        myInstance = new Game();
    }
    return myInstance;
}

Game::Game() :
    m_pWindow(nullptr), m_pRenderer(nullptr),
    m_bRunning(false), m_bMakeVideo(false), m_currentFrame(1), m_pGameStateMachine(nullptr),
    m_pFrameGenerator(nullptr)
{}

Game::~Game()
{
    std::cout << "cleaning up the game!" << std::endl;
    delete m_pGameStateMachine;
    delete m_pFrameGenerator;
    Clock::destroy();
    ViewPort::destroy();
    TextureManager::destroy();
    InputHandler::clean();
    GameData::clean();
    SDL_DestroyRenderer(m_pRenderer);
    SDL_DestroyWindow(m_pWindow);
    SDL_Quit();
}

void Game::init()
{
    Clock::getInstance()->startClock();
    if (!init(GameData::getInstance()->getJsonInt("GameData.viewPort.windowXpos"),
              GameData::getInstance()->getJsonInt("GameData.viewPort.windowYpos"),
              GameData::getInstance()->getJsonInt("GameData.viewPort.width"),
              GameData::getInstance()->getJsonInt("GameData.viewPort.height"),
              GameData::getInstance()->getJsonBool("GameData.viewPort.fullscreen")))
    {
        throw std::string("Error initializing game! ") + SDL_GetError();
    }
}

bool Game::init(int xpos, int ypos, int width,
                int height, bool fullscreen)
{
    unsigned int flags = 0;
    if (fullscreen)
    {
        flags = SDL_WINDOW_FULLSCREEN;
    }
    // initialize SDL
    if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
    {
        std::cout << "SDL init success" << std::endl;
        // if succeeded create our window
        m_pWindow = SDL_CreateWindow(GameData::getInstance()->getJsonString("GameData.screenTitle").c_str(),
                                     xpos, ypos, width, height, flags);
        if (m_pWindow == nullptr)
        {
            std::cerr << "Error creating window: " << SDL_GetError() << std::endl;
            return false;
        }
        std::cout << "window creation success!" << std::endl;
        // if the window creation succeeded create our renderer
        m_pRenderer = SDL_CreateRenderer(m_pWindow, -1, 0);
        if (m_pRenderer == nullptr)
        {
            std::cerr << "Error creating renderer: " << SDL_GetError() << std::endl;
            return false;
        }
        m_pFrameGenerator = new FrameGenerator(m_pRenderer, m_pWindow);
        std::cout << "renderer creation success!" << std::endl;
        SDL_SetRenderDrawColor(m_pRenderer, 255, 255, 255, 255);

        m_pGameStateMachine = new GameStateMachine();
        m_pGameStateMachine->changeState("MENU", new MenuState());
        std::cout << "initialization success!" << std::endl;
        m_bRunning = true; // everything initialized successfully, start the main loop
        return true;
    }
    return false;
}

void Game::render()
{
    SDL_RenderClear(m_pRenderer); // clear the renderer to the draw color
    m_pGameStateMachine->render();
    SDL_RenderPresent(m_pRenderer); // draw to the screen
}

void Game::update(Uint32 ticks)
{
    m_pGameStateMachine->update(ticks);
    handleEvents();
    // Check to see if we need to start making frames or not
    if (InputHandler::getInstance()->isKeyDown(SDL_SCANCODE_F4))
    {
        toggleMakeVideo();
    }
    // Make a frame if necessary
    if (m_bMakeVideo)
    {
        m_pFrameGenerator->makeFrame();
    }
}

void Game::handleEvents()
{
    InputHandler::getInstance()->update();
}

void Game::playGame()
{
    Uint32 ticks;

    while(m_bRunning)
    {
        //handleEvents();
        ticks = Clock::getInstance()->getElapsedTicks();
        if (ticks > 0)
        {
            Clock::getInstance()->incrementFrame();
            m_currentFrame = Clock::getInstance()->getFrames();
            render();
            update(ticks);
        }
    }
}

void Game::toggleMakeVideo()
{
    m_bMakeVideo = !m_bMakeVideo;
}

void Game::clean()
{
    delete myInstance;
}
