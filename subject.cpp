//
// Created by jacomus on 3/1/17.
//

#include "subject.h"

Subject::Subject() :
    m_observers()
{}

Subject::~Subject()
{
    m_observers.clear();
}

void Subject::Attach(Observer* o)
{
    m_observers.emplace_back(o);
}

void Subject::Detach(Observer* o)
{
    m_observers.remove(o);
}

void Subject::Notify(const Vector2D& pos)
{
    for (Observer* o : m_observers)
    {
        o->updateFromSubject(pos);
    }
}
