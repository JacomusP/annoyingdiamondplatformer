//
// Created by jacomus on 9/4/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_CRATE_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_CRATE_H

#include "gameObject.h"
#include "collisionBox.h"

class Crate : public GameObject
{
public:
    Crate(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
          const unsigned int numRows, const unsigned int numCols, const std::string& name,
          const int frameInterval, const unsigned int health, const unsigned int xOffset,
          const unsigned int yOffset, const unsigned int cbWidth, const unsigned int cbHeight);
    Crate(const Crate& c);
    Crate& operator=(const Crate& c);
    virtual ~Crate();
    virtual void draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const;
    virtual std::string type() const;
    virtual bool checkForCollision(const GameObject& gObj);
    virtual void update(Uint32 ticks, const GameState& gameState);
    bool containsLoot() const { return m_bContainsLoot; }
    GameObject* getContainedLoot() const { return m_containedLoot; }
    CollisionBox getCollisionBox() const { return m_collisionBox; }
    void depositLoot(const GameObject* loot);
    void dropLoot();

private:
    bool m_bContainsLoot;
    GameObject* m_containedLoot;
    CollisionBox m_collisionBox;
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_CRATE_H
