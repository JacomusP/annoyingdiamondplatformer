//
// Created by jacomus on 3/6/17.
//

#include <SDL_image.h>
#include "ioMod.h"
#include "gameData.h"
#include "game.h"

IO_Mod* IO_Mod::myInstance = nullptr;

IO_Mod* IO_Mod::getInstance()
{
    if (myInstance == nullptr)
    {
        myInstance = new IO_Mod();
    }
    return myInstance;
}

void IO_Mod::destroy()
{
    delete myInstance;
}

SDL_Texture* IO_Mod::readTexture(const std::string& filename)
{
    SDL_Texture* texture = IMG_LoadTexture(m_renderer, filename.c_str());
    if (texture == nullptr)
    {
        throw std::string("Couldn't load ") + filename + " as a texture!";
    }
    return texture;
}

SDL_Surface* IO_Mod::readSurface(const std::string& filename)
{
    SDL_Surface* surface = IMG_Load(filename.c_str());
    if (!surface)
    {
        throw std::string("Couldn't load ") + filename + " as a surface!";
    }
    return surface;
}

void IO_Mod::writeText(const std::string& msg, int x, int y) const
{
    int textWidth;
    int textHeight;

    SDL_Surface* surface = TTF_RenderText_Solid(m_font, msg.c_str(), m_textColor);
    SDL_Texture* texture = SDL_CreateTextureFromSurface(m_renderer, surface);
    textWidth = surface->w;
    textHeight = surface->h;
    SDL_FreeSurface(surface);
    SDL_Rect dst = {x, y, textWidth, textHeight};

    SDL_RenderCopy(m_renderer, texture, nullptr, &dst);
    SDL_DestroyTexture(texture);
}

void IO_Mod::writeText(const std::string& msg, int x, int y, SDL_Color textColor) const
{
    int textWidth;
    int textHeight;
    SDL_Surface* surface = TTF_RenderText_Solid(m_font, msg.c_str(), textColor);
    SDL_Texture* texture = SDL_CreateTextureFromSurface(m_renderer, surface);
    textWidth = surface->w;
    textHeight = surface->h;
    SDL_FreeSurface(surface);
    SDL_Rect dst = {x, y, textWidth, textHeight};

    SDL_RenderCopy(m_renderer, texture, nullptr, &dst);
    SDL_DestroyTexture(texture);
}

IO_Mod::IO_Mod() :
    m_init(TTF_Init()), m_font(TTF_OpenFont(GameData::getInstance()->getJsonString("GameData.font.file").c_str(),
                                            GameData::getInstance()->getJsonInt("GameData.font.size"))),
    m_textColor(), m_renderer(const_cast<SDL_Renderer*>(Game::getInstance()->getRenderer()))
{
    if (m_init == -1)
    {
        std::cout << "Error: Couldn't initialized font!" << TTF_GetError() << std::endl;
        throw std::string("error: Couldn't initialized font!");
    }
    if (m_font == nullptr)
    {
        std::cout << "Error: font not found! " << TTF_GetError() << std::endl;
        throw std::string("error: font not found!");
    }
    m_textColor.r = static_cast<Uint8>(GameData::getInstance()->getJsonInt("GameData.font.red"));
    m_textColor.g = static_cast<Uint8>(GameData::getInstance()->getJsonInt("GameData.font.green"));
    m_textColor.b = static_cast<Uint8>(GameData::getInstance()->getJsonInt("GameData.font.blue"));
    m_textColor.a = static_cast<Uint8>(GameData::getInstance()->getJsonInt("GameData.font.alpha"));
}

void IO_Mod::setTextColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
    m_textColor.r = r;
    m_textColor.g = g;
    m_textColor.b = b;
    m_textColor.a = a;
}

IO_Mod::~IO_Mod()
{
    TTF_CloseFont(m_font);
    TTF_Quit();
}
