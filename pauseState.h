//
// Created by jacomus on 3/23/17.
//

#ifndef BUILDINGDEPTH_PAUSESTATE_H
#define BUILDINGDEPTH_PAUSESTATE_H

#include <vector>
#include <string>
#include "gameState.h"
#include "gameObject.h"

class PauseState : public GameState
{
public:
    PauseState();
    virtual ~PauseState();
    virtual void update(Uint32 ticks);
    virtual void render();
    virtual bool onEnter();
    virtual bool onExit();
    virtual std::string getStateID() const;
private:
    std::vector<GameObject*> m_gameObjects;
    static const std::string s_pauseID;

    // Methods
    bool loadTextures();

    // call back functions
    static void s_pauseToMain();
    static void s_resumePlay();

    PauseState(const PauseState& pS) = delete;
    PauseState& operator=(const PauseState& pS) = delete;
};


#endif //BUILDINGDEPTH_PAUSESTATE_H
