//
// Created by jacomus on 7/11/17.
//

#include "gameObjectFactory.h"

GameObjectFactory::GameObjectFactory() : m_creators()
{}

GameObjectFactory::~GameObjectFactory()
{
    for (std::pair<std::string, BaseCreator*> c : m_creators)
    {
        delete c.second;
    }
}

bool GameObjectFactory::registerType(const std::string& typeID, const BaseCreator* pCreator)
{
    bool result = false;
    auto it = m_creators.find(typeID);

    // if the type is already registered, do nothing
    if (it != m_creators.end())
    {
        delete pCreator;
    }
    else
    {
        m_creators[typeID] = const_cast<BaseCreator*>(pCreator);
        result = true;
    }
    return result;
}

GameObject* GameObjectFactory::create(const std::string& typeID)
{
    GameObject* gObj = nullptr;
    auto it = m_creators.find(typeID);

    if (it == m_creators.end())
    {
        std::cout << "could not find type " << typeID << std::endl;
    }
    else
    {
        BaseCreator* pCreator = (*it).second;
        gObj = pCreator->createGameObject();
    }
    return gObj;
}
