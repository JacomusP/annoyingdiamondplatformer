//
// Created by jacomus on 2/28/17.
//

#ifndef POLYMORPHISMINACTION_TEXTUREMANAGEREXCEPTION_H
#define POLYMORPHISMINACTION_TEXTUREMANAGEREXCEPTION_H

#include <exception>
#include <string>

class TextureManagerException : public std::exception
{
public:
    TextureManagerException(const std::string& msg);
    TextureManagerException(const TextureManagerException& tME);
    virtual const char* what() const throw();

private:
    std::string m_message;

    TextureManagerException& operator=(const TextureManagerException& tME) = delete;
};


#endif //POLYMORPHISMINACTION_TEXTUREMANAGEREXCEPTION_H
