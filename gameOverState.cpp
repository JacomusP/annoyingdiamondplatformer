//
// Created by jacomus on 5/2/17.
//

#include "gameOverState.h"
#include "playState.h"
#include "menuState.h"
#include "game.h"
#include "gameData.h"
#include "textureManager.h"
#include "textureManagerException.h"
#include "menuButton.h"
#include "inputHandler.h"

const std::string GameOverState::s_gameOverID = "GAME_OVER";

GameOverState::GameOverState(const bool won) :
    GameState(), m_gameObjects(), m_won(won)
{}

GameOverState::~GameOverState()
{
    onExit();
}

void GameOverState::update(Uint32 ticks)
{
    if (GameState::getExited()) return; // if we have exited this state then return from this function
    for (unsigned int i = 0; i < m_gameObjects.size(); i++)
    {
        m_gameObjects[i]->update(ticks, *this);
    }
}

void GameOverState::render()
{
    if (GameState::getExited()) return; // if we have exited this state then return from this function
    for (unsigned int i = 0; i < m_gameObjects.size(); i++)
    {
        m_gameObjects[i]->draw(Game::getInstance()->getRenderer(), SDL_FLIP_NONE);
    }
    if (GameState::getExited()) return; // if we have exited this state then return from this function
    GameData* inst = GameData::getInstance();
    std::string id;
    if (m_won)
    {
        id = inst->getJsonString("GameData.gameOverState.textures.youWonText.id");
    }
    else
    {
        id = inst->getJsonString("GameData.gameOverState.textures.gameOverText.id");
    }
    TextureManager::getInstance()->draw(id, 500, 100, const_cast<SDL_Renderer*>(Game::getInstance()->getRenderer()));
}

bool GameOverState::onEnter()
{
    GameData* inst = GameData::getInstance();
    if (!loadTextures())
    {
        std::cerr << "gameOverState - Problem loading textures!" << std::endl;
        return false;
    }
    try
    {
        GameState::setExited(false);
        m_gameObjects.push_back(
            new MenuButton(Vector2D(inst->getJsonFloat("GameData.pauseState.gameObjects.MenuButton.posX"),
                                    inst->getJsonFloat("GameData.pauseState.gameObjects.MenuButton.posY")), Vector2D(),
                           Vector2D(), inst->getJsonString("GameData.pauseState.textures.menuButton.id"), 1, 3,
                           "playButt", 0, 0, s_returnToMain));
        m_gameObjects.push_back(
            new MenuButton(Vector2D(inst->getJsonFloat("GameData.pauseState.gameObjects.ResumeButton.posX"),
                                    inst->getJsonFloat("GameData.pauseState.gameObjects.ResumeButton.posY")),
                           Vector2D(), Vector2D(), inst->getJsonString("GameData.pauseState.textures.resumeButton.id"),
                           1, 3, "exitButt", 0, 0, s_restartPlay));
    }
    catch (const boost::property_tree::ptree_error& e)
    {
        std::cerr << "gameOverState - Error Reading JSON data! " << e.what() << std::endl;
        return false;
    }
    std::cout << "entering gameOverState" << std::endl;
    return true;
}

bool GameOverState::loadTextures()
{
    try
    {
        GameData* inst = GameData::getInstance();
        TextureManager::getInstance()->load(inst->getJsonString("GameData.pauseState.textures.menuButton.path"),
                                            inst->getJsonString("GameData.pauseState.textures.menuButton.id"),
                                            inst->getJsonInt("GameData.pauseState.gameObjects.MenuButton.width"),
                                            inst->getJsonInt("GameData.pauseState.gameObjects.MenuButton.height"),
                                            const_cast<SDL_Renderer*>(Game::getInstance()->getRenderer()));
        std::cout << "gameOverState - menuButton loaded" << std::endl;
        TextureManager::getInstance()->load(inst->getJsonString("GameData.pauseState.textures.resumeButton.path"),
                                            inst->getJsonString("GameData.pauseState.textures.resumeButton.id"),
                                            inst->getJsonInt("GameData.pauseState.gameObjects.ResumeButton.width"),
                                            inst->getJsonInt("GameData.pauseState.gameObjects.ResumeButton.height"),
                                            const_cast<SDL_Renderer*>(Game::getInstance()->getRenderer()));
        std::cout << "gameOverState - resumeButton loaded" << std::endl;
        TextureManager::getInstance()->load(inst->getJsonString("GameData.gameOverState.textures.gameOverText.path"),
                                            inst->getJsonString("GameData.gameOverState.textures.gameOverText.id"),
                                            inst->getJsonInt("GameData.gameOverState.messages.width"),
                                            inst->getJsonInt("GameData.gameOverState.messages.height"),
                                            const_cast<SDL_Renderer*>(Game::getInstance()->getRenderer()));
        TextureManager::getInstance()->load(inst->getJsonString("GameData.gameOverState.textures.youWonText.path"),
                                            inst->getJsonString("GameData.gameOverState.textures.youWonText.id"),
                                            inst->getJsonInt("GameData.gameOverState.messages.width"),
                                            inst->getJsonInt("GameData.gameOverState.messages.height"),
                                            const_cast<SDL_Renderer*>(Game::getInstance()->getRenderer()));
    }
    catch (const boost::property_tree::ptree_error& e)
    {
        std::cerr << "gameOverState - Error loading JSON data! " << e.what() << std::endl;
        return false;
    }
    catch (const TextureManagerException& tME)
    {
        std::cerr << "gameOverState - Error: " << tME.what();
        return false;
    }
    return true;
}

bool GameOverState::onExit()
{
    GameData* inst = GameData::getInstance();
    if (!GameState::getExited())
    {
        for (GameObject* sdlGO : m_gameObjects)
        {
            delete sdlGO;
        }
        m_gameObjects.clear();
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.pauseState.textures.menuButton.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.pauseState.textures.resumeButton.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.gameOverState.textures.gameOverText.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.gameOverState.textures.youWonText.id"));
        // reset the mouse button states to false
        InputHandler::getInstance()->reset();
    }
    std::cout << "exiting gameOverState" << std::endl;
    GameState::setExited(true);
    return true;
}

void GameOverState::s_returnToMain()
{
    Game::getInstance()->getStateMachine()->exitState("PLAY");
    Game::getInstance()->getStateMachine()->changeState("MENU", new MenuState());
}

void GameOverState::s_restartPlay()
{
    Game::getInstance()->getStateMachine()->exitState("PLAY");
    Game::getInstance()->getStateMachine()->changeState("PLAY", new PlayState());
}

std::string GameOverState::getStateID() const
{
    return s_gameOverID;
}
