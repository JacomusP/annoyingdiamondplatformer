//
// Created by jacomus on 2/7/17.
//

#include <iostream>
#include <SDL_image.h>
#include "textureManager.h"
#include "textureManagerException.h"

TextureManager* TextureManager::myInstance = nullptr;

TextureManager* TextureManager::getInstance()
{
    if (myInstance == nullptr)
    {
        myInstance = new TextureManager();
    }
    return myInstance;
}

TextureManager::TextureManager() : m_textureMap(), m_surfaceMap()
{}

TextureManager::~TextureManager()
{
    for (std::pair<std::string, Textures*> texture : m_textureMap)
    {
        SDL_DestroyTexture(texture.second->m_texture);
        delete texture.second;
    }
    for (std::pair<std::string, SDL_Surface*> surface : m_surfaceMap)
    {
        SDL_FreeSurface(surface.second);
    }
    m_textureMap.clear();
    m_surfaceMap.clear();
}

void TextureManager::load(std::string fileName, std::string id, int fWidth,
                          int fHeight, SDL_Renderer *pRenderer)
{
    SDL_Surface* pTempSurface = IMG_Load(fileName.c_str());

    if (pTempSurface != nullptr)
    {
        SDL_Texture* pTexture = SDL_CreateTextureFromSurface(const_cast<SDL_Renderer*>(pRenderer), pTempSurface);
        // If everything went ok, ad the texture to our list
        if (pTexture != nullptr)
        {
            m_textureMap[id] = new Textures(pTexture, fWidth, fHeight);
            m_surfaceMap[id] = pTempSurface;
        }
        else
        {
            throw TextureManagerException(std::string("Error creating texture from surface: ") +
                                          std::string(SDL_GetError()));
        }
    }
    else
    {
        throw TextureManagerException(std::string("Error loading image to a surface: ") +
                                      std::string(SDL_GetError()));
    }
}

void TextureManager::draw(std::string id, int x, int y, SDL_Renderer* pRenderer,
                          SDL_RendererFlip flip)
{
    SDL_Rect srcRect;
    SDL_Rect destRect;

    srcRect.x = 0;
    srcRect.y = 0;
    srcRect.w = m_textureMap[id]->m_frameWidth;
    srcRect.h = m_textureMap[id]->m_frameHeight;
    destRect.w = m_textureMap[id]->m_frameWidth;
    destRect.h = m_textureMap[id]->m_frameHeight;
    destRect.x = x;
    destRect.y = y;

    SDL_RenderCopyEx(pRenderer, m_textureMap[id]->m_texture, &srcRect, &destRect, 0, nullptr, flip);
}

void TextureManager::draw(std::string id, SDL_Rect source, int x, int y, int width, int height,
          SDL_Renderer* pRenderer, SDL_RendererFlip flip)
{
    SDL_Rect srcRect;
    SDL_Rect destRect;
    srcRect.x = source.x;
    srcRect.y = source.y;
    srcRect.w = source.w;
    srcRect.h = source.h;
    destRect.w = width;
    destRect.h = height;
    destRect.x = x;
    destRect.y = y;

    SDL_RenderCopyEx(pRenderer, m_textureMap[id]->m_texture, &srcRect, &destRect, 0, nullptr, flip);
}

void TextureManager::drawFrame(std::string id, int x, int y, int currentRow, int currentFrame,
               SDL_Renderer* pRenderer, SDL_RendererFlip flip)
{
    SDL_Rect srcRect;
    SDL_Rect destRect;

    srcRect.x = m_textureMap[id]->m_frameWidth * currentFrame;
    srcRect.y = m_textureMap[id]->m_frameHeight * (currentRow - 1);
    srcRect.w = m_textureMap[id]->m_frameWidth;
    srcRect.h = m_textureMap[id]->m_frameHeight;
    destRect.w = m_textureMap[id]->m_frameWidth;
    destRect.h = m_textureMap[id]->m_frameHeight;
    destRect.x = x;
    destRect.y = y;

    SDL_RenderCopyEx(pRenderer, m_textureMap[id]->m_texture, &srcRect, &destRect, 0, nullptr, flip);
}

void TextureManager::clearFromTextureMap(const std::string& textureID)
{
    if (m_textureMap[textureID] != nullptr)
    {
        SDL_DestroyTexture(m_textureMap[textureID]->m_texture);
        delete m_textureMap[textureID];
    }
    m_textureMap.erase(textureID);
    if (m_surfaceMap[textureID] != nullptr)
    {
        SDL_FreeSurface(m_surfaceMap[textureID]);
    }
    m_surfaceMap.erase(textureID);
}

void TextureManager::destroy()
{
    delete myInstance;
}

SDL_Surface* TextureManager::getSurface(const std::string& surfaceID)
{
    std::map<std::string, SDL_Surface*>::const_iterator it = m_surfaceMap.find(surfaceID);
    if (it != m_surfaceMap.end())
    {
        return it->second;
    }
    return nullptr;
}

int TextureManager::getWidth(const std::string& id) const
{
    auto it = m_textureMap.find(id);
    if (it != m_textureMap.end())
    {
        return it->second->m_frameWidth;
    }
    throw TextureManagerException("Texture " + id + " does not exist!");
}

int TextureManager::getHeight(const std::string& id) const
{
    auto it = m_textureMap.find(id);
    if (it != m_textureMap.end())
    {
        return it->second->m_frameHeight;
    }
    throw TextureManagerException("Texture " + id + " does not exist!");
}
