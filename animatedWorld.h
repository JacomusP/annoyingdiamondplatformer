//
// Created by jacomus on 3/31/17.
//

#ifndef BUILDINGDEPTH_ANIMATEDWORLD_H
#define BUILDINGDEPTH_ANIMATEDWORLD_H

#include <SDL_quit.h>
#include <map>
#include "world.h"
#include "spriteSheet.h"

class AnimatedWorld : public World
{
public:
    AnimatedWorld(const std::string& id, const int factor, const int picWidth, const int picHeight,
                  const int worldWidth, const int worldHeight, const int frameInterval,
                  const unsigned int numRows, const unsigned int numCols, const std::string& animationName,
                  const unsigned int row, const unsigned int rowSpan, const unsigned int startPos,
                  const unsigned int endPos);
    AnimatedWorld(const AnimatedWorld& aW) = delete;
    const AnimatedWorld& operator=(const AnimatedWorld& aW) = delete;
    virtual ~AnimatedWorld();
    virtual void draw();
    void update(const Uint32 ticks);
    void addAnimation(const std::string& name, const unsigned int row, const unsigned int rowSpan,
                      const unsigned int startPos, const unsigned int endPos);
    void setCurrentAnimation(const std::string& animationName);

private:
    int m_frameInterval;
    int m_timeSinceLastFrame;
    SpriteSheet m_spriteSheet;
    std::map<std::string, std::string> m_animationNames;

    // Methods
    void advanceToNextFrameOfAnimation(Uint32 ticks);
};


#endif //BUILDINGDEPTH_ANIMATEDWORLD_H
