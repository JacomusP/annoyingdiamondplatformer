//
// Created by jacomus on 2/14/17.
//

#ifndef ANIMATIONWITHSDL2_0_GAMESTATEMACHINE_H
#define ANIMATIONWITHSDL2_0_GAMESTATEMACHINE_H

#include <SDL.h>
#include <map>
#include <vector>
#include <string>
#include "gameState.h"

class GameStateMachine
{
public:
    GameStateMachine();
    // explicitly disallow those functions we do not want the compiler to write for us
    GameStateMachine(const std::map<std::string, GameState*>& gameStates) = delete;
    GameStateMachine(const GameStateMachine& gSM) = delete;
    GameStateMachine& operator=(const GameStateMachine& gSM) = delete;
    ~GameStateMachine();
    void pushState(const std::string& stateName, GameState* pState);
    void changeState(const std::string& stateName, GameState* pState);
    void exitState(const std::string& stateName);
    void popState();
    void update(Uint32 ticks);
    void render();
private:
    std::string m_currentState;
    std::string m_previousState;
    std::vector<std::string> m_statesToDelete;
    std::map<std::string, GameState*> m_gameStates;
};


#endif //ANIMATIONWITHSDL2_0_GAMESTATEMACHINE_H
