//
// Created by jacomus on 9/14/17.
//

#include "collisionBox.h"

CollisionBox::CollisionBox() :
    m_xOffset(0), m_yOffset(0), m_width(0), m_height(0)
{}

CollisionBox::CollisionBox(int xOff, int yOff, int width, int height) :
    m_xOffset(xOff), m_yOffset(yOff), m_width(width), m_height(height)
{}
