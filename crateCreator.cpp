//
// Created by jacomus on 9/4/17.
//

#include "crateCreator.h"
#include "crate.h"
#include "gameData.h"

CrateCreator::CrateCreator(const int gObjID, const std::string& jsonPath,
             const std::string& baseJsonPath, const std::string& texturePath, const std::string& crateName) :
    BaseCreator(gObjID, jsonPath, baseJsonPath, texturePath), m_baseCrateName(crateName)
{}

GameObject* CrateCreator::createGameObject()
{
    GameData* inst = GameData::getInstance();
    std::string gObjID = std::to_string(getGameObjectID());
    std::string jsonPath = getJsonPath();
    std::string baseJsonPath = getBaseJsonPath();
    std::string texturePath = getTexturePath();
    Crate* c = new Crate(Vector2D(0.0f, 0.0f), Vector2D(), Vector2D(), inst->getJsonString(getTexturePath() + ".id"), 1,
                         1, m_baseCrateName + gObjID, 0, 50, 2, 0, 19, 22);
    setGameObjectID(getGameObjectID() + 1);
    return c;
}
