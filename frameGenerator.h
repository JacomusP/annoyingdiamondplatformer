#ifndef POLYMORPHISMINACTION_FRAMEGENERATOR_H
#define POLYMORPHISMINACTION_FRAMEGENERATOR_H

#include <SDL.h>

class FrameGenerator 
{
public:
    FrameGenerator(SDL_Renderer* const r, SDL_Window* const win) :
        rend(r), window(win), frameCount(0)
    {}
    ~FrameGenerator() {}
    void makeFrame();
    unsigned int getFrameCount() const { return frameCount; }
private:
    SDL_Renderer* const rend;
    SDL_Window* const window;
    unsigned int frameCount;
    FrameGenerator(const FrameGenerator&) = delete;
    FrameGenerator& operator=(const FrameGenerator&) = delete;
};

#endif //POLYMORPHISMINACTION_FRAMEGENERATOR_H
