//
// Created by jacomus on 5/8/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_GAMEOBJECT_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_GAMEOBJECT_H

#include <string>
#include <map>
#include "vector2D.h"
#include "spriteSheet.h"
#include "explosion.h"
#include "gameState.h"

class GameObject
{
public:
    GameObject();
    GameObject(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
               const unsigned int numRows, const unsigned int numCols, const std::string& name,
               const int frameInterval, const unsigned int health);
    GameObject(const GameObject& gObj);
    GameObject& operator=(const GameObject& gObj);
    virtual ~GameObject();
    // Accessor Methods
    const Vector2D& getPosition() const { return m_position; }
    const Vector2D& getVelocity() const { return m_velocity; }
    const Vector2D& getAcceleration() const { return m_acceleration; }
    const SpriteSheet& getSpriteSheet() const { return m_spriteSheet; }
    const Explosion& getExplosion() const { return m_explosion; }
    bool isDead() const { return m_dead; }
    bool isExploded() const { return m_exploded; }
    bool isOnGround() const { return m_onGround; }
    bool wentLeftLast() const { return m_leftLast; }
    const std::string& getName() const { return m_name; }
    const std::string& getCurrentAnimation() const { return m_spriteSheet.getCurrentAnimation(); }
    int getFrameInterval() const { return m_frameInterval; }
    int getTimeSinceLastFrame() const { return m_timeSinceLastFrame; }
    int getHeight() const;
    int getWidth() const;

    // Mutator Methods
    void setPosition(const Vector2D& pos);
    void setPositionX(const float x);
    void setPositionY(const float y);
    void setVelocity(const Vector2D& vel);
    void setVelocityX(const float x);
    void setVelocityY(const float y);
    void advanceToNextFrameOfAnimation(Uint32 ticks);
    void setOnGround(const bool onGround);
    void setExploded(const bool exploded);
    void addAnimation(const std::string& name, const unsigned int row, const unsigned int rowSpan,
                      const unsigned int startPos, const unsigned int endPos);
    void setCurrentAnimation(const std::string& animationName);
    void setLeftLast(const bool lL);
    void takeDamage(const unsigned int amount);
    // Overridable Methods
    virtual void draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const;
    // Must be overriden Methods
    virtual std::string type() const = 0;
    virtual bool checkForCollision(const GameObject& gObj) = 0;
    virtual void update(Uint32 ticks, const GameState& gameState) = 0;
private:
    Vector2D m_position;
    Vector2D m_velocity;
    Vector2D m_acceleration;
    SpriteSheet m_spriteSheet;
    Explosion m_explosion;
    bool m_dead;
    bool m_exploded;
    bool m_onGround;
    bool m_leftLast;
    std::string m_name;
    std::map<std::string, std::string> m_animationNames;
    int m_frameInterval;
    int m_timeSinceLastFrame;
    unsigned int m_health;
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_GAMEOBJECT_H
