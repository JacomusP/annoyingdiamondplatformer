//
// Created by jacomus on 4/29/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_EXPLOSION_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_EXPLOSION_H

#include <list>
#include "chunk.h"

class Explosion
{
public:
    Explosion();
    Explosion(const Explosion& e);
    Explosion& operator=(const Explosion& e);
    ~Explosion();
    void draw(SDL_Renderer* pRenderer) const;
    void update(Uint32 ticks);
    void makeChunks(unsigned int n, const std::string& textureID, const Vector2D& position,
                    const Vector2D& velocity, const int width, const int height);
    unsigned int chunkCount() const { return m_chunkList.size(); }
    unsigned int freeCount() const { return m_freeList.size(); }
private:
    std::list<Chunk*> m_chunkList; // An explosion is a list of chunks
    std::list<Chunk*> m_freeList; // When a chunk goes out of range it goes here
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_EXPLOSION_H
