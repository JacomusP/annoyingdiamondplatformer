//
// Created by jacomus on 9/4/17.
//

#include "crate.h"
#include "weapon.h"
#include "collision.h"

Crate::Crate(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
             const unsigned int numRows, const unsigned int numCols, const std::string& name,
             const int frameInterval, const unsigned int health, const unsigned int xOffset,
             const unsigned int yOffset, const unsigned int cbWidth, const unsigned int cbHeight) :
    GameObject(pos, vel, accel, textureID, numRows, numCols, name, frameInterval, health),
    m_bContainsLoot(false), m_containedLoot(nullptr), m_collisionBox(xOffset, yOffset, cbWidth, cbHeight)
{}

Crate::Crate(const Crate& c) :
    GameObject(c), m_bContainsLoot(c.m_bContainsLoot), m_containedLoot(c.m_containedLoot),
    m_collisionBox(c.m_collisionBox)
{}

Crate& Crate::operator=(const Crate& c)
{
    if (this == &c)
    {
        return *this;
    }
    GameObject::operator=(c);
    m_bContainsLoot = c.m_bContainsLoot;
    m_containedLoot = c.m_containedLoot;
    m_collisionBox = c.m_collisionBox;
    return *this;
}

Crate::~Crate()
{
    std::cout << "Crate destructor" << std::endl;
    m_containedLoot = nullptr; // Don't need to delete this because it will be dropped and picked up by another object
}

void Crate::draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const
{
    GameObject::draw(pRenderer, flip);
}

std::string Crate::type() const
{
    return "Crate";
}

bool Crate::checkForCollision(const GameObject& gObj)
{
    return Collision::getInstance()->crateAndWeapon(this, &gObj);
}

void Crate::update(Uint32 ticks, const GameState& gameState)
{
    GameObject::update(ticks, gameState);
    if (isExploded())
    {
        if (m_bContainsLoot)
        {
            std::cout << getName() << " contains " << m_containedLoot->getName() << " ... dropping it now!" << std::endl;
            dropLoot();
        }
    }
}

void Crate::depositLoot(const GameObject* loot)
{
    m_bContainsLoot = true;
    m_containedLoot = const_cast<GameObject*>(loot);
}

void Crate::dropLoot()
{
    if (m_containedLoot->type() == "Weapon")
    {
        auto w = dynamic_cast<Weapon*>(m_containedLoot);
        w->setInBox(false);
        setPositionY(getPosition().getY() - 25);
        w->setPosition(getPosition());
    }
    m_bContainsLoot = false;
    m_containedLoot = nullptr;
}
