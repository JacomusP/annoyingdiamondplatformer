//
// Created by jacomus on 5/9/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_WEAPONCARRIER_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_WEAPONCARRIER_H

#include "gameObject.h"
#include "weapon.h"

class WeaponCarrier : public GameObject
{
public:
    WeaponCarrier(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel,
                      const std::string& textureID, const unsigned int numRows, const unsigned int numCols,
                      const std::string& name, const int frameInterval, const unsigned int health);
    WeaponCarrier(const WeaponCarrier& wC);
    WeaponCarrier& operator=(const WeaponCarrier& wC);
    virtual ~WeaponCarrier();
    // Accessor Methods
    const std::string& getEquippedWeaponName() { return m_equippedWeaponName; }
    Weapon* getEquippedWeapon() { return m_weapons[m_equippedWeaponName]; }
    unsigned int getWeaponPower() { return m_weapons[m_equippedWeaponName]->getPower(); }
    unsigned int getNumberOfWeapons() { return m_weapons.size(); }
    // Methods
    void equipWeapon(const std::string& weaponName);
    void unequipWeapon();
    void pickupWeapon(const std::string& weaponName, Weapon* weapon);
    bool hasWeapon(const std::string& weaponName);
    // Methods that can be overridden
    virtual void draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const;
    // Methods that must be overridden
    virtual std::string type() const;
    virtual bool checkForCollision(const GameObject& gObj);
    virtual void update(Uint32 ticks, const GameState& gameState);
private:
    std::map<std::string, Weapon*> m_weapons;
    std::string m_equippedWeaponName;
    Weapon* m_equippedWeapon;
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_WEAPONCARRIER_H
