//
// Created by jacomus on 9/14/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_COLLISIONBOX_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_COLLISIONBOX_H

class CollisionBox
{
public:
    CollisionBox();
    CollisionBox(int xOff, int yOff, int width, int height);
    CollisionBox(const CollisionBox& cBox) = default;
    CollisionBox& operator=(const CollisionBox& cBox) = default;
    ~CollisionBox() = default;
    // Accessor Methods
    int getXOffset() const { return m_xOffset; }
    int getYOffset() const { return m_yOffset; }
    int getWidth() const { return m_width; }
    int getHeight() const { return m_height; }

private:
    int m_xOffset;
    int m_yOffset;
    int m_width;
    int m_height;
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_COLLISIONBOX_H
