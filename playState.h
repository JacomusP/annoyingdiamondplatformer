//
// Created by jacomus on 6/9/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_GAMESTATE_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_GAMESTATE_H

#include <map>
#include <vector>
#include "gameState.h"
#include "gameObject.h"
#include "weapon.h"
#include "player.h"
#include "enemy.h"
#include "platform.h"
#include "crate.h"
#include "world.h"
#include "animatedWorld.h"
#include "hud.h"
#include "diamondManager.h"
#include "gameObjectFactory.h"
#include "randomWorldGenerator.h"

class PlayState : public GameState
{
public:
    PlayState();
    PlayState(const PlayState& pS) = delete;
    PlayState& operator=(const PlayState& pS) = delete;
    virtual ~PlayState();
    virtual void update(Uint32 ticks);
    virtual void render();
    virtual bool onEnter();
    virtual bool onExit();
    virtual std::string getStateID() const;
    void checkInput();

private:
    struct AnimationInfo
    {
        std::string name;
        unsigned int row;
        unsigned int rowSpan;
        unsigned int startPos;
        unsigned int endPos;

        AnimationInfo(const std::string& n, const unsigned int r, const unsigned int rS,
                      const unsigned int sP, const unsigned int eP):
            name(n), row(r), rowSpan(rS), startPos(sP), endPos(eP)
        {}
    };

    World* m_skyMountains;
    AnimatedWorld* m_cliffWaterfalls;
    std::vector<Weapon*> m_weapons;
    Player* m_player;
    std::vector<Enemy*> m_enemies;
    std::vector<Platform*> m_platforms;
    std::map<std::string, std::vector<AnimationInfo>> m_animations;
    Hud m_hud;
    DiamondManager* m_diamondManager;
    unsigned int m_numEnemiesDead;
    GameObjectFactory m_gameObjectFactory;
    RandomWorldGenerator m_randomWorldGenerator;
    bool m_playerCollidedWithPlatform;
    std::vector<bool> m_enemiesCollidedWithPlatform;
    std::vector<Crate*> m_crates;
    unsigned long m_numberOfEnemies;
    unsigned long m_numberOfWeapons;
    static const std::string s_playID;

    // Methods
    bool loadTextures();
    void initializeEnemyAnimations();
    void initializePlayerAnimations();
    void applyGravity();
    void checkIfAllEnemiesDead();
    void checkIfPlayerIsDead();
    void resetCollidedWithPlatformVariables();
    void checkWhichSideGameObjectCollidedWith(GameObject* gObj, const CollisionObject* cObj);
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_GAMESTATE_H
