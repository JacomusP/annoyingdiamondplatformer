//
// Created by jacomus on 7/11/17.
//

#include "enemyCreator.h"
#include "enemy.h"
#include "gameData.h"

EnemyCreator::EnemyCreator(const int gObjID, const std::string& jsonPath,
             const std::string& baseJsonPath, const std::string& texturePath, const std::string& enemyName) :
    BaseCreator(gObjID, jsonPath, baseJsonPath, texturePath), m_baseEnemyName(enemyName)
{}

GameObject* EnemyCreator::createGameObject()
{
    GameData* inst = GameData::getInstance();
    std::string gObjID = std::to_string(getGameObjectID());
    std::string jsonPath = getJsonPath();
    std::string baseJsonPath = getBaseJsonPath();
    std::string texturePath = getTexturePath();
    Enemy* e = new Enemy(Vector2D(0, 0), Vector2D(), Vector2D(), inst->getJsonString(texturePath + ".id"),
                         inst->getJsonUInt(texturePath + ".rows"), inst->getJsonUInt(texturePath + ".cols"),
                         m_baseEnemyName + gObjID, inst->getJsonInt(baseJsonPath + ".frameInterval"),
                         inst->getJsonUInt(baseJsonPath + ".health"), 0, 0, 0);
    setGameObjectID(getGameObjectID() + 1);
    return e;
}
