//
// Created by jacomus on 4/29/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_CHUNK_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_CHUNK_H

#include <SDL.h>
#include "vector2D.h"

class Chunk
{
public:
    Chunk(const std::string& textureID, const Vector2D& position, const Vector2D& velocity, const SDL_Rect source,
          int width, int height);
    Chunk(const Chunk& c);
    Chunk& operator=(const Chunk& c);
    ~Chunk();
    void draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip);
    void update(Uint32 ticks);
    bool goneTooFar() const { return m_bTooFar; }
    void reset();
private:
    Vector2D m_position;
    Vector2D m_velocity;
    std::string m_textureID;
    float m_distance;
    float m_maxDistance;
    bool m_bTooFar;
    SDL_Rect m_source;
    int m_width;
    int m_height;
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_CHUNK_H
