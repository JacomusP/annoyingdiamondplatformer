//
// Created by jacomus on 3/25/17.
//

#include "spriteSheet.h"

SpriteSheet::SpriteSheet(const std::string& textureID, const unsigned int numRows, const unsigned int numCols) :
    m_mapAnimations(), m_textureID(textureID), m_currentAnimation(), m_currentFrame(0),
    m_currentRow(1), m_numRows(numRows), m_numCols(numCols)
{}

SpriteSheet::SpriteSheet(const SpriteSheet& sS) :
    m_mapAnimations(), m_textureID(sS.m_textureID), m_currentAnimation(sS.m_currentAnimation),
    m_currentFrame(sS.m_currentFrame), m_currentRow(sS.m_currentRow),
    m_numRows(sS.m_numRows), m_numCols(sS.m_numCols)
{
    // Insert the animations
    insertAnimations(sS.m_mapAnimations);
}

SpriteSheet::~SpriteSheet()
{
    freeAnimations();
}

SpriteSheet& SpriteSheet::operator=(const SpriteSheet& sS)
{
    if (this == &sS)
    {
        return *this;
    }
    // Delete the animations
    freeAnimations();
    // Insert the animations
    insertAnimations(sS.m_mapAnimations);
    // Copy data members
    m_textureID = sS.m_textureID;
    m_currentAnimation = sS.m_currentAnimation;
    m_currentFrame = sS.m_currentFrame;
    m_currentRow = sS.m_currentRow;
    m_numRows = sS.m_numRows;
    m_numCols = sS.m_numCols;
    return *this;
}

void SpriteSheet::addAnimation(const std::string &name, const unsigned int row, const unsigned int rowSpan,
                               const unsigned int startPos, const unsigned int endPos)
{
    // If the element already exists delete the memory allocated for it and remove it from the map
    if (m_mapAnimations.find(name) != m_mapAnimations.end())
    {
        delete m_mapAnimations[name];
        m_mapAnimations.erase(name);
    }
    // Add the element to the map
    m_mapAnimations[name] = new Animation(name, row, rowSpan, startPos, endPos);
}

void SpriteSheet::advanceToNextFrame()
{
    // increment current frame
    m_currentFrame += 1;
    // check to see if it went past the end Position of the animation
    if (m_currentFrame > m_mapAnimations[m_currentAnimation]->m_endPos)
    {
        // does the animation span more then one row
        if (m_mapAnimations[m_currentAnimation]->m_rowSpan > 1)
        {
            m_currentRow -= (m_mapAnimations[m_currentAnimation]->m_rowSpan - 1);
        }
        m_currentFrame = m_mapAnimations[m_currentAnimation]->m_startPos;
    }
    // check to see if it went past the end of the row
    //else if ((m_currentFrame % m_numCols) > m_numCols)
    else if (m_currentFrame >= (m_currentRow * m_numCols))
    {
        // check to see if animation continues on the next row
        if (m_mapAnimations[m_currentAnimation]->m_rowSpan > 1)
        {
            // if it does increment current row and reset current column to 0 and set currentFrame to 0 as well
            m_currentRow += 1;
        }
        else // if it doesn't wrap current frame back to start pos
        {
            m_currentFrame = m_mapAnimations[m_currentAnimation]->m_startPos;
        }
    }
}

void SpriteSheet::setCurrentAnimation(const std::string& name)
{
    m_currentAnimation = name;
    m_currentFrame = m_mapAnimations[m_currentAnimation]->m_startPos;
    m_currentRow = m_mapAnimations[m_currentAnimation]->m_row;
}

void SpriteSheet::setCurrentFrame(const unsigned int frame)
{
    m_currentFrame = frame;
}

void SpriteSheet::freeAnimations()
{
    for (std::pair<std::string, Animation*> ani : m_mapAnimations)
    {
        delete ani.second;
    }
    m_mapAnimations.clear();
}

void SpriteSheet::insertAnimations(const std::map<std::string, Animation*> animations)
{
    for (std::pair<std::string, Animation*> ani : animations)
    {
        m_mapAnimations[ani.first] = new Animation(ani.second->m_name, ani.second->m_row,
                                                   ani.second->m_rowSpan, ani.second->m_startPos,
                                                   ani.second->m_endPos);
    }
}
