//
// Created by jacomus on 3/23/17.
//

#ifndef BUILDINGDEPTH_MENUSTATE_H
#define BUILDINGDEPTH_MENUSTATE_H

#include <vector>
#include <string>
#include "gameState.h"
#include "gameObject.h"
#include "world.h"
#include "animatedWorld.h"

class MenuState : public GameState
{
public:
    MenuState();
    virtual ~MenuState();
    virtual void update(Uint32 ticks);
    virtual void render();
    virtual bool onEnter();
    virtual bool onExit();
    virtual std::string getStateID() const;
private:
    World* m_skyMountains;
    AnimatedWorld* m_waterfallCliffs;
    World* m_platforms;
    std::vector<GameObject*> m_gameObjects;
    static const std::string s_menuID;

    // Methods
    bool loadTextures();

    // call back functions for menu items
    static void s_menuToPlay();
    static void s_exitFromMenu();

    MenuState(const MenuState& mS) = delete;
    MenuState& operator=(const MenuState& mS) = delete;
};


#endif //BUILDINGDEPTH_MENUSTATE_H
