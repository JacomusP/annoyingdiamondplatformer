//
// Created by jacomus on 5/9/17.
//

#include "weaponCarrier.h"
#include "collision.h"

WeaponCarrier::WeaponCarrier(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel,
                             const std::string& textureID, const unsigned int numRows, const unsigned int numCols,
                             const std::string& name, const int frameInterval, const unsigned int health) :
    GameObject(pos, vel, accel, textureID, numRows, numCols, name, frameInterval, health),
    m_weapons(), m_equippedWeaponName(), m_equippedWeapon(nullptr)
{}

WeaponCarrier::WeaponCarrier(const WeaponCarrier& wC) :
    GameObject(wC), m_weapons(wC.m_weapons), m_equippedWeaponName(wC.m_equippedWeaponName),
    m_equippedWeapon(wC.m_equippedWeapon)
{}

WeaponCarrier& WeaponCarrier::operator=(const WeaponCarrier& wC)
{
    if (this == &wC)
    {
        return *this;
    }
    GameObject::operator=(wC);
    m_weapons = wC.m_weapons;
    m_equippedWeaponName = wC.m_equippedWeaponName;
    m_equippedWeapon = wC.m_equippedWeapon;
    return *this;
}

WeaponCarrier::~WeaponCarrier()
{
    std::cout << "WeaponCarrier destructor" << std::endl;
    for (std::pair<std::string, Weapon*> blah : m_weapons)
    {
        delete blah.second;
    }
    m_weapons.clear();
}

void WeaponCarrier::pickupWeapon(const std::string& weaponName, Weapon* weapon)
{
    std::map<std::string, Weapon*>::const_iterator it = m_weapons.find(weaponName);
    if (it == m_weapons.end())
    {
        m_weapons[weaponName] = weapon;
        std::cout << "Added " << weaponName << " weapon!" << std::endl;
        m_weapons[weaponName]->pickUp();
    }
    else
    {
        std::cout << "You already have that weapon! You can't have two!" << std::endl;
    }
    if (m_equippedWeaponName.empty())
    {
        equipWeapon(weaponName);
    }
}

void WeaponCarrier::equipWeapon(const std::string& weaponName)
{
    m_equippedWeaponName = weaponName;
    m_weapons[m_equippedWeaponName]->setOwner(this);
    m_weapons[m_equippedWeaponName]->setPosition(getPosition());
    m_weapons[m_equippedWeaponName]->setCurrentAnimation(getSpriteSheet().getCurrentAnimation());
    m_weapons[m_equippedWeaponName]->equip();
    m_equippedWeapon = m_weapons[m_equippedWeaponName];
}

void WeaponCarrier::unequipWeapon()
{
    m_equippedWeaponName.clear();
    m_equippedWeapon->unequip();
    m_equippedWeapon = nullptr;
}

void WeaponCarrier::draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const
{
    GameObject::draw(pRenderer, flip);
    if (!isDead())
    {
        if (!m_equippedWeaponName.empty())
        {
            m_equippedWeapon->draw(pRenderer, SDL_FLIP_NONE);
        }
    }
}

bool WeaponCarrier::checkForCollision(const GameObject& gObj)
{
    return Collision::getInstance()->weaponCarrierAndGameObject(this, &gObj);
}

void WeaponCarrier::update(Uint32 ticks, const GameState& gState)
{
    if (!m_equippedWeaponName.empty())
    {
        m_weapons[m_equippedWeaponName]->update(ticks, gState);
        m_weapons[m_equippedWeaponName]->setPosition(getPosition());
    }
    GameObject::update(ticks, gState);
}

bool WeaponCarrier::hasWeapon(const std::string& weaponName)
{
    return m_weapons.find(weaponName) != m_weapons.end();
}

std::string WeaponCarrier::type() const
{
    return "WeaponCarrier";
}
