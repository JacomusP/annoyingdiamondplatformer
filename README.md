# AnnoyingDiamondPlatformer

This repo houses my completed game called Annoying Diamond Platformer.

This was a project that I continued from a class project. My vision for the game was not fully realized yet so I decided to keep going and implement everything that I wanted to put in the game and that resulted
in everything that is in this repo. I'm very proud to say that I finally have a completed game under my belt and I am excited to work on my next one.

Game play video can be found on my development instagram page https://www.instagram.com/jacomusp_dev/