//
// Created by jacomus on 8/25/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_RANDOMPLATFORMGENERATOR_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_RANDOMPLATFORMGENERATOR_H

#include <vector>
#include <random>
#include "vector2D.h"
#include "gameObject.h"
#include "gameObjectFactory.h"
#include "platform.h"
#include "player.h"
#include "enemy.h"
#include "weapon.h"
#include "crate.h"

class RandomWorldGenerator
{
public:
    RandomWorldGenerator(int rangeBegin, int rangeEnd);
    ~RandomWorldGenerator() = default;
    RandomWorldGenerator(const RandomWorldGenerator& rPG) = delete;
    RandomWorldGenerator(RandomWorldGenerator&& rPG) = delete;
    RandomWorldGenerator& operator=(const RandomWorldGenerator& rPG) = delete;
    RandomWorldGenerator& operator=(RandomWorldGenerator&& rPG) = delete;
    // Identifies whether the position in v is a valid place for a platform
    bool canPlacePlatformHere(const Vector2D& v, const Vector2D& lastPosition, const int iteration);
    bool willPlaceEnemyHere();
    GameObject* placePlatformHere(Vector2D& pos, const Vector2D& lastPosition, GameObjectFactory& gOF,
                                  const std::string& platType);
    GameObject* placePlayerHere(const Vector2D& pos, GameObjectFactory& gOF, const std::string& playerString);
    GameObject* placeWeaponHere(const Vector2D& pos, GameObjectFactory& gOF, const std::string& weaponName,
                                const bool inBox);
    GameObject* placeEnemyHere(const Vector2D& pos, GameObjectFactory& gOF, const std::string& enemyType);
    GameObject* placeCrateHere(Vector2D& pos, GameObjectFactory& gOF, const std::string& crateType);
    bool generateWorld(std::vector<Platform*>& platforms, Player** player, std::vector<Enemy*>& enemies,
                       std::vector<Crate*>& crates, std::vector<Weapon*>& weapons, GameObjectFactory& gOF);

private:
    std::random_device m_randomDevice;
    std::mt19937 m_randomNumberEngine;
    std::uniform_int_distribution<> m_distribution;
    // Private Methods
    std::string determineWhichWeaponToPlaceInCrate(bool* spearP, bool* battleAxeP,
                                                   bool* glaiveP, bool* scimitarP, bool* spindarP);
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_RANDOMPLATFORMGENERATOR_H
