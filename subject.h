//
// Created by jacomus on 3/1/17.
//

#ifndef POLYMORPHISMINACTION_SUBJECT_H
#define POLYMORPHISMINACTION_SUBJECT_H

#include <list>
#include "observer.h"
#include "vector2D.h"

class Subject
{
public:
    virtual ~Subject();

    virtual void Attach(Observer* o);
    virtual void Detach(Observer* o);
    virtual void Notify(const Vector2D& p);

protected:
    Subject();
private:
    std::list<Observer*> m_observers;
};


#endif //POLYMORPHISMINACTION_SUBJECT_H
