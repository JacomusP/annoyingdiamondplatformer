//
// Created by jacomus on 5/23/17.
//

#include "bullet.h"
#include "gameData.h"
#include "collision.h"

Bullet::Bullet(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
               const unsigned int numRows, const unsigned int numCols, const std::string& name, const int frameInterval,
               const unsigned int health) :
    GameObject(pos, vel, accel, textureID, numRows, numCols, name, frameInterval, health),
    m_bTooFar(false), m_distance(0.0f),
    m_maxDistance(GameData::getInstance()->getJsonFloat("GameData.playState.bullet.maxDistance")),
    m_startingPosition(getPosition())
{}

Bullet::Bullet(const Bullet& b) :
    GameObject(b), m_bTooFar(b.m_bTooFar), m_distance(b.m_distance), m_maxDistance(b.m_maxDistance),
    m_startingPosition(b.m_startingPosition)
{}

Bullet& Bullet::operator=(const Bullet& b)
{
    if (this == &b)
    {
        return *this;
    }
    GameObject::operator=(b);
    m_bTooFar = b.m_bTooFar;
    m_distance = b.m_distance;
    m_maxDistance = b.m_maxDistance;
    m_startingPosition = b.m_startingPosition;
    return *this;
}

Bullet::~Bullet()
{
    //std::cout << "Bullet destructor" << std::endl;
}

void Bullet::reset()
{
    m_bTooFar = false;
    m_distance = 0.0f;
}

void Bullet::draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const
{
    GameObject::draw(pRenderer, flip);
}

std::string Bullet::type() const
{
    return "Bullet";
}

bool Bullet::checkForCollision(const GameObject& gObj)
{
    return Collision::getInstance()->bulletAndGameObject(this, &gObj);
}

void Bullet::update(Uint32 ticks, const GameState& gameState)
{
    GameObject::advanceToNextFrameOfAnimation(ticks);
    GameObject::update(ticks, gameState);
    m_distance = std::fabs(m_startingPosition.getX() - getPosition().getX());
    if (m_distance > m_maxDistance)
    {
        m_bTooFar = true;
    }
}
