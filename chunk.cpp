//
// Created by jacomus on 4/29/17.
//

#include "chunk.h"
#include "textureManager.h"
#include "viewPort.h"
#include "gameData.h"

Chunk::Chunk(const std::string& textureID, const Vector2D& position,
             const Vector2D& velocity, const SDL_Rect source, int width, int height) :
    m_position(position), m_velocity(velocity),
    m_textureID(textureID), m_distance(0.0),
    m_maxDistance(GameData::getInstance()->getJsonFloat("GameData.playState.chunk.maxDistance")),
    m_bTooFar(false), m_source(source), m_width(width), m_height(height)
{}

Chunk::Chunk(const Chunk& c) :
    m_position(c.m_position), m_velocity(c.m_velocity), m_textureID(c.m_textureID),
    m_distance(c.m_distance), m_maxDistance(c.m_maxDistance),
    m_bTooFar(c.m_bTooFar), m_source(c.m_source), m_width(c.m_width), m_height(c.m_height)
{}

Chunk& Chunk::operator=(const Chunk& c)
{
    if (this == &c) return *this;
    m_position = c.m_position;
    m_velocity = c.m_velocity;
    m_textureID = c.m_textureID;
    m_distance = c.m_distance;
    m_maxDistance = c.m_maxDistance;
    m_bTooFar = c.m_bTooFar;
    m_source = c.m_source;
    m_width = c.m_width;
    m_height = c.m_height;
    return *this;
}

Chunk::~Chunk()
{
    //std::cout << "Chunk destructor!" << std::endl;
}

void Chunk::draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip)
{
    float x = m_position.getX();
    float y = m_position.getY();
    x -= ViewPort::getInstance()->getX();
    y -= ViewPort::getInstance()->getY();
    TextureManager::getInstance()->draw(m_textureID, m_source, static_cast<int>(x),
                                        static_cast<int>(y), m_width, m_height,
                                        const_cast<SDL_Renderer*>(pRenderer), flip);
}

void Chunk::update(Uint32 ticks)
{
    float yincr = m_velocity.getY() * static_cast<float>(ticks) * 0.001f;
    m_position.setY(m_position.getY() - yincr);
    float xincr = m_velocity.getX() * static_cast<float>(ticks) * 0.001f;
    m_position.setX(m_position.getX() - xincr);
    m_distance += (hypot(xincr, yincr));
    if (m_distance > m_maxDistance)
    {
        m_bTooFar = true;
    }
}

void Chunk::reset()
{
    m_bTooFar = false;
    m_distance = 0.0;
}
