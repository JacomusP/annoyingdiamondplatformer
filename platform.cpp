//
// Created by jacomus on 5/29/17.
//

#include "platform.h"

Platform::Platform() :
    CollisionObject()
{}

Platform::Platform(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
                   const unsigned int numRows, const unsigned int numCols, const std::string& name, const int frameInterval,
                   const unsigned int health, const int colHeight, const int colWidth) :
    CollisionObject(pos, vel, accel, textureID, numRows, numCols, name, frameInterval, health, colHeight, colWidth)
{}

Platform::Platform(const Platform& p) :
    CollisionObject(p)
{}

Platform& Platform::operator=(const Platform& p)
{
    if (this == &p)
    {
        return *this;
    }
    CollisionObject::operator=(p);
    return *this;
}

Platform::~Platform()
{
    std::cout << "Platform destructor" << std::endl;
}

void Platform::draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const
{
    CollisionObject::draw(pRenderer, flip);
}

std::string Platform::type() const
{
    return CollisionObject::type() + ": Platform";
}

bool Platform::checkForCollision(const GameObject& gObj)
{
    return CollisionObject::checkForCollision(gObj);
}

void Platform::update(Uint32 ticks, const GameState& gameState)
{
    CollisionObject::update(ticks, gameState);
}
