//
// Created by jacomus on 5/29/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_PLATFORM_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_PLATFORM_H

#include "collisionObject.h"

class Platform : public CollisionObject
{
public:
    Platform();
    Platform(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
                 const unsigned int numRows, const unsigned int numCols, const std::string& name, const int frameInterval,
                 const unsigned int health, const int colHeight, const int colWidth);
    Platform(const Platform& p);
    Platform& operator=(const Platform& p);
    virtual ~Platform();
    virtual void draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const;
    virtual std::string type() const;
    virtual bool checkForCollision(const GameObject& gObj);
    virtual void update(Uint32 ticks, const GameState& gameState);
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_PLATFORM_H
