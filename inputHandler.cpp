//
// Created by jacomus on 2/14/17.
//

#include "inputHandler.h"
#include "game.h"

InputHandler* InputHandler::myInstance = nullptr;

InputHandler* InputHandler::getInstance()
{
    if (myInstance == nullptr)
    {
        myInstance = new InputHandler();
    }
    return myInstance;
}

InputHandler::InputHandler() :
    m_mouseButtonStates(), m_mousePosition(new Vector2D()), m_keyStates(nullptr)
{
    for (int i = 0; i < 3; i++)
    {
        m_mouseButtonStates.push_back(false);
    }
}

InputHandler::~InputHandler()
{
    delete m_mousePosition;
}

void InputHandler::reset()
{
    m_mouseButtonStates[mouse_buttons::LEFT] = false;
    m_mouseButtonStates[mouse_buttons::RIGHT] = false;
    m_mouseButtonStates[mouse_buttons::MIDDLE] = false;
}

void InputHandler::update()
{
    SDL_Event event;
    while(SDL_PollEvent(&event) != 0)
    {
        switch(event.type)
        {
            case SDL_QUIT:
                Game::getInstance()->quit();
                break;
            case SDL_MOUSEMOTION:
                onMouseMove(event);
                break;
            case SDL_MOUSEBUTTONDOWN:
                onMouseButtonDown(event);
                break;
            case SDL_MOUSEBUTTONUP:
                onMouseButtonUp(event);
                break;
            case SDL_KEYDOWN:
                onKeyDown();
                break;
            case SDL_KEYUP:
                onKeyUp();
                break;
            default:
                break;
        }
    }
}

void InputHandler::onMouseMove(SDL_Event& event)
{
    m_mousePosition->setX(event.motion.x);
    m_mousePosition->setY(event.motion.y);
}

void InputHandler::onMouseButtonDown(SDL_Event& event)
{
    if (event.button.button == SDL_BUTTON_LEFT)
    {
        m_mouseButtonStates[mouse_buttons::LEFT] = true;
    }
    if (event.button.button == SDL_BUTTON_MIDDLE)
    {
        m_mouseButtonStates[mouse_buttons::MIDDLE] = true;
    }
    if (event.button.button == SDL_BUTTON_RIGHT)
    {
        m_mouseButtonStates[mouse_buttons::RIGHT] = true;
    }
}

void InputHandler::onMouseButtonUp(SDL_Event& event)
{
    if (event.button.button == SDL_BUTTON_LEFT)
    {
        m_mouseButtonStates[mouse_buttons::LEFT] = false;
    }
    if (event.button.button == SDL_BUTTON_MIDDLE)
    {
        m_mouseButtonStates[mouse_buttons::MIDDLE] = false;
    }
    if (event.button.button == SDL_BUTTON_RIGHT)
    {
        m_mouseButtonStates[mouse_buttons::RIGHT] = false;
    }
}

bool InputHandler::getMouseButtonState(int buttonNumber)
{
    return m_mouseButtonStates[buttonNumber];
}

Vector2D* InputHandler::getMousePosition() const
{
    return m_mousePosition;
}

bool InputHandler::isKeyDown(SDL_Scancode key)
{
    if (m_keyStates != nullptr)
    {
        return (m_keyStates[key] == 1);
    }
    return false;
}

void InputHandler::onKeyDown()
{
    m_keyStates = SDL_GetKeyboardState(nullptr);
}

void InputHandler::onKeyUp()
{
    m_keyStates = SDL_GetKeyboardState(nullptr);
}

void InputHandler::clean()
{
    delete myInstance;
}
