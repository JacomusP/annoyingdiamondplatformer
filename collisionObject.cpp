//
// Created by jacomus on 5/29/17.
//

#include "collisionObject.h"
#include "collision.h"

CollisionObject::CollisionObject() :
    GameObject(), m_collisionHeight(0), m_collisionWidth(0), m_collisionSide(COLLISION_SIDE::NONE),
    m_top(0, 0, 0, 0), m_right(0, 0, 0, 0), m_bottom(0, 0, 0, 0), m_left(0, 0, 0, 0)
{}

CollisionObject::CollisionObject(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
                                 const unsigned int numRows, const unsigned int numCols, const std::string& name,
                                 const int frameInterval, const unsigned int health, const int colHeight, const int colWidth)
    :
    GameObject(pos, vel, accel, textureID, numRows, numCols, name, frameInterval, health),
    m_collisionHeight(colHeight), m_collisionWidth(colWidth), m_collisionSide(COLLISION_SIDE::NONE),
    m_top(pos.getX(), pos.getX() + getWidth(), pos.getY() + colHeight, pos.getY() + colHeight * 2),
    m_right(pos.getX() + getWidth() - colWidth, pos.getX() + getWidth(), pos.getY() + colHeight * 2,
            pos.getY() + getHeight() - colHeight),
    m_bottom(pos.getX(), pos.getX() + getWidth(), pos.getY() + getHeight() - colHeight, pos.getY() + getHeight()),
    m_left(pos.getX(), pos.getX() + colWidth, pos.getY() + colHeight * 2, pos.getY() + getHeight() - colHeight)
{}

CollisionObject::CollisionObject(const CollisionObject& cObj) :
    GameObject(cObj), m_collisionHeight(cObj.m_collisionHeight), m_collisionWidth(cObj.m_collisionWidth),
    m_collisionSide(cObj.m_collisionSide), m_top(cObj.m_top), m_right(cObj.m_right), m_bottom(cObj.m_bottom),
    m_left(cObj.m_left)
{}

CollisionObject& CollisionObject::operator=(const CollisionObject& cObj)
{
    if (this == &cObj)
    {
        return *this;
    }
    GameObject::operator=(cObj);
    m_collisionHeight = cObj.m_collisionHeight;
    m_collisionWidth = cObj.m_collisionWidth;
    m_collisionSide = cObj.m_collisionSide;
    m_top = cObj.m_top;
    m_right = cObj.m_right;
    m_bottom = cObj.m_bottom;
    m_left = cObj.m_left;
    return *this;
}

CollisionObject::~CollisionObject()
{
    //std::cout << "CollisionObject destructor" << std::endl;
}

void CollisionObject::draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const
{
    GameObject::draw(pRenderer, flip);
}

std::string CollisionObject::type() const
{
    return "CollisionObject";
}

bool CollisionObject::checkForCollision(const GameObject& gObj)
{
    return Collision::getInstance()->gameObjectsAndCollisionObject(&gObj, this);
}

void CollisionObject::update(Uint32 ticks, const GameState& gameState)
{
    GameObject::update(ticks, gameState);
}

void CollisionObject::setCollisionSideTop()
{
    m_collisionSide = COLLISION_SIDE::TOP;
}

void CollisionObject::setCollisionSideLeft()
{
    m_collisionSide = COLLISION_SIDE::LEFT;
}

void CollisionObject::setCollisionSideBottom()
{
    m_collisionSide = COLLISION_SIDE::BOTTOM;
}

void CollisionObject::setCollisionSideRight()
{
    m_collisionSide = COLLISION_SIDE::RIGHT;
}

void CollisionObject::setCollisionSideNone()
{
    m_collisionSide = COLLISION_SIDE::NONE;
}

bool CollisionObject::whichCollisionSide(const std::string& side) const
{
    if (side == "TOP")
    {
        return (m_collisionSide == COLLISION_SIDE::TOP);
    }
    else if (side == "RIGHT")
    {
        return (m_collisionSide == COLLISION_SIDE::RIGHT);
    }
    else if (side == "BOTTOM")
    {
        return (m_collisionSide == COLLISION_SIDE::BOTTOM);
    }
    else if (side == "LEFT")
    {
        return (m_collisionSide == COLLISION_SIDE::LEFT);
    }
    else
    {
        return false;
    }
}

void CollisionObject::updateCollisionBoxes()
{
    float x = getPosition().getX();
    float y = getPosition().getY();
    int w = getWidth();
    int h = getHeight();
    m_top.uLeft = Vector2D(x, y + m_collisionHeight);
    m_top.uRight = Vector2D(x + w, y + m_collisionHeight);
    m_top.lLeft = Vector2D(x, y + m_collisionHeight * 2);
    m_top.lRight = Vector2D(x + w, y + m_collisionHeight * 2);
    m_right.uLeft = Vector2D(x + w - m_collisionWidth, y + m_collisionHeight * 2);
    m_right.uRight = Vector2D(x + w, y + m_collisionHeight * 2);
    m_right.lLeft = Vector2D(x + w - m_collisionWidth, y + h - m_collisionHeight);
    m_right.lRight = Vector2D(x + w, y + h - m_collisionHeight);
    m_bottom.uLeft = Vector2D(x, y + h - m_collisionHeight);
    m_bottom.uRight = Vector2D(x + w, y + h - m_collisionHeight);
    m_bottom.lLeft = Vector2D(x, y + h);
    m_bottom.lRight = Vector2D(x + w, y + h);
    m_left.uLeft = Vector2D(x, y + (m_collisionHeight * 2) + 1);
    m_left.uRight = Vector2D(x + m_collisionWidth, y + (m_collisionHeight * 2) + 1);
    m_left.lLeft = Vector2D(x, y + h - m_collisionHeight);
    m_left.lRight = Vector2D(x + m_collisionWidth, y + h - m_collisionHeight);
}

void CollisionObject::enableOrDisableTopCollisionBox(const bool b)
{
    m_top.active = b;
}

void CollisionObject::enableOrDisableRightCollisionBox(const bool b)
{
    m_right.active = b;
}

void CollisionObject::enableOrDisableBottomCollisionBox(const bool b)
{
    m_bottom.active = b;
}

void CollisionObject::enableOrDisableLeftCollisionBox(const bool b)
{
    m_left.active = b;
}
