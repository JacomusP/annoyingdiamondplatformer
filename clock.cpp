//
// Created by jacomus on 3/6/17.
//

#include <iostream>
#include "clock.h"
#include "gameData.h"

Clock* Clock::myInstance = nullptr;

Clock* Clock::getInstance()
{
    if (myInstance == nullptr)
    {
        myInstance = new Clock();
    }
    return myInstance;
}

void Clock::destroy()
{
    delete myInstance;
}

unsigned int Clock::getTicks() const
{
    if (m_paused)
    {
        return m_timeAtPause;
    }
    else
    {
        return (SDL_GetTicks() - m_timeAtStart);
    }
}

unsigned int Clock::getElapsedTicks()
{
    if (m_paused)
    {
        return 0;
    }

    m_currTicks = getTicks();
    m_ticks = m_currTicks - m_prevTicks;

    if (m_FRAME_CAP_ON)
    {
        if (m_ticks < m_PERIOD)
        {
            return 0;
        }
        m_prevTicks = m_currTicks;
        return m_ticks;
    }
    else
    {
        m_prevTicks = m_currTicks;
        return m_ticks;
    }
}

void Clock::incrementFrame()
{
    if (!m_paused)
    {
        m_frames++;
        addFrameToQueue(getFps());
    }
}

void Clock::toggleSlowMotion()
{
    throw(std::string("Slow motion is not implemented yet!"));
}

unsigned int Clock::capFrameRate() const
{
    return 0;
}

int Clock::getFps() const
{
    if (getSeconds() > 0)
    {
        return (m_frames / getSeconds());
    }
    else if (getTicks() > 5000 and getFrames() == 0)
    {
        throw std::string("Can't getFps if you don't increment the frames!");
    }
    else
    {
        return 0;
    }
}

int Clock::getAvgFps() const
{
    if (m_fpsQueue.size() == m_maxFrames)
    {
        return (m_sum / m_maxFrames);
    }
    else
    {
        return 0;
    }
}

void Clock::startClock()
{
    m_started = true;
    m_paused = false;
    m_frames = 0;
    m_timeAtStart = SDL_GetTicks();
    m_timeAtPause = m_timeAtStart;
    m_prevTicks = 0;
}

void Clock::pause()
{
    if (m_started && !m_paused)
    {
        m_timeAtPause = SDL_GetTicks() - m_timeAtStart;
        m_paused = true;
        std::cout << "Paused the game clock!" << std::endl;
    }
}

void Clock::unpause()
{
    if (m_started && m_paused)
    {
        m_timeAtStart = SDL_GetTicks() - m_timeAtPause;
        m_paused = false;
        std::cout << "Unpaused the game clock!" << std::endl;
    }
}

void Clock::addFrameToQueue(int fps)
{
    if (m_fpsQueue.size() < m_maxFrames)
    {
        m_fpsQueue.push(fps);
        m_sum += fps;
    }
    else
    {
        m_sum -= m_fpsQueue.front();
        m_fpsQueue.pop();
        m_fpsQueue.push(fps);
        m_sum += fps;
    }
}

Clock::Clock() :
    m_started(false), m_paused(false),
    m_FRAME_CAP_ON(GameData::getInstance()->getJsonBool("GameData.frameCapOn")),
    m_PERIOD(GameData::getInstance()->getJsonUInt("GameData.period")),
    m_frames(0), m_timeAtStart(0), m_timeAtPause(0), m_currTicks(0),
    m_prevTicks(0), m_ticks(0), m_maxFrames(GameData::getInstance()->getJsonUInt("GameData.maxFrames")),
    m_fpsQueue(), m_sum(0)
{}

Clock::Clock(const Clock &c) :
    m_started(c.m_started), m_paused(c.m_paused),
    m_FRAME_CAP_ON(c.m_FRAME_CAP_ON), m_PERIOD(c.m_PERIOD),
    m_frames(c.m_frames), m_timeAtStart(c.m_timeAtStart),
    m_timeAtPause(c.m_timeAtPause), m_currTicks(c.m_currTicks),
    m_prevTicks(c.m_prevTicks), m_ticks(c.m_ticks), m_maxFrames(c.m_maxFrames),
    m_fpsQueue(c.m_fpsQueue), m_sum(0)
{}

Clock::~Clock()
{}
