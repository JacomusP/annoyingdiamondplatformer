Name: James Peterkin II

Clemson Email Address: jpeterk@g.clemson.edu

Project no: 5

Project Due Date: May 2, 2017 at 9:30 pm

Project Description: To develop a complete game that had a mission and
                     most of all had an end. Also I added explosions and
					 bullets to the game which game me practice with
					 object pools which are a really neat programming
					 pattern. I also used the observer patterns to help
					 implement my enemy class AI state machine.

Game Overview: So essentially what my game is a platformer where you have
               to collect all the weapons and then defeat all of the
			   enemies on the screen but watch out the enemies can shoot
			   back at you. There are currently only two weapons in the
			   game but I plan to add more later and I will share it with
			   you Dr. Malloy. My game is also equiped with a start menu
			   and a pause menu the pause menu can be initiated by pressing
			   ESC while playing the game.

The easiest part of this project: Turning it in

Problems that I had in completing this project: I didn't have any problems
                                                this time.

All Assets in the game were created by me using Photoshop.

NOTES!!!: I just want to reiterate again that I DID NOT use xml as my game data
          source I used JSON! In order for my code to run you will need to install
		  the boost library. I am using the boost property_tree data structure to
		  store and parse the JSON data. Again I say in order for my code to run
		  and work properly you need to install the boost library!