//
// Created by jacomus on 5/9/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_PLAYER_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_PLAYER_H

#include "weaponCarrier.h"
#include "subject.h"
#include "collisionBox.h"

class Player : public WeaponCarrier, public Subject
{
public:
    Player(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
               const unsigned int numRows, const unsigned int numCols, const std::string& name, const int frameInterval,
               const unsigned int health);
    Player(const Player& p);
    Player& operator=(const Player& p);
    virtual ~Player();
    virtual void draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const;
    virtual void update(Uint32 ticks, const GameState& gameState);
    virtual std::string type() const;
    virtual bool checkForCollision(const GameObject& gObj);
    // Accessor Methods
    bool isInvincible() const { return m_invincible; }
    const CollisionBox& getCollisionBox() const { return m_collisionBox; }
    // Mutator Methods
    void setInvincibility(const bool i);
private:
    enum class STATE
    {
        IDLE_RIGHT,
        IDLE_LEFT,
        WALK_RIGHT,
        WALK_LEFT,
        RUN_RIGHT,
        RUN_LEFT,
        JUMP_LEFT,
        JUMP,
        JUMP_RIGHT,
        DOUBLE_JUMP_LEFT,
        DOUBLE_JUMP,
        DOUBLE_JUMP_RIGHT,
        FALLING,
        ATTACK_RIGHT,
        ATTACK_LEFT,
        DEAD,
        NONE
    };

    bool m_invincible;
    CollisionBox m_collisionBox;
    STATE m_state;
    // Methods
    void handleInput();
    void reactToStateChange(const STATE& lastState);
    void toggleInvincibility();
    std::string convertSTATEToString() const;
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_PLAYER_H
