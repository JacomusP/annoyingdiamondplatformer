//
// Created by jacomus on 5/29/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_COLLISIONOBJECT_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_COLLISIONOBJECT_H

#include "gameObject.h"

struct PlatformCollisionBox
{
    Vector2D uLeft;
    Vector2D uRight;
    Vector2D lLeft;
    Vector2D lRight;
    bool active;
    PlatformCollisionBox(float leftX, float rightX, float upperY, float lowerY) :
        uLeft(leftX, upperY), uRight(rightX, upperY), lLeft(leftX, lowerY), lRight(rightX, lowerY), active(true)
    {}
    PlatformCollisionBox(const PlatformCollisionBox& cBox) = default;
    PlatformCollisionBox& operator=(const PlatformCollisionBox& cBox) = default;
    ~PlatformCollisionBox() = default;
};

class CollisionObject : public GameObject
{
public:
    CollisionObject();
    CollisionObject(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
                        const unsigned int numRows, const unsigned int numCols, const std::string& name,
                        const int frameInterval, const unsigned int health, const int colHeight, const int colWidth);
    CollisionObject(const CollisionObject& cObj);
    CollisionObject& operator=(const CollisionObject& cObj);
    virtual ~CollisionObject();
    virtual void draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const;
    virtual std::string type() const;
    virtual bool checkForCollision(const GameObject& gObj);
    virtual void update(Uint32 ticks, const GameState& gameState);
    int getCollisionHeight() const { return m_collisionHeight; }
    int getCollisionWidth() const { return m_collisionWidth; }
    PlatformCollisionBox getTopCollisionBox() const { return m_top; }
    PlatformCollisionBox getRightCollisionBox() const { return m_right; }
    PlatformCollisionBox getBottomCollisionBox() const { return m_bottom; }
    PlatformCollisionBox getLeftCollisionBox() const { return m_left; }
    bool whichCollisionSide(const std::string& side) const;
    void setCollisionSideTop();
    void setCollisionSideLeft();
    void setCollisionSideBottom();
    void setCollisionSideRight();
    void setCollisionSideNone();
    void updateCollisionBoxes();
    void enableOrDisableTopCollisionBox(const bool b);
    void enableOrDisableRightCollisionBox(const bool b);
    void enableOrDisableBottomCollisionBox(const bool b);
    void enableOrDisableLeftCollisionBox(const bool b);
private:
    enum class COLLISION_SIDE
    {
        LEFT,
        TOP,
        RIGHT,
        BOTTOM,
        NONE
    };

    int m_collisionHeight;
    int m_collisionWidth;
    COLLISION_SIDE m_collisionSide;
    PlatformCollisionBox m_top;
    PlatformCollisionBox m_right;
    PlatformCollisionBox m_bottom;
    PlatformCollisionBox m_left;
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_COLLISIONOBJECT_H
