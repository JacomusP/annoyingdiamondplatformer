//
// Created by jacomus on 7/11/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_GAMEOBJECTFACTORY_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_GAMEOBJECTFACTORY_H

#include <string>
#include <map>
#include "baseCreator.h"

class GameObjectFactory
{
public:
    GameObjectFactory();
    ~GameObjectFactory();
    bool registerType(const std::string& typeID, const BaseCreator* pCreator);
    GameObject* create(const std::string& typeID);

private:
    std::map<std::string, BaseCreator*> m_creators;
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_GAMEOBJECTFACTORY_H
