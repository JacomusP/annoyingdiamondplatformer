//
// Created by jacomus on 7/12/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_WEAPONCREATOR_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_WEAPONCREATOR_H

#include "baseCreator.h"

class WeaponCreator : public BaseCreator
{
public:
    WeaponCreator(const int gObjID, const std::string& jsonPath,
                  const std::string& baseJsonPath, const std::string& texturePath);
    GameObject* createGameObject();
    virtual ~WeaponCreator() {}
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_WEAPONCREATOR_H
