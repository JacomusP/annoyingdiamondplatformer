//
// Created by jacomus on 7/12/17.
//

#include "weaponCreator.h"
#include "weapon.h"
#include "gameData.h"

WeaponCreator::WeaponCreator(const int gObjID, const std::string& jsonPath,
              const std::string& baseJsonPath, const std::string& texturePath) :
    BaseCreator(gObjID, jsonPath, baseJsonPath, texturePath)
{}

GameObject* WeaponCreator::createGameObject()
{
    GameData* inst = GameData::getInstance();
    return new Weapon(Vector2D(inst->getJsonFloat(getBaseJsonPath() + ".posX"),
                               inst->getJsonFloat(getBaseJsonPath() + ".posY")), Vector2D(), Vector2D(),
                      inst->getJsonString(getTexturePath() + ".id"), inst->getJsonUInt(getTexturePath() + ".rows"),
                      inst->getJsonUInt(getTexturePath() + ".cols"), inst->getJsonString(getBaseJsonPath() + ".name"),
                      inst->getJsonInt(getBaseJsonPath() + ".frameInterval"),
                      inst->getJsonUInt(getBaseJsonPath() + ".power"));
}
