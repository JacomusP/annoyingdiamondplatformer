//
// Created by jacomus on 4/4/17.
//

#ifndef BUILDINGDEPTH_HUD_H
#define BUILDINGDEPTH_HUD_H

#include "vector2D.h"

class Hud
{
public:
    Hud();
    Hud(const Hud& h) = delete;
    Hud& operator=(const Hud& h) = delete;
    ~Hud();
    void draw();
    void toggleDraw() { m_draw = !m_draw; }
private:
    Vector2D m_position;
    int m_width;
    int m_height;
    bool m_draw;
};


#endif //BUILDINGDEPTH_HUD_H
