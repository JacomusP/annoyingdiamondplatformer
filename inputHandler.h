//
// Created by jacomus on 2/14/17.
//

#ifndef ANIMATIONWITHSDL2_0_INPUTHANDLER_H
#define ANIMATIONWITHSDL2_0_INPUTHANDLER_H

#include <vector>
#include <SDL.h>
#include "vector2D.h"

enum mouse_buttons
{
    LEFT = 0,
    MIDDLE = 1,
    RIGHT = 2
};

class InputHandler
{
public:
    static InputHandler* getInstance();
    void update();
    static void clean();
    void reset();
    // keyboard events
    bool isKeyDown(SDL_Scancode key);
    // mouse events
    bool getMouseButtonState(int buttonNumber);
    Vector2D* getMousePosition() const;

private:
    static InputHandler* myInstance;
    std::vector<bool> m_mouseButtonStates;
    Vector2D* m_mousePosition;
    const Uint8* m_keyStates;
    // Methods
    InputHandler();
    ~InputHandler();
    // handle mouse events
    void onMouseMove(SDL_Event& event);
    void onMouseButtonDown(SDL_Event& event);
    void onMouseButtonUp(SDL_Event& event);
    // handle keyboard events
    void onKeyDown();
    void onKeyUp();
    // explicitly disallow those functions we do not want the compiler writing for me
    InputHandler(const Uint8* keys) = delete;
    InputHandler(const InputHandler& iH) = delete;
    InputHandler& operator=(const InputHandler& iH) = delete;
};


#endif //ANIMATIONWITHSDL2_0_INPUTHANDLER_H
