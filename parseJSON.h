//
// Created by jacomus on 2/28/17.
//

#ifndef POLYMORPHISMINACTION_PARSEJSON_H
#define POLYMORPHISMINACTION_PARSEJSON_H

#include <string>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

class ParseJSON
{
public:
    ParseJSON(const std::string& fileName);
    const boost::property_tree::ptree getJsonData() const;

private:
    std::string m_fileName;
    boost::property_tree::ptree m_root;

    void parseJson();

    ParseJSON(const ParseJSON&) = delete;
    ParseJSON& operator=(const ParseJSON& pjson) = delete;
};


#endif //POLYMORPHISMINACTION_PARSEJSON_H
