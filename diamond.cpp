//
// Created by jacomus on 4/4/17.
//

#include "diamond.h"
#include "gameData.h"
#include "collision.h"

Diamond::Diamond(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
                 const unsigned int numRows, const unsigned int numCols, const std::string& name, const int frameInterval,
                 const unsigned int health, const std::string& type) :
    GameObject(pos, vel, accel, textureID, numRows, numCols, name, frameInterval, health), m_type(type)
{
    if (type == "smallDiamond")
    {
        setVelocityX(GameData::getInstance()->getJsonFloat("GameData.playState.gameObjects.SmallDiamond.velX"));
        setVelocityY(GameData::getInstance()->getJsonFloat("GameData.playState.gameObjects.SmallDiamond.velY"));
    }
    else if (type == "mediumDiamond")
    {
        setVelocityX(GameData::getInstance()->getJsonFloat("GameData.playState.gameObjects.MediumDiamond.velX"));
        setVelocityY(GameData::getInstance()->getJsonFloat("GameData.playState.gameObjects.MediumDiamond.velY"));
    }
    else if (type == "largeDiamond")
    {
        setVelocityX(GameData::getInstance()->getJsonFloat("GameData.playState.gameObjects.LargeDiamond.velX"));
        setVelocityY(GameData::getInstance()->getJsonFloat("GameData.playState.gameObjects.LargeDiamond.velY"));
    }
}

Diamond::~Diamond()
{
    std::cout << "Diamond: " << getName() << "'s Destructor" << std::endl;
}

void Diamond::draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const
{
    GameObject::draw(pRenderer, flip);
}

void Diamond::update(Uint32 ticks, const GameState &gS)
{
    GameObject::advanceToNextFrameOfAnimation(ticks);
    if (gS.getStateID() == "PLAY")
    {}
    if (getPosition().getY() > GameData::getInstance()->getJsonInt("GameData.playState.world.height"))
    {
        setPositionY(0.0);
    }
    GameObject::update(ticks, gS);
}

std::string Diamond::type() const
{
    return m_type;
}

bool Diamond::checkForCollision(const GameObject& gObj)
{
    gObj.type();
    return false;
}
