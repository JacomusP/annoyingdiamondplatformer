//
// Created by jacomus on 7/11/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_BASECREATOR_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_BASECREATOR_H

#include "gameObject.h"

class BaseCreator
{
public:
    BaseCreator(const int gObjID, const std::string& jsonPath,
                const std::string& baseJsonPath, const std::string& texturePath) :
        m_gObjID(gObjID), m_strJsonPath(jsonPath), m_strBaseJsonPath(baseJsonPath),
        m_strTexturePath(texturePath)
    {}
    virtual GameObject* createGameObject() = 0;
    virtual ~BaseCreator() = default;
    // Mutator Methods
    void setGameObjectID(const int gObjID) { m_gObjID = gObjID; }
    void setJsonPath(const std::string& jsonPath) { m_strJsonPath = jsonPath; }
    void setBaseJsonPath(const std::string& baseJsonPath) { m_strBaseJsonPath = baseJsonPath; }
    void setTexturePath(const std::string& texturePath) { m_strTexturePath = texturePath; }
    // Accessor Methods
    int getGameObjectID() const { return m_gObjID; }
    const std::string& getJsonPath() const  { return m_strJsonPath; }
    const std::string& getBaseJsonPath() const { return m_strBaseJsonPath; }
    const std::string& getTexturePath() const { return m_strTexturePath; }

private:
    int m_gObjID;
    std::string m_strJsonPath;
    std::string m_strBaseJsonPath;
    std::string m_strTexturePath;
};

#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_BASECREATOR_H
