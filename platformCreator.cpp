//
// Created by jacomus on 7/12/17.
//

#include "platformCreator.h"
#include "platform.h"
#include "gameData.h"

PlatformCreator::PlatformCreator(const int gObjID, const std::string& jsonPath,
                const std::string& baseJsonPath, const std::string& texturePath, const std::string& platName) :
    BaseCreator(gObjID, jsonPath, baseJsonPath, texturePath), m_basePlatformName(platName)
{}

GameObject* PlatformCreator::createGameObject()
{
    GameData* inst = GameData::getInstance();
    std::string gObjID = std::to_string(getGameObjectID());
    Platform* p = new Platform(Vector2D(0.0f, 0.0f), Vector2D(), Vector2D(),
                               inst->getJsonString(getTexturePath() + ".id"), 1, 1, m_basePlatformName + gObjID, 0, 0,
                               inst->getJsonInt(getTexturePath() + ".collisionHeight"),
                               inst->getJsonInt(getTexturePath() + ".collisionWidth"));
    setGameObjectID(getGameObjectID() + 1);
    return p;
}
