//
// Created by jacomus on 7/11/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_ENEMYCREATOR_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_ENEMYCREATOR_H

#include "baseCreator.h"

class EnemyCreator : public BaseCreator
{
public:
    EnemyCreator(const int gObjID, const std::string& jsonPath,
                 const std::string& baseJsonPath, const std::string& texturePath, const std::string& enemyName);
    GameObject* createGameObject();
    virtual ~EnemyCreator() {}

private:
    std::string m_baseEnemyName;
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_ENEMYCREATOR_H
