//
// Created by jacomus on 5/8/17.
//

#include "gameObject.h"
#include "textureManager.h"
#include "viewPort.h"
#include "gameData.h"

GameObject::GameObject() :
    m_position(), m_velocity(), m_acceleration(), m_spriteSheet("", 0, 0), m_explosion(), m_dead(false),
    m_exploded(false), m_onGround(false), m_leftLast(false), m_name(""), m_animationNames(), m_frameInterval(0),
    m_timeSinceLastFrame(0), m_health(0)
{}

GameObject::GameObject(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
                       const unsigned int numRows, const unsigned int numCols, const std::string& name,
                       const int frameInterval, const unsigned int health) :
    m_position(pos), m_velocity(vel), m_acceleration(accel), m_spriteSheet(textureID, numRows, numCols),
    m_explosion(), m_dead(false), m_exploded(false), m_onGround(false),
    m_leftLast(false), m_name(name), m_animationNames(), m_frameInterval(frameInterval),
    m_timeSinceLastFrame(0), m_health(health)
{}

GameObject::GameObject(const GameObject& gObj) :
    m_position(gObj.m_position), m_velocity(gObj.m_velocity), m_acceleration(gObj.m_acceleration),
    m_spriteSheet(gObj.m_spriteSheet), m_explosion(gObj.m_explosion), m_dead(gObj.m_dead),
    m_exploded(gObj.m_exploded), m_onGround(gObj.m_onGround), m_leftLast(gObj.m_leftLast),
    m_name(gObj.m_name), m_animationNames(gObj.m_animationNames),
    m_frameInterval(gObj.m_frameInterval), m_timeSinceLastFrame(gObj.m_timeSinceLastFrame),
    m_health(gObj.m_health)
{}

GameObject& GameObject::operator=(const GameObject& gObj)
{
    if (this == &gObj)
    {
        return *this;
    }
    m_position = gObj.m_position;
    m_velocity = gObj.m_velocity;
    m_acceleration = gObj.m_acceleration;
    m_spriteSheet = gObj.m_spriteSheet;
    m_explosion = gObj.m_explosion;
    m_dead = gObj.m_dead;
    m_exploded = gObj.m_exploded;
    m_onGround = gObj.m_onGround;
    m_leftLast = gObj.m_leftLast;
    m_name = gObj.m_name;
    m_animationNames = gObj.m_animationNames;
    m_frameInterval = gObj.m_frameInterval;
    m_timeSinceLastFrame = gObj.m_timeSinceLastFrame;
    m_health = gObj.m_health;
    return *this;
}

GameObject::~GameObject()
{
    //std::cout << "GameObject destructor" << std::endl;
}

void GameObject::draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const
{
    if (!m_dead)
    {
        float x = m_position.getX();
        float y = m_position.getY();
        // Don't compensate for the viewport moving when drawing MenuButtons
        if (type() != "MenuButton")
        {
            x -= ViewPort::getInstance()->getX();
            y -= ViewPort::getInstance()->getY();
        }
        TextureManager::getInstance()->drawFrame(m_spriteSheet.getTextureID(),
                                                 static_cast<int>(x), static_cast<int>(y),
                                                 m_spriteSheet.getCurrentRow(),
                                                 m_spriteSheet.getCurrentFrame() % m_spriteSheet.getColumns(),
                                                 const_cast<SDL_Renderer*>(pRenderer), flip);
    }
    else
    {
        if (!m_exploded)
        {
            m_explosion.draw(const_cast<SDL_Renderer*>(pRenderer));
        }
    }
}

void GameObject::update(Uint32 ticks, const GameState& gameState)
{
    ticks += 1;
    gameState.getStateID();

    if (!m_dead)
    {
        m_velocity += m_acceleration;
        m_position += m_velocity;
    }
    else
    {
        if (!m_exploded)
        {
            m_explosion.update(ticks);
            if (m_explosion.chunkCount() == 0)
            {
                setExploded(true);
            }
        }
    }
}

void GameObject::setPosition(const Vector2D& pos)
{
    m_position = pos;
}

void GameObject::setPositionX(const float x)
{
    m_position.setX(x);
}

void GameObject::setPositionY(const float y)
{
    m_position.setY(y);
}

void GameObject::setVelocity(const Vector2D& vel)
{
    m_velocity = vel;
}

void GameObject::setVelocityX(const float x)
{
    m_velocity.setX(x);
}

void GameObject::setVelocityY(const float y)
{
    m_velocity.setY(y);
}

void GameObject::advanceToNextFrameOfAnimation(Uint32 ticks)
{
    m_timeSinceLastFrame += ticks;
    if (m_timeSinceLastFrame > m_frameInterval)
    {
        m_spriteSheet.advanceToNextFrame();
        m_timeSinceLastFrame = 0;
    }
}

void GameObject::setOnGround(const bool onGround)
{
    m_onGround = onGround;
}

void GameObject::takeDamage(const unsigned int amount)
{
    if (!m_dead)
    {
        if (amount >= m_health)
        {
            m_health = 0;
            m_dead = true;
            m_explosion.makeChunks(GameData::getInstance()->getJsonUInt("GameData.playState.chunk.size"),
                                   m_spriteSheet.getTextureID(), m_position, m_velocity, getWidth(), getHeight());
        }
        else
        {
            m_health -= amount;
        }
        m_position.setX(m_position.getX() - 20);
    }
}

void GameObject::setExploded(const bool exploded)
{
    m_exploded = exploded;
}

void GameObject::addAnimation(const std::string& name, const unsigned int row, const unsigned int rowSpan,
                              const unsigned int startPos, const unsigned int endPos)
{
    m_animationNames.insert(std::pair<std::string, std::string>(name, name));
    m_spriteSheet.addAnimation(name, row, rowSpan, startPos, endPos);
}

void GameObject::setCurrentAnimation(const std::string &animationName)
{
    if (m_animationNames.find(animationName) != m_animationNames.end())
    {
        m_spriteSheet.setCurrentAnimation(animationName);
    }
    else
    {
        std::cerr << "The texture '" << animationName << "' is not a valid animation name!" << std::endl;
    }
}

void GameObject::setLeftLast(const bool lL)
{
    m_leftLast = lL;
}

int GameObject::getHeight() const
{
    return TextureManager::getInstance()->getHeight(m_spriteSheet.getTextureID());
}

int GameObject::getWidth() const
{
    return TextureManager::getInstance()->getWidth(m_spriteSheet.getTextureID());
}
