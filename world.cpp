//
// Created by jacomus on 3/6/17.
//

#include <iostream>
#include "world.h"
#include "game.h"
#include "textureManager.h"
#include "viewPort.h"

World::World(const std::string& id, int fact, int pW, int pH, int w, int h) :
    m_textureId(id), m_factor(fact), m_picWidth(pW), m_picHeight(pH),
    m_width(w), m_height(h), m_viewX(0.0), m_viewY(0.0)
{}

World::~World()
{}

void World::update()
{
    //std::cout << "m_factor: " << m_factor << " m_picWidth: " << m_picWidth << std::endl;
    m_viewX = static_cast<int>(ViewPort::getInstance()->getX() / m_factor) % m_picWidth;
    m_viewY = ViewPort::getInstance()->getY();
    m_width += 0;
    m_height += 0;
    //std::cout << m_textureId << ": m_viewX = " << m_viewX << " m_viewY = " << m_viewY << std::endl;
}

void World::draw()
{
    TextureManager* tM_inst = TextureManager::getInstance();
    int viewX = static_cast<int>(m_viewX);
    int viewY = static_cast<int>(m_viewY);
    tM_inst->draw(m_textureId, -viewX, -viewY, const_cast<SDL_Renderer*>(Game::getInstance()->getRenderer()),
                  SDL_FLIP_NONE);
    tM_inst->draw(m_textureId, m_picWidth-viewX, -viewY,
                  const_cast<SDL_Renderer*>(Game::getInstance()->getRenderer()), SDL_FLIP_NONE);
}

std::ostream& operator<<(std::ostream& out, const World& world)
{
    out << "Id: " << world.getTextureId() << std::endl << "Factor: " << world.getFactor() << std::endl
        << "Picture Width: " << world.getPicWidth() << " Picture Height: " << world.getPicHeight()
        << std::endl << "World Width: " << world.getWorldWidth() << " World Height: "
        << world.getWorldHeight() << std::endl << "ViewPortX: " << world.getViewX() << " ViewPortY: "
        << world.getViewY();
    return out;
}
