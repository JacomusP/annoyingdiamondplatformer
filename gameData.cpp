//
// Created by jacomus on 2/28/17.
//

#include <boost/property_tree/exceptions.hpp>
#include "gameData.h"

GameData* GameData::myInstance = nullptr;

GameData* GameData::getInstance()
{
    if (myInstance == nullptr)
    {
        myInstance = new GameData();
    }
    return myInstance;
}

void GameData::clean()
{
    delete myInstance;
}

bool GameData::getJsonBool(const std::string& s) const
{
    try
    {
        bool data = m_gameData.get<bool>(s);
        return data;
    }
    catch (const boost::property_tree::ptree_error& e)
    {
        throw(e);
    }
}

const std::string GameData::getJsonString(const std::string& s) const
{
    try
    {
        return m_gameData.get<std::string>(s);
    }
    catch (const boost::property_tree::ptree_error& e)
    {
        throw(e);
    }
}

float GameData::getJsonFloat(const std::string& s) const
{
    try
    {
        float data = m_gameData.get<float>(s);
        return data;
    }
    catch (const boost::property_tree::ptree_error& e)
    {
        throw(e);
    }
}

int GameData::getJsonInt(const std::string& s) const
{
    try
    {
        int data = m_gameData.get<int>(s);
        return data;
    }
    catch (const boost::property_tree::ptree_error& e)
    {
        throw(e);
    }
}

Uint32 GameData::getJsonUInt(const std::string& s) const
{
    try
    {
        Uint32 data = m_gameData.get<Uint32>(s);
        return data;
    }
    catch (const boost::property_tree::ptree_error& e)
    {
        throw(e);
    }
}

GameData::GameData(const std::string& fileName) :
    m_parser(fileName), m_gameData(m_parser.getJsonData())
{}

GameData::~GameData()
{}
