//
// Created by jacomus on 7/12/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_PLATFORMCREATOR_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_PLATFORMCREATOR_H

#include "baseCreator.h"

class PlatformCreator : public BaseCreator
{
public:
    PlatformCreator(const int gObjID, const std::string& jsonPath,
                    const std::string& baseJsonPath, const std::string& texturePath, const std::string& platName);
    GameObject* createGameObject();
    virtual ~PlatformCreator() {}

private:
    std::string m_basePlatformName;
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_PLATFORMCREATOR_H
