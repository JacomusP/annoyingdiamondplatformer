//
// Created by jacomus on 4/4/17.
//

#ifndef BUILDINGDEPTH_DIAMONDMANAGER_H
#define BUILDINGDEPTH_DIAMONDMANAGER_H

#include <vector>
#include "diamond.h"

class DiamondManager
{
public:
    DiamondManager();
    DiamondManager(const DiamondManager& dM) = delete;
    DiamondManager& operator=(const DiamondManager& dM) = delete;
    ~DiamondManager();
    void drawSmall(const SDL_Renderer* pRenderer, SDL_RendererFlip flip);
    void drawMedium(const SDL_Renderer* pRenderer, SDL_RendererFlip flip);
    void drawLarge(const SDL_Renderer* pRenderer, SDL_RendererFlip flip);
    void update(Uint32 ticks, const GameState& gS);
    void initializeDiamonds();
    unsigned int getNumSmall() const { return static_cast<int>(m_smallDiamonds.size()); }
    unsigned int getNumMedium() const { return static_cast<int>(m_mediumDiamonds.size()); }
    unsigned int getNumLarge() const { return static_cast<int>(m_largeDiamonds.size()); }
private:
    std::vector<Diamond*> m_smallDiamonds;
    std::vector<Diamond*> m_mediumDiamonds;
    std::vector<Diamond*> m_largeDiamonds;

    // Methods
    void updateSmall(Uint32 ticks, const GameState& gS);
    void updateMedium(Uint32 ticks, const GameState& gS);
    void updateLarge(Uint32 ticks, const GameState& gS);
};


#endif //BUILDINGDEPTH_DIAMONDMANAGER_H
