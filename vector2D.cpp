//
// Created by jacomus on 2/14/17.
//

#include <cmath>
#include <iostream>
#include "vector2D.h"

Vector2D::Vector2D() :
    m_x(0), m_y(0)
{}

Vector2D::Vector2D(const float x, const float y) :
    m_x(x), m_y(y)
{}

Vector2D::Vector2D(const Vector2D &v2) :
    m_x(v2.m_x), m_y(v2.m_y)
{}

Vector2D::~Vector2D()
{}

float Vector2D::getX() const
{
    return m_x;
}

float Vector2D::getY() const
{
    return m_y;
}

void Vector2D::setX(const float x)
{
    m_x = x;
}

void Vector2D::setY(const float y)
{
    m_y = y;
}

float Vector2D::distance()
{
    float distance = std::sqrt((m_x * m_x) + (m_y * m_y));
    return distance;
}

Vector2D Vector2D::operator+(const Vector2D& v2)
{
    return Vector2D(m_x + v2.m_x, m_y + v2.m_y);
}

Vector2D& operator+=(Vector2D& v1, Vector2D& v2)
{
    v1.m_x += v2.m_x;
    v1.m_y += v2.m_y;
    return v1;
}

Vector2D Vector2D::operator*(const float scalar)
{
    return Vector2D(scalar * m_x, scalar * m_y);
}

Vector2D& Vector2D::operator*=(const float scalar)
{
    m_x *= scalar;
    m_y *= scalar;
    return *this;
}

Vector2D Vector2D::operator-(const Vector2D& v2)
{
    return Vector2D(m_x - v2.m_x, m_y - v2.m_y);
}

Vector2D& operator-=(Vector2D& v1, Vector2D& v2)
{
    v1.m_x -= v2.m_x;
    v1.m_y -= v2.m_y;
    return v1;
}

Vector2D Vector2D::operator/(const float scalar)
{
    return Vector2D(m_x / scalar, m_y / scalar);
}

Vector2D& Vector2D::operator/=(const float scalar)
{
    m_x /= scalar;
    m_y /= scalar;
    return *this;
}

Vector2D& Vector2D::operator=(const Vector2D& v)
{
    if (this == &v)
    {
        return *this;
    }
    m_x = v.m_x;
    m_y = v.m_y;
    return *this;
}

std::ostream& operator<<(std::ostream& out, const Vector2D& v)
{
    return out << v.getX() << ", " << v.getY();
}

Vector2D operator+(const Vector2D& v1, const Vector2D& v2)
{
    return Vector2D(v1.getX() + v2.getX(), v1.getY() + v2.getY());
}

Vector2D operator-(const Vector2D& v1, const Vector2D& v2)
{
    return Vector2D(v1.getX() - v2.getX(), v1.getY() - v2.getY());
}

bool operator==(const Vector2D& v1, const Vector2D& v2)
{
    return (v1.getX() == v2.getX() and v1.getY() == v2.getY());
}

void Vector2D::normalize()
{
    float dist = distance();
    if (dist > 0) // Never want to attempt to divide by 0
    {
        (*this) *= 1 / dist;
    }
}
