//
// Created by jacomus on 3/25/17.
//

#ifndef BUILDINGDEPTH_SPRITESHEET_H
#define BUILDINGDEPTH_SPRITESHEET_H

#include <string>
#include <map>

class SpriteSheet
{
public:
    SpriteSheet(const std::string& textureID, const unsigned int numRows, const unsigned int numCols);
    SpriteSheet(const SpriteSheet& sS);
    ~SpriteSheet();
    SpriteSheet& operator=(const SpriteSheet& sS);
    void addAnimation(const std::string& name, const unsigned int row, const unsigned int rowSpan,
                      const unsigned int startPos, const unsigned int endPos);
    void advanceToNextFrame();
    void setCurrentAnimation(const std::string& name);
    void setCurrentFrame(const unsigned int frame);
    const std::string& getTextureID() const { return m_textureID; }
    const std::string& getCurrentAnimation() const { return m_currentAnimation; }
    unsigned int getCurrentFrame() const { return m_currentFrame; }
    unsigned int getCurrentRow() const { return m_currentRow; }
    unsigned int getColumns() const { return m_numCols; }
    unsigned int getStartPos() const { return m_mapAnimations.find(m_currentAnimation)->second->m_startPos; }
    unsigned int getEndPos() const { return m_mapAnimations.find(m_currentAnimation)->second->m_endPos; }

private:
    struct Animation
    {
        std::string m_name;
        unsigned int m_row;
        unsigned int m_rowSpan;
        unsigned int m_startPos; // in row
        unsigned int m_endPos; // in row
        Animation(const std::string& name, const unsigned int row, const unsigned int rowSpan,
                  const unsigned int startPos, const unsigned int endPos) :
            m_name(name), m_row(row), m_rowSpan(rowSpan), m_startPos(startPos), m_endPos(endPos)
        {}
    };

    std::map<std::string, Animation*> m_mapAnimations;
    std::string m_textureID;
    std::string m_currentAnimation;
    unsigned int m_currentFrame;
    unsigned int m_currentRow;
    unsigned int m_numRows;
    unsigned int m_numCols;

    // Methods
    void freeAnimations();
    void insertAnimations(const std::map<std::string, Animation*> animations);
};


#endif //BUILDINGDEPTH_SPRITESHEET_H
