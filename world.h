//
// Created by jacomus on 3/6/17.
//

#ifndef POLYMORPHISMINACTION_WORLD_H
#define POLYMORPHISMINACTION_WORLD_H

#include <string>
#include <SDL_quit.h>
#include <iostream>

class World
{
public:
    World(const std::string& id, const int factor, const int picWidth, const int picHeight,
          const int worldWidth, const int worldHeight);
    virtual ~World();
    void update();
    virtual void draw();
    float getViewX() const { return m_viewX; }
    float getViewY() const { return m_viewY; }
    int getPicWidth() const { return m_picWidth; }
    int getPicHeight() const { return m_picHeight; }
    int getWorldWidth() const { return m_width; }
    int getWorldHeight() const { return m_height; }
    int getFactor() const { return m_factor; }
    const std::string& getTextureId() const { return m_textureId; }

private:
    std::string m_textureId;
    int m_factor;
    int m_picWidth;
    int m_picHeight;
    int m_width;
    int m_height;
    float m_viewX;
    float m_viewY;

    // Explicitly disallow those function we do not want the compiler to generate for us
    World(const World& w) = delete;
    World& operator=(const World& w) = delete;
};

std::ostream& operator<<(std::ostream& out, const World& world);

#endif //POLYMORPHISMINACTION_WORLD_H
