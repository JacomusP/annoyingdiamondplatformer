//
// Created by jacomus on 3/23/17.
//

#include "pauseState.h"
#include "menuState.h"
#include "game.h"
#include "gameData.h"
#include "textureManager.h"
#include "textureManagerException.h"
#include "menuButton.h"
#include "inputHandler.h"
#include "clock.h"

const std::string PauseState::s_pauseID = "PAUSE";

PauseState::PauseState() :
    GameState(), m_gameObjects()
{}

PauseState::~PauseState()
{
    onExit();
}

void PauseState::update(Uint32 ticks)
{
    if (GameState::getExited()) return; // if we have exited this state then return from this function
    for (unsigned int i = 0; i < m_gameObjects.size(); i++)
    {
        m_gameObjects[i]->update(ticks, *this);
    }
}

void PauseState::render()
{
    if (GameState::getExited()) return; // if we have exited this state then return from this function
    for (unsigned int i = 0; i < m_gameObjects.size(); i++)
    {
        m_gameObjects[i]->draw(Game::getInstance()->getRenderer(), SDL_FLIP_NONE);
    }
}

bool PauseState::onEnter()
{
    GameData* inst = GameData::getInstance();
    if (!loadTextures())
    {
        std::cerr << "pauseState - Problem loading textures!" << std::endl;
        return false;
    }
    try
    {
        GameState::setExited(false);
        m_gameObjects.push_back(
            new MenuButton(Vector2D(inst->getJsonFloat("GameData.pauseState.gameObjects.MenuButton.posX"),
                                    inst->getJsonFloat("GameData.pauseState.gameObjects.MenuButton.posY")), Vector2D(),
                           Vector2D(), inst->getJsonString("GameData.pauseState.textures.menuButton.id"), 1, 3,
                           "playButt", 0, 0, s_pauseToMain));
        m_gameObjects.push_back(
            new MenuButton(Vector2D(inst->getJsonFloat("GameData.pauseState.gameObjects.ResumeButton.posX"),
                                    inst->getJsonFloat("GameData.pauseState.gameObjects.ResumeButton.posY")),
                           Vector2D(), Vector2D(), inst->getJsonString("GameData.pauseState.textures.resumeButton.id"),
                           1, 3, "exitButt", 0, 0, s_resumePlay));
    }
    catch (const boost::property_tree::ptree_error& e)
    {
        std::cerr << "pauseState - Error Reading JSON data! " << e.what() << std::endl;
        return false;
    }
    std::cout << "entering pauseState" << std::endl;
    return true;
}

bool PauseState::loadTextures()
{
    try
    {
        GameData* inst = GameData::getInstance();
        TextureManager::getInstance()->load(inst->getJsonString("GameData.pauseState.textures.menuButton.path"),
                                            inst->getJsonString("GameData.pauseState.textures.menuButton.id"),
                                            inst->getJsonInt("GameData.pauseState.gameObjects.MenuButton.width"),
                                            inst->getJsonInt("GameData.pauseState.gameObjects.MenuButton.height"),
                                            const_cast<SDL_Renderer*>(Game::getInstance()->getRenderer()));
        std::cout << "pauseState - menuButton loaded" << std::endl;
        TextureManager::getInstance()->load(inst->getJsonString("GameData.pauseState.textures.resumeButton.path"),
                                            inst->getJsonString("GameData.pauseState.textures.resumeButton.id"),
                                            inst->getJsonInt("GameData.pauseState.gameObjects.ResumeButton.width"),
                                            inst->getJsonInt("GameData.pauseState.gameObjects.ResumeButton.height"),
                                            const_cast<SDL_Renderer*>(Game::getInstance()->getRenderer()));
        std::cout << "pauseState - resumeButton loaded" << std::endl;
    }
    catch (const boost::property_tree::ptree_error& e)
    {
        std::cerr << "pauseState - Error loading JSON data! " << e.what() << std::endl;
        return false;
    }
    catch (const TextureManagerException& tME)
    {
        std::cerr << "pauseState - Error: " << tME.what();
        return false;
    }
    return true;
}

bool PauseState::onExit()
{
    GameData* inst = GameData::getInstance();
    if (!GameState::getExited())
    {
        for (GameObject* sdlGO : m_gameObjects)
        {
            delete sdlGO;
        }
        m_gameObjects.clear();
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.pauseState.textures.menuButton.id"));
        TextureManager::getInstance()->clearFromTextureMap(
            inst->getJsonString("GameData.pauseState.textures.resumeButton.id"));
        // reset the mouse button states to false
        InputHandler::getInstance()->reset();
    }
    std::cout << "exiting pauseState" << std::endl;
    GameState::setExited(true);
    return true;
}

void PauseState::s_pauseToMain()
{
    Game::getInstance()->getStateMachine()->exitState("PLAY");
    Game::getInstance()->getStateMachine()->changeState("MENU", new MenuState());
}

void PauseState::s_resumePlay()
{
    Game::getInstance()->getStateMachine()->popState();
}

std::string PauseState::getStateID() const
{
    return s_pauseID;
}
