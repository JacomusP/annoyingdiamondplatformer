//
// Created by jacomus on 5/23/17.
//

#ifndef OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_BULLET_H
#define OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_BULLET_H

#include "gameObject.h"
#include "vector2D.h"

class Bullet : public GameObject
{
public:
    Bullet(const Vector2D& pos, const Vector2D& vel, const Vector2D& accel, const std::string& textureID,
               const unsigned int numRows, const unsigned int numCols, const std::string& name, const int frameInterval,
               const unsigned int health);
    Bullet(const Bullet& b);
    Bullet& operator=(const Bullet& b);
    virtual ~Bullet();
    virtual void draw(const SDL_Renderer* pRenderer, SDL_RendererFlip flip) const;
    virtual std::string type() const;
    virtual bool checkForCollision(const GameObject& gObj);
    virtual void update(Uint32 ticks, const GameState& gameState);
    bool goneTooFar() const { return m_bTooFar; }
    void reset();
private:
    bool m_bTooFar;
    float m_distance;
    float m_maxDistance;
    Vector2D m_startingPosition;
};


#endif //OBJECTPOOLSCOLLISIONSEXPLOSIONSPROJECTILES_BULLET_H
