//
// Created by jacomus on 3/1/17.
//

#ifndef POLYMORPHISMINACTION_OBSERVER_H
#define POLYMORPHISMINACTION_OBSERVER_H

#include "vector2D.h"

class Observer
{
public:
    virtual ~Observer();
    virtual void updateFromSubject(const Vector2D& pos) = 0;

protected:
    Observer();
};


#endif //POLYMORPHISMINACTION_OBSERVER_H
